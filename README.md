# Importante: Se migra el repositorio a gitlab, además se separa en una arquitectura API-REST en el nuevo repositorio. No obstante, de acá se puede tomar y usar todo lo que sirva :)
[Nuevo repo](https://gitlab.com/huertechno)

Tanto el código como la [documentación](https://0xacab.org/vladimir.aap/HuerTechno/wikis/home) son un trabajo en proceso.

# Instalación de la aplicación web

Huertechno como dispositivo tecnologico podría ser dividido en dos partes.

- Huertechno web
- Huertechno dispositivo (device)

La parte del dispositivo está documentado en [link a esa parte del repo]() donde se comenta la forma de cargar en tu microcontrolador el programa que actuara como **sensor** o como **actuador**. 

En estas instrucciones 

Para ver los detalles del funcionamiento y componentes del Sistema de monitoreo y control remoto [TecnoHuerta v1.0](https://0xacab.org/tecnohuerta/HuerTechno/wikis/huertechno-v1.0) favor dirigirse al enlace. 


A continuación se presenta la instalación del sistema el cual está montado en una Raspberry pi con [Raspbian Stretch lite  2018-03-13](https://www.raspberrypi.org/downloads/raspbian/). 

Toda la instalación y configuración es mediante ssh, por lo que si necesitas saber como conectarte favor dirigirte a la pagina de la wiki sobre [configuración de internet y ssh]().

## Prerequisitos (para agregar en docker)
 - git
 - [zsh](https://github.com/robbyrussell/oh-my-zsh)
 - tmux
 - vim


## I. Instalación del servidor y sistema de base de datos:

Para que la aplicación sea desplegada en un explorador a través de cualquier dispositivo dentro de la red local
se debe instalar un servidor, para nuestro caso hemos instalado y configurado [Nginx](https://nginx.org/en/).

    1. Nginx hará de proxi revers, para instalar ponemos:

> `$ sudo apt install nginx `


### Configuración de Nginx

Una vez instalado Nginx, editamos el archivo */etc/nginx/sites-available/default* y lo reemplazamos por el siguiente
codigo:

```
server {

        listen 80;
        server_tokens off;
        server_name huertechno.com;

        location / {
                include uwsgi_params;
                uwsgi_pass unix:/tmp/huertechno.sock;
        }
}

```

Luego, procedemos a crear un enlace simbolico de este archivo que se encuentra en *sites-available* hacia la 
carpeta *sites-enabled*

` sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default `


 Posterior recargamos la configuración:
 
 `sudo service nginx reload `
 
 
## instalación de redis

La aplicación utiliza una bsae de datos en ram para gestionar las colas junto a Celery. Esto lo utilizamos como
una alternativa al multihilo en las aplicaciones. En nuestro caso hemos utilizado [Redis](https://redis.io/).

    2. Instalamos redis.

> `$ sudo apt-get install redis-server`

Como sistema de base de datos hemos escogido postgreql versión estable 9.6. Una de las ventajas es que posee un campo tipo JSON
donde podemos almacenar las mediciones.

    3. A continuación instalamos y configuramos la base de datos Postgresql

> `$ sudo apt-get install postgresql-9.6`

Creamos la base de datos **huertechno** y el role **pi** a quien asociaremos a la base de datos y le daremos los privilegios 
correspondientes. Esto debido a que la aplicación en flask va a crear todas las tablas necesarias, no obstante debe estar creada
la base de datos.

 > `$ sudo -u postgres bash`

Entramos al interprete de postgresql

> `postgres@raspberrypi:/home/pi$ psql`

Creamos la base de dato **huertechno** quien almacenará las mediciones desde los sensores.

>  `postgres=# create database huertechno;`
  
Creamos el role **pi**  junto con la password *pi123*, la cual después deberías cambiar por una más segura.
Además, a este role le asignamos todos los privilegios.

> `postgres=# CREATE USER pi WITH PASSWORD 'pi123'; `
 
> `postgres=# GRANT ALL PRIVILEGES ON DATABASE "huertechno" to "pi";`

> `postgres=# ALTER ROLE "pi" WITH LOGIN;;`


Salimos de *psql* (crl + d)

>  `postgres=# \q;`


##  Instalación y configuración de mosquitto
Para esta versión en la cual trabajaremos con un dispositivo basado en la placa **ESP8266** 
utilizaremos mosquitto como implementación del broker mqtt. Este broker recibirá 
los mensajes que publican los dipositivos, restringirá el acceso a publicar y suscribirse
entre otras cosas. 

Para más información se puede ver:
 - [Protocolo Mqtt](http://mqtt.org/)
 - [Mosquitto](https://mosquitto.org/)
 - [Huertechno -mqtt](enlace a una pagina de la wiki) (construcción)

Para instalar mosquitto lo haremos a través de los repositorios:

>  `pi@raspberrypi:~ $ sudo apt install mosquitto`

configuración de mosquitto, la cual podemos editar el archivo que se 
encuentra en /etc/mosquitto/conf.d

### Configuración de Mosquitto

A continuación, configuramos el broker para que se puedan conectar solo los dispositivos
registrados.




## II. Instalación y configuración de directorios y librerías de python:

    1. Primero asegurarte que tu sistema está actualizado en la última versión
  
  >  `pi@raspberrypi:~ $ sudo apt-get update & sudo apt upgrade`


    2. Instalamos *pip* para poder instalar paquetes en python e instalar *virtualenv* como entorno virtual. La razón es que nativamente se encuentra en el sistema la versión de python2.7, sin embargo, esta versión en poco tiempo quedará sin soporte por lo que se debe instalar python3. En este sentido, para no tener problemas en el sistema y romper dependencias generamos un entorno virtual.

> `pi@raspberrypi:~ $ sudo apt install python-pip`

> `pi@raspberrypi:~ $ sudo pip install virtualenv`



    3. Clonamos el repositorio en nuestro directorio "~" y le damos permisos .
    
> `pi@raspberrypi:~ $ sudo apt-get install git`
    
> `pi@raspberrypi:~ $ git clone https://0xacab.org/hueretechno/HuerTechno.git`

> `pi@raspberrypi:~ $ sudo chown -R www-data:www-data /home/pi/HuerTechno`



    4. Nos posicionamos en src_app,  creamosla carpeta venv 

> `pi@raspberrypi:~ $ cd HuerTechno/src/src_app`

> `pi@raspberrypi:~/src_app $ mkdir venv`



Clonamos el repositorio de la dirección ssh, no obstante puedes clonarlo de http.
En la carpeta venv montaremos el entorno virtual con la opción *-p python3* que será la 
forma tener python3 en nuestro ambiente virtual .


> `pi@raspberrypi:~/src_app $ virtualenv -p python3 venv `

> `pi@raspberrypi:~/src_app $ sudo venv/bin/pip install -r requirements.txt `


## III. Instalamos uWSGI

uWSGI junto con Nginx hará de proxi revers en nuestra aplicación, de esta forma tendremos
la comunicación desde nuestro entorno virtual hacia el servicio web.

    1. Instalamos uwsgi en el sistema y el plugin para la posterior configuración. 

> `$ sudo apt-get install uwsgi uwsgi-plugin-python3`


### Configuración de uWSGI

Para configurar uWSGI creamos un archivo **huertechno.ini** en en */etc/uwsgi/apps-available* 
con el siguiente contenido:

```
 [uwsgi]                                                                                                                                                                
                                                                                                                                                                        
 plugin     = python3                                                                                                                                                   
 #socket    = 127.0.0.1:8080                                                                                                                                            
 socket     = /tmp/huertechno.sock                                                                                                                                      
 chdir      = /home/pi/HuerTechno/src/src_app/                                                                                                                          
 venv       = /home/pi/HuerTechno/src/src_app/venv                                                                                                                      
 uid        = www-data                                                                                                                                                  
 gid        = www-data                                                                                                                                                  
 wsgi-file  = run.py                                                                                                                                                    
 callable   = app                                                                                                                                                       
 logto = /tmp/huertechno.log                                                                                                                                            
                                                                                                                                                                        
 # the fix                                                                                                                                                              
 lazy = true                                                                                                                                                            
 lazy-apps = true  
```

Luego procedemos a crear un enlace simbolico en *apps-enabled*

 ` sudo ln -s /etc/uwsgi/apps-available/huertechno.ini /etc/uwsgi/apps-enabled/huertechno.ini`
 
 Posterior recargamos la configuración:
 
 `sudo service uwsgi reload `
 

Para el momento debemos tener corriendo los siguientes servicios:
 - mosquitto
 - postgresql
 - nginx
 - uwsgi
Lo anterior lo podemos comporobar mediante:
>  `pi@raspberrypi:~ $ sudo service <nombre del servicio> status`


### Configuración de Celery

Finalmente, para que Celery se inicie de forma automática como un servicio,
es necesario implementarlo en systemd. Siguiendo la 
[documentación de celery](http://docs.celeryproject.org/en/latest/userguide/daemonizing.html#usage-systemd)
debemos crear y configurar los archivos que ahí se mencionan. 

En nuestro caso, sino existe la carpeta conf.d se debe crear y los archivos quedarían.

#### celery.service 
```
/etc/systemd/system/celery.service
```

```
[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
User=pi
Group=www-data
EnvironmentFile=/etc/conf.d/celery
WorkingDirectory=/home/pi/HuerTechno/src/src_app
ExecStart=/bin/sh -c '${CELERY_BIN} multi start w1 -A ${CELERY_APP}'
ExecStop=/bin/sh -c '${CELERY_BIN} multi stopwait ${CELERYD_NODES} \
  --pidfile=${CELERYD_PID_FILE}'
ExecReload=/bin/sh -c '${CELERY_BIN} multi restart ${CELERYD_NODES} \
  -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
  --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'

[Install]
WantedBy=multi-user.target
```

Mientras que el archivo

``` /etc/conf.d/celery: ```

debería quedar de la siguiente forma.

``` 
# Name of nodes to start
# here we have a single node
CELERYD_NODES="w1"
# or we could have three nodes:
#CELERYD_NODES="w1 w2 w3"

# Absolute or relative path to the 'celery' command:
#CELERY_BIN="/usr/local/bin/celery"
CELERY_BIN="/home/pi/HuerTechno/src/src_app/venv/bin/celery"

# App instance to use
# comment out this line if you don't use an app
#CELERY_APP="app"
# or fully qualified:
CELERY_APP="app.background"

# How to call manage.py
CELERYD_MULTI="multi"

# Extra command-line arguments to the worker
CELERYD_OPTS="--time-limit=300 --concurrency=8"

# - %n will be replaced with the first part of the nodename.
# - %I will be replaced with the current child process index
#   and is important when using the prefork pool to avoid race conditions.
CELERYD_PID_FILE="/var/run/celery/%n.pid"
CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
CELERYD_LOG_LEVEL="INFO"

```

y el archivo /etc/tmpfiles.d/celery.conf queda igual que como lo señala la documentación.




