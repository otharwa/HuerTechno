--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.7
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: action; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE action (
    id integer NOT NULL,
    date_created timestamp without time zone,
    time_action timestamp without time zone,
    cultivo_id integer,
    component_id integer,
    state character varying(2) NOT NULL,
    accion character varying(3)
);


ALTER TABLE action OWNER TO pi;

--
-- Name: action_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE action_id_seq OWNER TO pi;

--
-- Name: action_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE action_id_seq OWNED BY action.id;


--
-- Name: book; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE book (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    recipe_id integer
);


ALTER TABLE book OWNER TO pi;

--
-- Name: book_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE book_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE book_id_seq OWNER TO pi;

--
-- Name: book_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE book_id_seq OWNED BY book.id;


--
-- Name: ciclo_vegetativo; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE ciclo_vegetativo (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    plant_id integer
);


ALTER TABLE ciclo_vegetativo OWNER TO pi;

--
-- Name: ciclo_vegetativo_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE ciclo_vegetativo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ciclo_vegetativo_id_seq OWNER TO pi;

--
-- Name: ciclo_vegetativo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE ciclo_vegetativo_id_seq OWNED BY ciclo_vegetativo.id;


--
-- Name: component; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE component (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    model character varying(128),
    marca character varying(128),
    "precision" character varying(128),
    variable character varying(128),
    function character varying(128),
    state character varying(128),
    num_pin character varying(2)
);


ALTER TABLE component OWNER TO pi;

--
-- Name: component_cultivo; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE component_cultivo (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    cultivo_id integer,
    component_id integer,
    state character varying(3)
);


ALTER TABLE component_cultivo OWNER TO pi;

--
-- Name: component_cultivo_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE component_cultivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE component_cultivo_id_seq OWNER TO pi;

--
-- Name: component_cultivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE component_cultivo_id_seq OWNED BY component_cultivo.id;


--
-- Name: component_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE component_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE component_id_seq OWNER TO pi;

--
-- Name: component_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE component_id_seq OWNED BY component.id;


--
-- Name: cultivo; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE cultivo (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    hight integer,
    lenght integer,
    type_cultivo character varying(128)
);


ALTER TABLE cultivo OWNER TO pi;

--
-- Name: cultivo_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE cultivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cultivo_id_seq OWNER TO pi;

--
-- Name: cultivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE cultivo_id_seq OWNED BY cultivo.id;


--
-- Name: detaile_cultivo; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE detaile_cultivo (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    cultivo_id integer,
    plant_id integer,
    cantidad character varying(128)
);


ALTER TABLE detaile_cultivo OWNER TO pi;

--
-- Name: detaile_cultivo_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE detaile_cultivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detaile_cultivo_id_seq OWNER TO pi;

--
-- Name: detaile_cultivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE detaile_cultivo_id_seq OWNED BY detaile_cultivo.id;


--
-- Name: detaile_device; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE detaile_device (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    device_id integer,
    component_id integer
);


ALTER TABLE detaile_device OWNER TO pi;

--
-- Name: detaile_device_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE detaile_device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE detaile_device_id_seq OWNER TO pi;

--
-- Name: detaile_device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE detaile_device_id_seq OWNED BY detaile_device.id;


--
-- Name: device; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE device (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    model character varying(128),
    marca character varying(128),
    function character varying(128)
);


ALTER TABLE device OWNER TO pi;

--
-- Name: device_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE device_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE device_id_seq OWNER TO pi;

--
-- Name: device_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE device_id_seq OWNED BY device.id;


--
-- Name: huerta; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE huerta (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    hight integer NOT NULL,
    lenght integer NOT NULL,
    type_huerta character varying(128)
);


ALTER TABLE huerta OWNER TO pi;

--
-- Name: measure; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE measure (
    id integer NOT NULL,
    date_created timestamp without time zone,
    cultivo_id integer,
    component_id integer,
    variable character varying(128),
    value character varying(10) NOT NULL
);


ALTER TABLE measure OWNER TO pi;

--
-- Name: measure_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE measure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE measure_id_seq OWNER TO pi;

--
-- Name: measure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE measure_id_seq OWNED BY measure.id;


--
-- Name: plant; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE plant (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    varieti_id integer
);


ALTER TABLE plant OWNER TO pi;

--
-- Name: plant_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE plant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE plant_id_seq OWNER TO pi;

--
-- Name: plant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE plant_id_seq OWNED BY plant.id;


--
-- Name: recipe; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE recipe (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    info_recipe json
);


ALTER TABLE recipe OWNER TO pi;

--
-- Name: recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_id_seq OWNER TO pi;

--
-- Name: recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE recipe_id_seq OWNED BY recipe.id;


--
-- Name: specie; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE specie (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    especie_id character varying(128),
    especie character varying(128)
);


ALTER TABLE specie OWNER TO pi;

--
-- Name: specie_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE specie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE specie_id_seq OWNER TO pi;

--
-- Name: specie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE specie_id_seq OWNED BY specie.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE "user" (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    mail character varying(128),
    password character varying(128)
);


ALTER TABLE "user" OWNER TO pi;

--
-- Name: user_cultivo; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE user_cultivo (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    role character varying(128),
    last_conexion timestamp without time zone,
    cultivo_id integer,
    user_id integer
);


ALTER TABLE user_cultivo OWNER TO pi;

--
-- Name: user_cultivo_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE user_cultivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_cultivo_id_seq OWNER TO pi;

--
-- Name: user_cultivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE user_cultivo_id_seq OWNED BY user_cultivo.id;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_id_seq OWNER TO pi;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE user_id_seq OWNED BY "user".id;


--
-- Name: varieti; Type: TABLE; Schema: public; Owner: pi
--

CREATE TABLE varieti (
    id integer NOT NULL,
    name character varying(128),
    date_created timestamp without time zone,
    genotipo character varying(128),
    cruce character varying(128),
    production character varying(128),
    specie_id integer
);


ALTER TABLE varieti OWNER TO pi;

--
-- Name: varieti_id_seq; Type: SEQUENCE; Schema: public; Owner: pi
--

CREATE SEQUENCE varieti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE varieti_id_seq OWNER TO pi;

--
-- Name: varieti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pi
--

ALTER SEQUENCE varieti_id_seq OWNED BY varieti.id;


--
-- Name: action id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY action ALTER COLUMN id SET DEFAULT nextval('action_id_seq'::regclass);


--
-- Name: book id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY book ALTER COLUMN id SET DEFAULT nextval('book_id_seq'::regclass);


--
-- Name: ciclo_vegetativo id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY ciclo_vegetativo ALTER COLUMN id SET DEFAULT nextval('ciclo_vegetativo_id_seq'::regclass);


--
-- Name: component id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component ALTER COLUMN id SET DEFAULT nextval('component_id_seq'::regclass);


--
-- Name: component_cultivo id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component_cultivo ALTER COLUMN id SET DEFAULT nextval('component_cultivo_id_seq'::regclass);


--
-- Name: cultivo id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY cultivo ALTER COLUMN id SET DEFAULT nextval('cultivo_id_seq'::regclass);


--
-- Name: detaile_cultivo id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_cultivo ALTER COLUMN id SET DEFAULT nextval('detaile_cultivo_id_seq'::regclass);


--
-- Name: detaile_device id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_device ALTER COLUMN id SET DEFAULT nextval('detaile_device_id_seq'::regclass);


--
-- Name: device id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY device ALTER COLUMN id SET DEFAULT nextval('device_id_seq'::regclass);


--
-- Name: measure id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY measure ALTER COLUMN id SET DEFAULT nextval('measure_id_seq'::regclass);


--
-- Name: plant id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY plant ALTER COLUMN id SET DEFAULT nextval('plant_id_seq'::regclass);


--
-- Name: recipe id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY recipe ALTER COLUMN id SET DEFAULT nextval('recipe_id_seq'::regclass);


--
-- Name: specie id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY specie ALTER COLUMN id SET DEFAULT nextval('specie_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY "user" ALTER COLUMN id SET DEFAULT nextval('user_id_seq'::regclass);


--
-- Name: user_cultivo id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY user_cultivo ALTER COLUMN id SET DEFAULT nextval('user_cultivo_id_seq'::regclass);


--
-- Name: varieti id; Type: DEFAULT; Schema: public; Owner: pi
--

ALTER TABLE ONLY varieti ALTER COLUMN id SET DEFAULT nextval('varieti_id_seq'::regclass);


--
-- Data for Name: action; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY action (id, date_created, time_action, cultivo_id, component_id, state, accion) FROM stdin;
301	2018-09-07 02:37:22.005612	2018-09-07 02:37:22.011909	1	6	1	0
302	2018-09-07 02:37:24.796834	2018-09-07 02:37:24.80319	1	6	1	1
303	2018-09-07 02:37:31.251499	2018-09-07 02:37:31.260727	1	7	1	0
304	2018-09-07 02:37:34.087926	2018-09-07 02:37:34.094261	1	8	1	0
305	2018-09-07 02:37:35.082135	2018-09-07 02:37:35.088387	1	5	1	0
306	2018-09-07 02:37:36.74059	2018-09-07 02:37:36.746716	1	6	1	0
307	2018-09-07 02:38:24.067267	2018-09-07 02:38:24.073688	1	6	1	1
308	2018-09-07 02:44:10.624941	2018-09-07 02:44:10.631929	1	6	1	0
309	2018-09-07 03:38:15.567283	2018-09-07 03:38:15.573719	1	6	1	1
310	2018-09-07 03:38:19.107897	2018-09-07 03:38:19.114434	1	7	1	1
311	2018-09-07 03:38:22.794621	2018-09-07 03:38:22.801543	1	8	1	1
325	2018-09-07 03:42:37.033194	2018-09-07 03:42:37.039616	1	8	1	0
326	2018-09-07 03:42:38.31959	2018-09-07 03:42:38.326082	1	6	1	1
328	2018-09-07 03:50:30.276458	2018-09-07 03:50:30.28287	1	7	1	0
330	2018-09-07 03:50:33.151728	2018-09-07 03:50:33.157855	1	5	1	0
354	2018-09-08 22:34:02.964711	2018-09-08 22:34:02.970848	1	7	1	1
355	2018-09-08 22:34:05.285802	2018-09-08 22:34:05.291959	1	8	1	1
356	2018-09-08 22:34:06.701567	2018-09-08 22:34:06.707743	1	5	1	1
357	2018-09-08 22:34:13.881641	2018-09-08 22:34:13.887852	1	6	1	1
358	2018-09-08 22:34:14.589496	2018-09-08 22:34:14.592945	1	6	1	0
359	2018-09-08 22:34:15.562649	2018-09-08 22:34:15.568819	1	5	1	0
360	2018-09-08 22:34:16.364554	2018-09-08 22:34:16.368055	1	5	1	1
361	2018-09-08 22:34:17.213174	2018-09-08 22:34:17.219295	1	8	1	0
362	2018-09-08 22:34:18.071424	2018-09-08 22:34:18.07764	1	7	1	0
363	2018-09-08 22:34:41.646908	2018-09-08 22:34:41.653021	1	6	1	1
364	2018-09-08 22:34:56.337854	2018-09-08 22:34:56.344004	1	5	1	0
365	2018-09-08 22:34:57.269407	2018-09-08 22:34:57.275997	1	6	1	0
366	2018-09-08 22:37:48.808281	2018-09-08 22:37:48.814576	1	8	1	1
371	2018-09-08 23:11:28.091946	2018-09-08 23:11:28.09807	1	6	1	1
373	2018-09-08 23:11:31.7431	2018-09-08 23:11:31.749264	1	8	1	0
375	2018-09-08 23:11:33.767635	2018-09-08 23:11:33.773702	1	7	1	1
381	2018-09-08 23:25:52.813945	2018-09-08 23:25:52.835451	1	6	1	0
379	2018-09-08 23:25:48.833448	2018-09-08 23:25:48.847147	1	8	1	1
383	2018-09-08 23:27:55.072903	2018-09-08 23:27:55.086513	1	7	1	1
386	2018-09-08 23:29:03.270373	2018-09-08 23:29:03.28462	1	8	1	1
389	2018-09-09 00:52:47.854215	2018-09-09 00:52:47.867687	1	6	1	0
391	2018-09-09 00:53:42.704945	2018-09-09 00:53:42.724873	1	5	1	1
393	2018-09-09 00:53:45.883527	2018-09-09 00:53:45.903261	1	7	1	0
395	2018-09-09 00:54:08.970923	2018-09-09 00:54:08.990372	1	6	1	0
397	2018-09-09 00:54:11.470616	2018-09-09 00:54:11.484119	1	7	1	1
398	2018-09-09 00:55:12.275923	2018-09-09 00:55:12.289437	1	6	1	1
399	2018-09-09 00:57:31.671166	2018-09-09 00:57:31.685741	1	6	1	0
401	2018-09-09 00:57:44.631226	2018-09-09 00:57:44.644931	1	8	1	0
403	2018-09-09 00:58:00.185033	2018-09-09 00:58:00.198609	1	6	1	0
406	2018-09-09 01:05:04.497923	2018-09-09 01:05:04.514466	1	8	1	1
408	2018-09-09 01:05:11.281239	2018-09-09 01:05:11.31216	1	6	1	1
433	2018-09-09 03:50:37.235866	2018-09-09 03:50:37.260098	1	5	1	0
436	2018-09-09 03:50:59.022019	2018-09-09 03:50:59.042536	1	6	1	1
438	2018-09-09 03:52:39.571946	2018-09-09 03:52:39.592025	1	6	1	0
440	2018-09-09 03:52:42.867519	2018-09-09 03:52:42.881271	1	7	1	0
442	2018-09-09 03:52:45.836744	2018-09-09 03:52:45.854938	1	6	1	1
444	2018-09-09 03:52:47.743976	2018-09-09 03:52:47.757715	1	7	1	1
446	2018-09-09 03:52:55.894134	2018-09-09 03:52:55.918788	1	8	1	0
452	2018-09-09 04:17:18.600795	2018-09-09 04:17:18.614696	1	5	1	0
454	2018-09-09 04:17:19.933725	2018-09-09 04:17:19.956704	1	6	1	1
450	2018-09-09 04:17:16.777923	2018-09-09 04:17:16.813824	1	7	1	0
447	2018-09-09 04:15:35.531982	2018-09-09 04:15:35.560897	1	7	1	0
448	2018-09-09 04:17:13.368846	2018-09-09 04:17:13.389729	1	8	1	1
455	2018-09-09 04:20:49.922907	2018-09-09 04:20:49.959829	1	6	1	0
457	2018-09-09 04:20:53.167233	2018-09-09 04:20:53.187946	1	8	1	1
459	2018-09-09 04:20:55.452494	2018-09-09 04:20:55.466483	1	6	1	1
461	2018-09-09 04:20:59.052692	2018-09-09 04:20:59.095398	1	5	1	0
463	2018-09-09 04:21:00.891213	2018-09-09 04:21:00.905007	1	7	1	0
465	2018-09-09 04:21:10.090615	2018-09-09 04:21:10.128341	1	5	1	0
467	2018-09-09 04:21:14.510243	2018-09-09 04:21:14.523749	1	5	1	1
469	2018-09-09 04:21:16.675087	2018-09-09 04:21:16.69409	1	7	1	1
471	2018-09-09 17:39:21.652334	2018-09-09 17:39:21.675151	1	5	1	0
473	2018-09-09 17:47:10.834908	2018-09-09 17:47:10.850947	1	5	1	1
475	2018-09-09 17:51:43.547198	2018-09-09 17:51:43.575387	1	5	1	1
477	2018-09-09 17:52:18.562657	2018-09-09 17:52:18.5792	1	8	1	1
479	2018-09-09 17:52:22.166549	2018-09-09 17:52:22.186733	1	5	1	0
481	2018-09-09 17:52:25.199771	2018-09-09 17:52:25.213568	1	7	1	1
483	2018-09-09 17:52:31.036678	2018-09-09 17:52:31.050286	1	5	1	1
485	2018-09-09 17:52:36.002159	2018-09-09 17:52:36.021741	1	6	1	1
487	2018-09-09 17:52:39.661469	2018-09-09 17:52:39.675579	1	7	1	1
489	2018-09-09 17:52:41.95293	2018-09-09 17:52:41.972157	1	5	1	1
491	2018-09-09 17:52:44.301232	2018-09-09 17:52:44.314848	1	8	1	1
494	2018-09-09 17:53:11.161515	2018-09-09 17:53:11.175582	1	5	1	0
496	2018-09-09 17:53:13.291856	2018-09-09 17:53:13.311393	1	6	1	0
498	2018-09-09 17:59:04.523756	2018-09-09 17:59:04.551429	1	5	1	1
500	2018-09-09 17:59:09.250468	2018-09-09 17:59:09.270399	1	6	1	1
502	2018-09-16 20:56:41.820162	2018-09-16 20:56:41.833868	1	5	1	0
505	2018-09-16 20:57:22.634946	2018-09-16 20:57:22.656008	1	7	1	1
506	2018-09-16 20:59:29.90736	2018-09-16 20:59:29.927099	1	8	1	0
508	2018-09-16 21:01:36.839063	2018-09-16 21:01:36.858513	1	5	1	0
510	2018-09-16 21:01:39.149955	2018-09-16 21:01:39.169354	1	6	1	1
512	2018-09-16 21:01:46.884362	2018-09-16 21:01:46.906104	1	6	1	1
514	2018-09-16 21:01:49.229263	2018-09-16 21:01:49.242775	1	5	1	1
516	2018-09-16 21:14:16.018577	2018-09-16 21:14:16.038184	1	6	1	0
518	2018-09-16 21:14:17.746668	2018-09-16 21:14:17.77489	1	5	1	0
520	2018-09-16 23:13:43.950031	2018-09-16 23:13:43.970064	1	6	1	1
522	2018-09-16 23:13:47.723548	2018-09-16 23:13:47.743827	1	5	1	1
524	2018-09-16 23:13:59.659689	2018-09-16 23:13:59.678252	1	7	1	0
526	2018-09-16 23:14:01.384218	2018-09-16 23:14:01.403298	1	8	1	0
528	2018-09-16 23:14:06.816628	2018-09-16 23:14:06.830368	1	6	1	1
530	2018-09-16 23:14:08.72275	2018-09-16 23:14:08.740676	1	8	1	1
532	2018-09-16 23:14:10.785991	2018-09-16 23:14:10.799658	1	5	1	1
533	2018-09-16 23:20:58.197331	2018-09-16 23:20:58.210753	1	5	1	0
535	2018-09-16 23:21:00.447174	2018-09-16 23:21:00.480224	1	6	1	0
537	2018-09-16 23:21:09.696479	2018-09-16 23:21:09.730084	1	8	1	1
539	2018-09-16 23:21:11.255381	2018-09-16 23:21:11.273293	1	7	1	1
541	2018-09-16 23:21:18.239125	2018-09-16 23:21:18.252465	1	7	1	1
543	2018-09-16 23:21:22.58345	2018-09-16 23:21:22.615215	1	7	1	1
545	2018-09-23 23:11:19.429833	2018-09-23 23:11:19.445755	1	6	1	0
312	2018-09-07 03:38:25.809207	2018-09-07 03:38:25.815694	1	5	1	1
313	2018-09-07 03:39:50.973512	2018-09-07 03:39:50.980068	1	6	1	0
314	2018-09-07 03:39:56.178035	2018-09-07 03:39:56.184261	1	5	1	0
315	2018-09-07 03:40:59.889018	2018-09-07 03:40:59.895177	1	8	1	0
316	2018-09-07 03:41:03.287848	2018-09-07 03:41:03.293977	1	7	1	0
317	2018-09-07 03:41:06.636153	2018-09-07 03:41:06.649952	1	6	1	0
318	2018-09-07 03:41:07.848409	2018-09-07 03:41:07.854567	1	8	1	1
319	2018-09-07 03:41:11.295759	2018-09-07 03:41:11.301934	1	5	1	0
320	2018-09-07 03:41:14.921261	2018-09-07 03:41:14.927338	1	8	1	0
321	2018-09-07 03:41:17.568787	2018-09-07 03:41:17.57504	1	5	1	1
322	2018-09-07 03:41:20.198529	2018-09-07 03:41:20.204647	1	5	1	0
323	2018-09-07 03:42:28.649533	2018-09-07 03:42:28.65568	1	7	1	0
324	2018-09-07 03:42:35.511383	2018-09-07 03:42:35.517423	1	6	1	0
331	2018-09-07 03:53:03.533892	2018-09-07 03:53:03.540239	1	6	1	1
333	2018-09-07 03:53:45.684969	2018-09-07 03:53:45.691136	1	6	1	0
334	2018-09-07 03:54:05.221217	2018-09-07 03:54:05.227267	1	7	1	0
335	2018-09-07 03:54:06.612658	2018-09-07 03:54:06.618683	1	6	1	0
336	2018-09-07 03:57:31.812543	2018-09-07 03:57:31.818593	1	5	1	0
337	2018-09-07 03:57:32.643314	2018-09-07 03:57:32.657169	1	8	1	0
340	2018-09-07 03:59:27.580939	2018-09-07 03:59:27.586942	1	8	1	1
341	2018-09-07 03:59:37.998183	2018-09-07 03:59:38.004519	1	7	1	0
351	2018-09-07 04:28:45.318945	2018-09-07 04:28:45.325104	1	5	1	0
350	2018-09-07 04:28:41.121317	2018-09-07 04:28:41.127372	1	5	1	1
349	2018-09-07 04:28:36.074295	2018-09-07 04:28:36.080396	1	5	1	0
348	2018-09-07 04:28:30.291235	2018-09-07 04:28:30.29736	1	5	1	1
352	2018-09-08 22:33:23.411104	2018-09-08 22:33:23.417476	1	6	1	1
367	2018-09-08 22:37:52.283378	2018-09-08 22:37:52.289801	1	7	1	1
368	2018-09-08 22:38:15.435346	2018-09-08 22:38:15.441528	1	5	1	1
369	2018-09-08 22:38:17.29095	2018-09-08 22:38:17.297211	1	5	1	0
370	2018-09-08 22:39:21.001395	2018-09-08 22:39:21.00755	1	5	1	1
372	2018-09-08 23:11:30.575979	2018-09-08 23:11:30.582158	1	6	1	0
374	2018-09-08 23:11:32.778832	2018-09-08 23:11:32.784903	1	7	1	0
376	2018-09-08 23:11:35.180096	2018-09-08 23:11:35.186265	1	5	1	0
377	2018-09-08 23:11:36.440421	2018-09-08 23:11:36.446597	1	7	1	0
409	2018-09-09 01:08:51.707377	2018-09-09 01:08:51.721224	1	8	1	1
411	2018-09-09 01:10:58.134033	2018-09-09 01:10:58.147582	1	7	1	1
413	2018-09-09 01:11:31.932556	2018-09-09 01:11:31.946602	1	8	1	1
414	2018-09-09 01:12:57.293258	2018-09-09 01:12:57.323016	1	6	1	0
416	2018-09-09 01:13:04.425775	2018-09-09 01:13:04.443176	1	7	1	0
418	2018-09-09 01:13:13.952293	2018-09-09 01:13:13.972546	1	5	1	1
419	2018-09-09 01:15:15.411212	2018-09-09 01:15:15.424729	1	5	1	0
421	2018-09-09 01:15:30.721246	2018-09-09 01:15:30.754387	1	7	1	1
423	2018-09-09 01:15:33.800923	2018-09-09 01:15:33.814523	1	6	1	1
425	2018-09-09 01:16:23.607118	2018-09-09 01:16:23.620949	1	7	1	0
428	2018-09-09 01:23:25.329213	2018-09-09 01:23:25.350302	1	7	1	1
430	2018-09-09 01:23:30.220685	2018-09-09 01:23:30.240445	1	6	1	0
453	2018-09-09 04:17:19.22678	2018-09-09 04:17:19.240456	1	6	1	0
449	2018-09-09 04:17:15.930728	2018-09-09 04:17:15.950564	1	7	1	1
451	2018-09-09 04:17:17.972326	2018-09-09 04:17:17.995963	1	8	1	0
456	2018-09-09 04:20:51.793339	2018-09-09 04:20:51.808122	1	7	1	1
458	2018-09-09 04:20:53.982508	2018-09-09 04:20:54.007035	1	5	1	1
460	2018-09-09 04:20:58.182512	2018-09-09 04:20:58.196175	1	6	1	0
462	2018-09-09 04:21:00.086188	2018-09-09 04:21:00.106153	1	8	1	0
464	2018-09-09 04:21:06.601907	2018-09-09 04:21:06.615527	1	5	1	1
466	2018-09-09 04:21:10.940044	2018-09-09 04:21:10.959698	1	8	1	1
468	2018-09-09 04:21:15.555937	2018-09-09 04:21:15.569629	1	8	1	0
470	2018-09-09 04:21:19.958091	2018-09-09 04:21:19.97166	1	6	1	1
472	2018-09-09 17:39:23.167926	2018-09-09 17:39:23.187877	1	8	1	1
474	2018-09-09 17:47:13.019599	2018-09-09 17:47:13.034162	1	5	1	0
476	2018-09-09 17:52:14.595674	2018-09-09 17:52:14.609764	1	8	1	0
478	2018-09-09 17:52:20.433399	2018-09-09 17:52:20.44704	1	8	1	0
480	2018-09-09 17:52:23.496168	2018-09-09 17:52:23.516273	1	7	1	0
482	2018-09-09 17:52:29.6871	2018-09-09 17:52:29.710856	1	8	1	1
484	2018-09-09 17:52:33.935267	2018-09-09 17:52:33.948842	1	6	1	0
486	2018-09-09 17:52:38.299637	2018-09-09 17:52:38.321125	1	7	1	0
488	2018-09-09 17:52:40.976452	2018-09-09 17:52:40.996198	1	5	1	0
490	2018-09-09 17:52:43.085331	2018-09-09 17:52:43.0991	1	8	1	0
492	2018-09-09 17:52:46.54333	2018-09-09 17:52:46.557089	1	8	1	0
493	2018-09-09 17:53:09.273834	2018-09-09 17:53:09.29692	1	8	1	1
495	2018-09-09 17:53:12.397026	2018-09-09 17:53:12.414825	1	7	1	0
497	2018-09-09 17:53:14.72821	2018-09-09 17:53:14.741759	1	8	1	0
499	2018-09-09 17:59:08.350317	2018-09-09 17:59:08.370247	1	7	1	1
501	2018-09-09 17:59:10.14848	2018-09-09 17:59:10.168499	1	8	1	1
503	2018-09-16 20:56:49.938016	2018-09-16 20:56:49.951292	1	7	1	0
327	2018-09-07 03:50:25.352533	2018-09-07 03:50:25.358918	1	6	1	0
329	2018-09-07 03:50:31.81883	2018-09-07 03:50:31.824977	1	8	1	0
332	2018-09-07 03:53:07.398559	2018-09-07 03:53:07.404797	1	7	1	1
338	2018-09-07 03:57:55.729743	2018-09-07 03:57:55.735936	1	6	1	1
339	2018-09-07 03:58:37.671395	2018-09-07 03:58:37.677518	1	7	1	1
342	2018-09-07 03:59:43.347312	2018-09-07 03:59:43.353581	1	7	1	1
343	2018-09-07 03:59:52.807328	2018-09-07 03:59:52.813558	1	5	1	1
344	2018-09-07 03:59:57.68577	2018-09-07 03:59:57.692123	1	6	1	0
345	2018-09-07 04:00:04.533267	2018-09-07 04:00:04.53948	1	7	1	0
346	2018-09-07 04:00:05.691915	2018-09-07 04:00:05.698125	1	8	1	0
347	2018-09-07 04:00:07.615512	2018-09-07 04:00:07.624697	1	5	1	0
353	2018-09-08 22:33:25.835164	2018-09-08 22:33:25.842337	1	6	1	0
378	2018-09-08 23:25:44.392593	2018-09-08 23:25:44.415903	1	6	1	1
380	2018-09-08 23:25:51.870581	2018-09-08 23:25:51.890357	1	8	1	0
382	2018-09-08 23:27:48.802089	2018-09-08 23:27:48.815788	1	5	1	1
384	2018-09-08 23:28:12.575512	2018-09-08 23:28:12.590099	1	7	1	0
385	2018-09-08 23:28:27.639819	2018-09-08 23:28:27.673765	1	6	1	1
387	2018-09-08 23:29:17.242356	2018-09-08 23:29:17.260538	1	7	1	1
388	2018-09-09 00:52:44.845551	2018-09-09 00:52:44.86364	1	5	1	0
390	2018-09-09 00:52:57.773861	2018-09-09 00:52:57.789326	1	6	1	1
392	2018-09-09 00:53:44.284552	2018-09-09 00:53:44.298107	1	8	1	0
394	2018-09-09 00:54:07.967765	2018-09-09 00:54:07.987508	1	5	1	0
396	2018-09-09 00:54:10.188119	2018-09-09 00:54:10.207807	1	8	1	1
400	2018-09-09 00:57:33.228166	2018-09-09 00:57:33.241971	1	5	1	1
402	2018-09-09 00:57:51.318556	2018-09-09 00:57:51.332248	1	6	1	1
404	2018-09-09 00:58:37.873725	2018-09-09 00:58:37.887337	1	7	1	0
405	2018-09-09 01:05:02.52058	2018-09-09 01:05:02.534109	1	5	1	0
407	2018-09-09 01:05:08.02403	2018-09-09 01:05:08.03758	1	8	1	0
410	2018-09-09 01:10:47.211976	2018-09-09 01:10:47.231575	1	5	1	1
412	2018-09-09 01:10:59.29236	2018-09-09 01:10:59.310151	1	8	1	0
415	2018-09-09 01:13:03.536284	2018-09-09 01:13:03.549927	1	5	1	0
417	2018-09-09 01:13:10.932324	2018-09-09 01:13:10.949985	1	7	1	1
420	2018-09-09 01:15:16.349295	2018-09-09 01:15:16.362956	1	7	1	0
422	2018-09-09 01:15:32.314241	2018-09-09 01:15:32.333876	1	5	1	1
424	2018-09-09 01:15:48.16934	2018-09-09 01:15:48.185278	1	5	1	0
426	2018-09-09 01:16:25.225917	2018-09-09 01:16:25.245462	1	8	1	0
427	2018-09-09 01:23:23.606735	2018-09-09 01:23:23.620684	1	5	1	1
429	2018-09-09 01:23:27.508768	2018-09-09 01:23:27.526572	1	8	1	1
432	2018-09-09 03:50:32.995064	2018-09-09 03:50:33.01505	1	6	1	0
431	2018-09-09 03:49:06.484995	2018-09-09 03:49:06.508081	1	6	1	1
435	2018-09-09 03:50:57.384172	2018-09-09 03:50:57.404214	1	7	1	1
434	2018-09-09 03:50:39.428144	2018-09-09 03:50:39.449682	1	7	1	0
437	2018-09-09 03:52:36.539317	2018-09-09 03:52:36.55297	1	5	1	1
439	2018-09-09 03:52:41.633495	2018-09-09 03:52:41.646847	1	5	1	0
441	2018-09-09 03:52:44.041679	2018-09-09 03:52:44.061322	1	8	1	0
443	2018-09-09 03:52:46.651014	2018-09-09 03:52:46.670617	1	5	1	1
445	2018-09-09 03:52:50.093063	2018-09-09 03:52:50.128018	1	8	1	1
504	2018-09-16 20:57:21.51712	2018-09-16 20:57:21.530608	1	5	1	1
507	2018-09-16 20:59:30.995494	2018-09-16 20:59:31.009179	1	6	1	0
509	2018-09-16 21:01:37.868104	2018-09-16 21:01:37.887407	1	7	1	0
511	2018-09-16 21:01:44.332822	2018-09-16 21:01:44.362166	1	6	1	0
513	2018-09-16 21:01:48.293476	2018-09-16 21:01:48.307709	1	7	1	1
515	2018-09-16 21:01:50.302039	2018-09-16 21:01:50.322279	1	8	1	1
517	2018-09-16 21:14:16.77347	2018-09-16 21:14:16.786784	1	8	1	0
519	2018-09-16 21:14:18.457442	2018-09-16 21:14:18.477002	1	7	1	0
521	2018-09-16 23:13:45.454747	2018-09-16 23:13:45.485733	1	8	1	1
523	2018-09-16 23:13:49.35015	2018-09-16 23:13:49.364047	1	7	1	1
525	2018-09-16 23:14:00.633419	2018-09-16 23:14:00.652768	1	5	1	0
527	2018-09-16 23:14:02.176504	2018-09-16 23:14:02.209893	1	6	1	0
529	2018-09-16 23:14:07.584664	2018-09-16 23:14:07.598074	1	5	1	1
531	2018-09-16 23:14:09.40115	2018-09-16 23:14:09.42072	1	5	1	0
534	2018-09-16 23:20:59.515425	2018-09-16 23:20:59.529166	1	8	1	0
536	2018-09-16 23:21:08.774573	2018-09-16 23:21:08.788001	1	6	1	1
538	2018-09-16 23:21:10.344924	2018-09-16 23:21:10.359322	1	5	1	1
540	2018-09-16 23:21:15.899341	2018-09-16 23:21:15.918876	1	7	1	0
542	2018-09-16 23:21:21.411312	2018-09-16 23:21:21.425185	1	7	1	0
544	2018-09-16 23:21:23.722822	2018-09-16 23:21:23.736769	1	7	1	0
299	2018-09-07 02:27:30.794037	2018-09-07 02:27:30.802996	1	6	1	0
300	2018-09-07 02:34:30.463269	2018-09-07 02:34:30.469469	1	5	1	1
\.


--
-- Name: action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('action_id_seq', 545, true);


--
-- Data for Name: book; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY book (id, name, date_created, recipe_id) FROM stdin;
\.


--
-- Name: book_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('book_id_seq', 1, false);


--
-- Data for Name: ciclo_vegetativo; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY ciclo_vegetativo (id, name, date_created, plant_id) FROM stdin;
\.


--
-- Name: ciclo_vegetativo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('ciclo_vegetativo_id_seq', 1, false);


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY component (id, name, date_created, model, marca, "precision", variable, function, state, num_pin) FROM stdin;
8	LUZ	\N	foco	\N	\N	LUZ	action	1	3
5	INTRACTOR	\N	ventilador	\N	\N	intra	action	1	0
3	YL69	\N	yl69	\N	\N	HUM_SOIL	sensor	1	\N
7	BOMBA_AGUA	\N	bomba	\N	\N	riego	action	0	2
2	DHT22	\N	dht22	\N	\N	HUM	sensor	0	\N
6	EXTRACTOR	\N	ventilador	\N	\N	extra	action	0	1
1	DHT22	\N	dht22	\N	\N	TEM	sensor	0	\N
4	FOTO_CELDA	\N	celda	\N	\N	LUZ	sensor	0	\N
20	DHT22_2	\N	dht22	\N	\N	TEM	sensor	0	\N
21	DHT22_2	\N	dht22	\N	\N	HUM	sensor	0	\N
22	YL69_2	\N	yl69	\N	\N	HUM_SOIL	sensor	0	\N
23	FOTO_CELDA_2	\N	celda	\N	\N	LUZ	sensor	0	\N
18	BOMBA_AGUA_2	\N	bomba	\N	\N	riego	action	0	\N
17	INTRACTOR_2	\N	ventilador	\N	\N	intra	action	0	\N
15	EXTRACTOR_2	\N	ventilador	\N	\N	extra	action	0	\N
19	LUZ_2	\N	foco	\N	\N	LUZ	action	1	\N
16	INTRACTOR_2	\N	ventilador	\N	\N	intra	action	0	\N
\.


--
-- Data for Name: component_cultivo; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY component_cultivo (id, name, date_created, cultivo_id, component_id, state) FROM stdin;
45	\N	\N	1	1	0
46	\N	\N	1	2	0
47	\N	\N	1	3	0
48	\N	\N	1	4	0
49	\N	\N	1	5	0
50	\N	\N	1	6	0
51	\N	\N	1	7	0
52	\N	\N	1	8	0
53	\N	\N	2	15	0
54	\N	\N	2	16	0
55	\N	\N	2	17	0
56	\N	\N	2	18	0
57	\N	\N	2	19	0
58	\N	\N	2	20	0
59	\N	\N	2	21	0
60	\N	\N	2	22	0
61	\N	\N	2	23	0
\.


--
-- Name: component_cultivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('component_cultivo_id_seq', 61, true);


--
-- Name: component_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('component_id_seq', 23, true);


--
-- Data for Name: cultivo; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY cultivo (id, name, date_created, hight, lenght, type_cultivo) FROM stdin;
1	Cultivo_1	\N	\N	\N	tierra
2	Cultivo_2	\N	\N	\N	tierra
\.


--
-- Name: cultivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('cultivo_id_seq', 2, true);


--
-- Data for Name: detaile_cultivo; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY detaile_cultivo (id, name, date_created, cultivo_id, plant_id, cantidad) FROM stdin;
\.


--
-- Name: detaile_cultivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('detaile_cultivo_id_seq', 1, false);


--
-- Data for Name: detaile_device; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY detaile_device (id, name, date_created, device_id, component_id) FROM stdin;
\.


--
-- Name: detaile_device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('detaile_device_id_seq', 1, false);


--
-- Data for Name: device; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY device (id, name, date_created, model, marca, function) FROM stdin;
1	node	\N	nodemcu	\N	\N
\.


--
-- Name: device_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('device_id_seq', 1, true);


--
-- Data for Name: huerta; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY huerta (id, name, date_created, hight, lenght, type_huerta) FROM stdin;
\.


--
-- Data for Name: measure; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY measure (id, date_created, cultivo_id, component_id, variable, value) FROM stdin;
1	2018-08-20 18:58:25.085276	1	4	LUZ	0.00
2	2018-08-20 18:58:34.959421	1	1	TEM	20.30
3	2018-08-20 18:58:45.028631	1	4	LUZ	0.00
4	2018-08-20 19:17:03.566837	1	2	HUM	29.30
5	2018-08-20 19:17:13.639278	1	3	HUM_SOIL	99.00
6	2018-08-20 19:17:23.689566	1	4	LUZ	0.00
7	2018-08-20 19:17:33.735041	1	1	TEM	20.50
8	2018-08-20 19:24:13.93842	1	1	TEM	20.40
9	2018-08-20 19:24:24.006039	1	2	HUM	29.20
10	2018-08-20 19:24:34.050815	1	3	HUM_SOIL	84.00
11	2018-08-20 19:24:44.095581	1	4	LUZ	0.00
12	2018-08-20 19:24:54.139997	1	1	TEM	20.40
13	2018-08-20 19:25:04.184102	1	2	HUM	29.20
14	2018-08-20 19:25:14.230318	1	3	HUM_SOIL	1.00
15	2018-08-20 19:25:24.273973	1	4	LUZ	0.00
16	2018-08-20 19:25:34.318142	1	1	TEM	20.40
17	2018-08-20 19:25:44.362038	1	2	HUM	29.20
18	2018-08-20 19:25:54.398374	1	3	HUM_SOIL	94.00
19	2018-08-20 19:26:04.444473	1	4	LUZ	0.00
20	2018-08-20 19:26:14.488695	1	1	TEM	20.40
21	2018-08-20 19:26:24.529913	1	2	HUM	29.10
22	2018-08-20 19:26:34.573869	1	3	HUM_SOIL	99.00
23	2018-08-20 19:26:44.617465	1	4	LUZ	0.00
24	2018-08-20 19:26:54.662585	1	1	TEM	20.40
25	2018-08-20 19:27:04.706513	1	2	HUM	29.20
26	2018-08-20 19:27:14.750132	1	3	HUM_SOIL	1.00
27	2018-08-20 19:27:24.794623	1	4	LUZ	0.00
28	2018-08-20 19:27:34.838098	1	1	TEM	20.30
29	2018-08-20 19:27:43.664098	1	2	HUM	29.10
30	2018-08-20 19:27:44.881801	1	3	HUM_SOIL	89.00
31	2018-08-20 19:27:53.31415	2	1	TEM	nan
32	2018-08-20 19:27:53.729739	1	4	LUZ	0.00
33	2018-08-20 19:27:54.074859	2	2	HUM	nan
34	2018-08-20 19:27:54.924222	1	1	TEM	20.30
35	2018-08-20 19:27:55.101748	2	3	HUM_SOIL	0.00
36	2018-08-20 19:28:02.3467	2	4	LUZ	1.00
37	2018-08-20 19:28:03.370436	2	1	TEM	nan
38	2018-08-20 19:28:03.774353	1	2	HUM	29.10
39	2018-08-20 19:28:04.394427	2	2	HUM	nan
40	2018-08-20 19:28:04.968029	1	3	HUM_SOIL	1.00
41	2018-08-20 19:28:05.42237	2	3	HUM_SOIL	0.00
42	2018-08-20 19:28:12.721447	2	4	LUZ	1.00
43	2018-08-20 19:28:13.745441	2	1	TEM	nan
44	2018-08-20 19:28:13.813868	1	4	LUZ	0.00
45	2018-08-20 19:28:14.769394	2	2	HUM	nan
46	2018-08-20 19:28:15.011629	1	1	TEM	20.40
47	2018-08-20 19:28:15.787246	2	3	HUM_SOIL	0.00
48	2018-08-20 19:28:23.094389	2	4	LUZ	1.00
49	2018-08-20 19:28:23.8589	1	2	HUM	29.20
50	2018-08-20 19:28:24.118535	2	1	TEM	nan
51	2018-08-20 19:28:25.055285	1	3	HUM_SOIL	1.00
52	2018-08-20 19:28:25.142296	2	2	HUM	nan
53	2018-08-20 19:28:26.16634	2	3	HUM_SOIL	0.00
54	2018-08-20 19:28:33.46475	2	4	LUZ	1.00
55	2018-08-20 19:28:33.909033	1	4	LUZ	0.00
56	2018-08-20 19:28:34.488585	2	1	TEM	20.50
57	2018-08-20 19:28:35.098981	1	1	TEM	20.30
58	2018-08-20 19:28:35.497133	2	2	HUM	29.30
59	2018-08-20 19:28:36.505939	2	3	HUM_SOIL	0.00
60	2018-08-20 19:28:43.832952	2	4	LUZ	0.00
61	2018-08-20 19:28:43.960052	1	2	HUM	29.20
62	2018-08-20 19:28:44.840857	2	1	TEM	20.50
63	2018-08-20 19:28:45.142605	1	3	HUM_SOIL	99.00
64	2018-08-20 19:28:45.848162	2	2	HUM	29.50
65	2018-08-20 19:28:46.870798	2	3	HUM_SOIL	0.00
66	2018-08-20 19:28:54.009465	1	4	LUZ	0.00
67	2018-08-20 19:28:54.20024	2	4	LUZ	0.00
68	2018-08-20 19:28:55.188887	1	1	TEM	20.40
69	2018-08-20 19:28:55.223962	2	1	TEM	20.50
70	2018-08-20 19:28:56.248003	2	2	HUM	30.90
71	2018-08-20 19:28:57.272033	2	3	HUM_SOIL	0.00
72	2018-08-20 19:29:04.053668	1	2	HUM	29.20
73	2018-08-20 19:29:04.569486	2	4	LUZ	0.00
74	2018-08-20 19:29:05.224465	1	3	HUM_SOIL	1.00
75	2018-08-20 19:29:05.577173	2	1	TEM	20.50
76	2018-08-20 19:29:06.584435	2	2	HUM	29.00
77	2018-08-20 19:29:07.591987	2	3	HUM_SOIL	0.00
78	2018-08-20 19:29:14.103932	1	4	LUZ	0.00
79	2018-08-20 19:29:14.94135	2	4	LUZ	0.00
80	2018-08-20 19:29:15.26778	1	1	TEM	20.40
81	2018-08-20 19:29:15.965108	2	1	TEM	20.50
82	2018-08-20 19:29:16.988915	2	2	HUM	29.20
83	2018-08-20 19:29:18.012099	2	3	HUM_SOIL	0.00
84	2018-08-20 19:29:24.148292	1	2	HUM	29.20
85	2018-08-20 19:29:25.311771	1	3	HUM_SOIL	99.00
86	2018-08-20 19:29:25.32354	2	4	LUZ	0.00
87	2018-08-20 19:29:26.346933	2	1	TEM	20.50
88	2018-08-20 19:29:27.370801	2	2	HUM	29.00
89	2018-08-20 19:29:28.394781	2	3	HUM_SOIL	0.00
90	2018-08-20 19:29:34.199425	1	4	LUZ	0.00
91	2018-08-20 19:29:35.357914	1	1	TEM	20.40
92	2018-08-20 19:29:35.675302	2	4	LUZ	0.00
93	2018-08-20 19:29:36.699276	2	1	TEM	20.40
94	2018-08-20 19:29:37.722034	2	2	HUM	29.00
95	2018-08-20 19:29:38.746053	2	3	HUM_SOIL	0.00
96	2018-08-20 19:29:44.246452	1	2	HUM	29.10
97	2018-08-20 19:29:45.401944	1	3	HUM_SOIL	99.00
98	2018-08-20 19:29:46.047073	2	4	LUZ	0.00
99	2018-08-20 19:29:47.071082	2	1	TEM	20.50
100	2018-08-20 19:29:48.099304	2	2	HUM	29.00
101	2018-08-20 19:29:49.123099	2	3	HUM_SOIL	0.00
102	2018-08-20 19:29:54.296998	1	4	LUZ	0.00
103	2018-08-20 19:29:55.447825	1	1	TEM	20.40
104	2018-08-20 19:29:56.412736	2	4	LUZ	0.00
105	2018-08-20 19:29:57.436531	2	1	TEM	20.40
106	2018-08-20 19:29:58.460257	2	2	HUM	28.90
107	2018-08-20 19:29:59.484076	2	3	HUM_SOIL	0.00
108	2018-08-20 19:30:04.338089	1	2	HUM	29.00
109	2018-08-20 19:30:05.491779	1	3	HUM_SOIL	1.00
110	2018-08-20 19:30:06.782764	2	4	LUZ	0.00
111	2018-08-20 19:30:07.806721	2	1	TEM	20.50
112	2018-08-20 19:30:08.830533	2	2	HUM	29.20
113	2018-08-20 19:30:09.854354	2	3	HUM_SOIL	0.00
114	2018-08-20 19:30:14.379634	1	4	LUZ	0.00
115	2018-08-20 19:30:15.53605	1	1	TEM	20.40
116	2018-08-20 19:30:17.148064	2	4	LUZ	0.00
117	2018-08-20 19:30:18.171746	2	1	TEM	20.40
118	2018-08-20 19:30:19.195765	2	2	HUM	29.10
119	2018-08-20 19:30:20.219787	2	3	HUM_SOIL	0.00
120	2018-08-20 19:30:24.432353	1	2	HUM	29.00
121	2018-08-20 19:30:25.579663	1	3	HUM_SOIL	96.00
122	2018-08-20 19:30:27.514114	2	4	LUZ	0.00
123	2018-08-20 19:30:28.538303	2	1	TEM	20.40
124	2018-08-20 19:30:29.562071	2	2	HUM	29.10
125	2018-08-20 19:30:30.586145	2	3	HUM_SOIL	0.00
128	2018-08-20 19:30:37.899127	2	4	LUZ	0.00
129	2018-08-20 19:30:38.922218	2	1	TEM	20.40
130	2018-08-20 19:30:39.946049	2	2	HUM	29.00
131	2018-08-20 19:30:40.969768	2	3	HUM_SOIL	0.00
126	2018-08-20 19:30:34.473378	1	4	LUZ	0.00
127	2018-08-20 19:30:35.625887	1	1	TEM	20.40
132	2018-08-20 19:30:44.521806	1	2	HUM	29.00
133	2018-08-20 19:30:45.670578	1	3	HUM_SOIL	1.00
134	2018-08-20 19:30:48.249758	2	4	LUZ	0.00
135	2018-08-20 19:30:49.273482	2	1	TEM	20.40
136	2018-08-20 19:30:50.29753	2	2	HUM	29.00
137	2018-08-20 19:30:51.321299	2	3	HUM_SOIL	0.00
138	2018-08-20 19:30:54.569548	1	4	LUZ	0.00
139	2018-08-20 19:30:55.716573	1	1	TEM	20.50
140	2018-08-20 19:30:58.26586	2	4	LUZ	0.00
141	2018-08-20 19:31:04.604401	1	2	HUM	29.00
142	2018-08-20 19:31:05.754814	1	3	HUM_SOIL	92.00
143	2018-08-20 19:31:14.651768	1	4	LUZ	0.00
144	2018-08-20 19:31:15.798741	1	1	TEM	20.50
145	2018-08-20 19:31:24.694702	1	2	HUM	29.00
146	2018-08-20 19:31:25.846622	1	3	HUM_SOIL	1.00
147	2018-08-20 19:31:34.738062	1	4	LUZ	0.00
148	2018-08-20 19:31:35.887939	1	1	TEM	20.50
149	2018-08-20 19:31:44.788424	1	2	HUM	29.00
150	2018-08-20 19:31:45.929036	1	3	HUM_SOIL	99.00
151	2018-08-20 19:34:27.238358	2	1	TEM	20.30
152	2018-08-20 19:34:27.324266	2	1	TEM	20.30
153	2018-08-20 19:34:28.035582	2	2	HUM	29.00
154	2018-08-20 19:34:28.069723	2	2	HUM	29.00
155	2018-08-20 19:34:29.063226	2	3	HUM_SOIL	0.00
156	2018-08-20 19:34:29.102365	2	3	HUM_SOIL	0.00
157	2018-08-20 19:34:36.380706	2	4	LUZ	0.00
158	2018-08-20 19:34:36.380447	2	4	LUZ	0.00
159	2018-08-20 19:34:37.399335	2	1	TEM	20.30
160	2018-08-20 19:34:37.408519	2	1	TEM	20.30
161	2018-08-20 19:34:38.423509	2	2	HUM	29.00
162	2018-08-20 19:34:38.425521	2	2	HUM	29.00
163	2018-08-20 19:34:38.840215	1	2	HUM	28.90
164	2018-08-20 19:34:39.442154	2	3	HUM_SOIL	0.00
165	2018-08-20 19:34:39.451008	2	3	HUM_SOIL	0.00
166	2018-08-20 19:34:46.73966	2	4	LUZ	0.00
167	2018-08-20 19:34:46.75555	2	4	LUZ	0.00
168	2018-08-20 19:34:47.749749	2	1	TEM	20.30
169	2018-08-20 19:34:47.789834	2	1	TEM	20.30
170	2018-08-20 19:34:47.979076	2	1	TEM	20.30
171	2018-08-20 19:34:48.757536	2	2	HUM	29.00
172	2018-08-20 19:34:48.78276	2	2	HUM	29.00
173	2018-08-20 19:34:48.800418	2	2	HUM	29.00
174	2018-08-20 19:34:48.893868	1	3	HUM_SOIL	1.00
175	2018-08-20 19:34:49.781172	2	3	HUM_SOIL	0.00
176	2018-08-20 19:34:49.788861	2	3	HUM_SOIL	0.00
177	2018-08-20 19:34:49.806423	2	3	HUM_SOIL	0.00
178	2018-08-20 19:34:57.156734	2	4	LUZ	0.00
179	2018-08-20 19:34:57.156734	2	4	LUZ	0.00
180	2018-08-20 19:34:57.172439	2	4	LUZ	0.00
181	2018-08-20 19:34:58.167192	2	1	TEM	20.30
182	2018-08-20 19:34:58.179586	2	1	TEM	20.30
183	2018-08-20 19:34:58.187725	2	1	TEM	20.30
184	2018-08-20 19:34:58.930867	1	4	LUZ	0.00
185	2018-08-20 19:34:59.174931	2	2	HUM	29.00
186	2018-08-20 19:34:59.201774	2	2	HUM	29.00
187	2018-08-20 19:34:59.208588	2	2	HUM	29.00
188	2018-08-20 19:35:00.18254	2	3	HUM_SOIL	0.00
189	2018-08-20 19:35:00.230122	2	3	HUM_SOIL	0.00
190	2018-08-20 19:35:00.231544	2	3	HUM_SOIL	0.00
191	2018-08-20 19:35:07.481906	2	4	LUZ	0.00
192	2018-08-20 19:35:07.481906	2	4	LUZ	0.00
193	2018-08-20 19:35:07.498846	2	4	LUZ	0.00
194	2018-08-20 19:35:08.502736	2	1	TEM	20.20
195	2018-08-20 19:35:08.519221	2	1	TEM	20.20
196	2018-08-20 19:35:08.521954	2	1	TEM	20.20
197	2018-08-20 19:35:08.979016	1	1	TEM	20.40
198	2018-08-20 19:35:09.537233	2	2	HUM	29.00
199	2018-08-20 19:35:09.537233	2	2	HUM	29.00
200	2018-08-20 19:35:09.543882	2	2	HUM	29.00
201	2018-08-20 19:35:10.549397	2	3	HUM_SOIL	0.00
202	2018-08-20 19:35:10.556711	2	3	HUM_SOIL	0.00
203	2018-08-20 19:35:10.571909	2	3	HUM_SOIL	0.00
204	2018-08-20 19:35:17.844968	2	4	LUZ	0.00
205	2018-08-20 19:35:17.844968	2	4	LUZ	0.00
206	2018-08-20 19:35:17.851867	2	4	LUZ	0.00
207	2018-08-20 19:35:18.872529	2	1	TEM	20.30
208	2018-08-20 19:35:18.872529	2	1	TEM	20.30
209	2018-08-20 19:35:18.879415	2	1	TEM	20.30
210	2018-08-20 19:35:19.018115	1	2	HUM	29.00
211	2018-08-20 19:35:19.892487	2	2	HUM	29.60
212	2018-08-20 19:35:19.903849	2	2	HUM	29.60
213	2018-08-20 19:35:19.913723	2	2	HUM	29.60
214	2018-08-20 19:35:20.915591	2	3	HUM_SOIL	0.00
215	2018-08-20 19:35:20.921987	2	3	HUM_SOIL	0.00
216	2018-08-20 19:35:20.938918	2	3	HUM_SOIL	0.00
218	2018-08-20 19:35:28.216045	2	4	LUZ	0.00
217	2018-08-20 19:35:28.216395	2	4	LUZ	0.00
219	2018-08-20 19:35:28.217896	2	4	LUZ	0.00
220	2018-08-20 19:35:29.067841	1	3	HUM_SOIL	92.00
221	2018-08-20 19:35:29.224045	2	1	TEM	20.30
222	2018-08-20 19:35:29.238764	2	1	TEM	20.30
223	2018-08-20 19:35:29.253099	2	1	TEM	20.30
224	2018-08-20 19:35:30.231494	2	2	HUM	29.10
225	2018-08-20 19:35:30.274505	2	2	HUM	29.10
226	2018-08-20 19:35:30.289208	2	2	HUM	29.10
227	2018-08-20 19:35:31.238406	2	3	HUM_SOIL	0.00
228	2018-08-20 19:35:31.306032	2	3	HUM_SOIL	0.00
229	2018-08-20 19:35:31.307252	2	3	HUM_SOIL	0.00
230	2018-08-20 19:35:38.583951	2	4	LUZ	0.00
231	2018-08-20 19:35:38.589968	2	4	LUZ	0.00
232	2018-08-20 19:35:38.595102	2	4	LUZ	0.00
233	2018-08-20 19:35:39.110359	1	4	LUZ	0.00
234	2018-08-20 19:35:39.597813	2	1	TEM	20.20
235	2018-08-20 19:35:39.602797	2	1	TEM	20.20
236	2018-08-20 19:35:39.618821	2	1	TEM	20.20
237	2018-08-20 19:35:40.609978	2	2	HUM	29.10
238	2018-08-20 19:35:40.6149	2	2	HUM	29.10
239	2018-08-20 19:35:40.651961	2	2	HUM	29.10
240	2018-08-20 19:35:41.618378	2	3	HUM_SOIL	0.00
241	2018-08-20 19:35:41.63273	2	3	HUM_SOIL	0.00
242	2018-08-20 19:35:41.674843	2	3	HUM_SOIL	0.00
243	2018-08-20 19:35:48.948828	2	4	LUZ	0.00
244	2018-08-20 19:35:48.948828	2	4	LUZ	0.00
245	2018-08-20 19:35:48.968675	2	4	LUZ	0.00
246	2018-08-20 19:35:49.159329	1	1	TEM	20.40
247	2018-08-20 19:35:49.957552	2	1	TEM	20.20
248	2018-08-20 19:35:49.982827	2	1	TEM	20.20
249	2018-08-20 19:35:50.000813	2	1	TEM	20.20
250	2018-08-20 19:35:50.96431	2	2	HUM	29.10
251	2018-08-20 19:35:51.006051	2	2	HUM	29.10
254	2018-08-20 19:35:52.028758	2	3	HUM_SOIL	0.00
259	2018-08-20 19:35:59.317151	2	4	LUZ	0.00
260	2018-08-20 19:36:00.328298	2	1	TEM	20.20
264	2018-08-20 19:36:01.352013	2	2	HUM	29.20
267	2018-08-20 19:36:02.370981	2	3	HUM_SOIL	0.00
271	2018-08-20 19:36:09.717655	2	4	LUZ	0.00
275	2018-08-20 19:36:10.73709	2	1	TEM	20.20
278	2018-08-20 19:36:11.770691	2	2	HUM	29.30
281	2018-08-20 19:36:12.805212	2	3	HUM_SOIL	0.00
283	2018-08-20 19:36:20.053755	2	4	LUZ	0.00
287	2018-08-20 19:36:21.08724	2	1	TEM	20.20
290	2018-08-20 19:36:22.105279	2	2	HUM	29.30
293	2018-08-20 19:36:23.113884	2	3	HUM_SOIL	0.00
298	2018-08-20 19:36:30.440104	2	4	LUZ	0.00
301	2018-08-20 19:36:31.469208	2	1	TEM	20.20
303	2018-08-20 19:36:32.488289	2	2	HUM	29.20
305	2018-08-20 19:36:33.506037	2	3	HUM_SOIL	0.00
310	2018-08-20 19:36:40.790155	2	4	LUZ	0.00
312	2018-08-20 19:36:41.79782	2	1	TEM	20.20
315	2018-08-20 19:36:42.804946	2	2	HUM	29.30
318	2018-08-20 19:36:43.822901	2	3	HUM_SOIL	0.00
252	2018-08-20 19:35:51.009737	2	2	HUM	29.10
255	2018-08-20 19:35:52.033177	2	3	HUM_SOIL	0.00
257	2018-08-20 19:35:59.317491	2	4	LUZ	0.00
261	2018-08-20 19:36:00.333114	2	1	TEM	20.20
263	2018-08-20 19:36:01.348824	2	2	HUM	29.20
266	2018-08-20 19:36:02.356451	2	3	HUM_SOIL	0.00
270	2018-08-20 19:36:09.701329	2	4	LUZ	0.00
273	2018-08-20 19:36:10.709115	2	1	TEM	20.20
276	2018-08-20 19:36:11.716042	2	2	HUM	29.30
279	2018-08-20 19:36:12.743892	2	3	HUM_SOIL	0.00
284	2018-08-20 19:36:20.061228	2	4	LUZ	0.00
286	2018-08-20 19:36:21.068003	2	1	TEM	20.20
289	2018-08-20 19:36:22.075104	2	2	HUM	29.30
292	2018-08-20 19:36:23.098733	2	3	HUM_SOIL	0.00
297	2018-08-20 19:36:30.428344	2	4	LUZ	0.00
300	2018-08-20 19:36:31.457758	2	1	TEM	20.20
304	2018-08-20 19:36:32.494163	2	2	HUM	29.20
307	2018-08-20 19:36:33.527173	2	3	HUM_SOIL	0.00
309	2018-08-20 19:36:40.790155	2	4	LUZ	0.00
314	2018-08-20 19:36:41.816586	2	1	TEM	20.20
316	2018-08-20 19:36:42.822642	2	2	HUM	29.30
253	2018-08-20 19:35:51.972379	2	3	HUM_SOIL	0.00
258	2018-08-20 19:35:59.317336	2	4	LUZ	0.00
262	2018-08-20 19:36:00.332477	2	1	TEM	20.20
265	2018-08-20 19:36:01.368882	2	2	HUM	29.20
268	2018-08-20 19:36:02.390037	2	3	HUM_SOIL	0.00
272	2018-08-20 19:36:09.721732	2	4	LUZ	0.00
274	2018-08-20 19:36:10.73023	2	1	TEM	20.20
277	2018-08-20 19:36:11.752169	2	2	HUM	29.30
280	2018-08-20 19:36:12.76393	2	3	HUM_SOIL	0.00
285	2018-08-20 19:36:20.06462	2	4	LUZ	0.00
288	2018-08-20 19:36:21.097171	2	1	TEM	20.20
291	2018-08-20 19:36:22.126071	2	2	HUM	29.30
294	2018-08-20 19:36:23.134409	2	3	HUM_SOIL	0.00
296	2018-08-20 19:36:30.421543	2	4	LUZ	0.00
299	2018-08-20 19:36:31.444139	2	1	TEM	20.20
302	2018-08-20 19:36:32.477127	2	2	HUM	29.20
306	2018-08-20 19:36:33.511738	2	3	HUM_SOIL	0.00
311	2018-08-20 19:36:40.790166	2	4	LUZ	0.00
313	2018-08-20 19:36:41.808094	2	1	TEM	20.20
317	2018-08-20 19:36:42.837243	2	2	HUM	29.30
256	2018-08-20 19:35:59.208941	1	2	HUM	29.00
269	2018-08-20 19:36:09.256253	1	3	HUM_SOIL	82.00
282	2018-08-20 19:36:19.29613	1	4	LUZ	0.00
295	2018-08-20 19:36:29.338992	1	1	TEM	20.30
308	2018-08-20 19:36:39.374074	1	2	HUM	29.00
319	2018-08-20 19:36:43.830068	2	3	HUM_SOIL	0.00
320	2018-08-20 19:36:43.852937	2	3	HUM_SOIL	0.00
321	2018-08-20 19:36:49.413607	1	3	HUM_SOIL	48.00
322	2018-08-20 19:36:51.16977	2	4	LUZ	0.00
323	2018-08-20 19:36:51.178128	2	4	LUZ	0.00
324	2018-08-20 19:36:51.186262	2	4	LUZ	0.00
325	2018-08-20 19:36:52.193246	2	1	TEM	20.20
326	2018-08-20 19:36:52.203829	2	1	TEM	20.20
327	2018-08-20 19:36:52.219717	2	1	TEM	20.20
328	2018-08-20 19:36:53.221739	2	2	HUM	29.30
329	2018-08-20 19:36:53.222869	2	2	HUM	29.30
330	2018-08-20 19:36:53.238393	2	2	HUM	29.30
331	2018-08-20 19:36:54.246153	2	3	HUM_SOIL	0.00
332	2018-08-20 19:36:54.246153	2	3	HUM_SOIL	0.00
333	2018-08-20 19:36:54.261747	2	3	HUM_SOIL	0.00
334	2018-08-20 19:36:59.453877	1	4	LUZ	0.00
335	2018-08-20 19:37:01.525892	2	4	LUZ	0.00
336	2018-08-20 19:37:01.526123	2	4	LUZ	0.00
337	2018-08-20 19:37:01.543244	2	4	LUZ	0.00
338	2018-08-20 19:37:02.533512	2	1	TEM	20.20
339	2018-08-20 19:37:02.533512	2	1	TEM	20.20
340	2018-08-20 19:37:02.574709	2	1	TEM	20.20
341	2018-08-20 19:37:03.541081	2	2	HUM	29.30
342	2018-08-20 19:37:03.541081	2	2	HUM	29.30
343	2018-08-20 19:37:03.597539	2	2	HUM	29.30
344	2018-08-20 19:37:04.548214	2	3	HUM_SOIL	0.00
345	2018-08-20 19:37:04.548214	2	3	HUM_SOIL	0.00
346	2018-08-20 19:37:04.620335	2	3	HUM_SOIL	0.00
347	2018-08-20 19:37:09.502041	1	1	TEM	20.30
348	2018-08-20 19:37:11.899572	2	4	LUZ	0.00
349	2018-08-20 19:37:11.899192	2	4	LUZ	0.00
350	2018-08-20 19:37:11.915015	2	4	LUZ	0.00
351	2018-08-20 19:37:12.918157	2	1	TEM	20.20
352	2018-08-20 19:37:12.933199	2	1	TEM	20.20
353	2018-08-20 19:37:12.948386	2	1	TEM	20.20
354	2018-08-20 19:37:13.940118	2	2	HUM	29.30
355	2018-08-20 19:37:13.950902	2	2	HUM	29.30
356	2018-08-20 19:37:13.957474	2	2	HUM	29.30
357	2018-08-20 19:37:14.947637	2	3	HUM_SOIL	0.00
358	2018-08-20 19:37:14.981603	2	3	HUM_SOIL	0.00
359	2018-08-20 19:37:14.989426	2	3	HUM_SOIL	0.00
360	2018-08-20 19:37:19.543786	1	2	HUM	29.00
361	2018-08-20 19:37:22.264161	2	4	LUZ	0.00
362	2018-08-20 19:37:22.264725	2	4	LUZ	0.00
363	2018-08-20 19:37:22.264161	2	4	LUZ	0.00
364	2018-08-20 19:37:23.271434	2	1	TEM	20.20
365	2018-08-20 19:37:23.271434	2	1	TEM	20.20
366	2018-08-20 19:37:23.271562	2	1	TEM	20.20
368	2018-08-20 19:37:24.279541	2	2	HUM	29.30
367	2018-08-20 19:37:24.279175	2	2	HUM	29.30
369	2018-08-20 19:37:24.279678	2	2	HUM	29.30
370	2018-08-20 19:37:25.286874	2	3	HUM_SOIL	0.00
371	2018-08-20 19:37:25.286874	2	3	HUM_SOIL	0.00
372	2018-08-20 19:37:25.286874	2	3	HUM_SOIL	0.00
373	2018-08-20 19:37:29.583963	1	3	HUM_SOIL	1.00
374	2018-08-20 19:37:32.630854	2	4	LUZ	0.00
375	2018-08-20 19:37:32.639685	2	4	LUZ	0.00
376	2018-08-20 19:37:32.647067	2	4	LUZ	0.00
377	2018-08-20 19:37:33.638195	2	1	TEM	20.20
378	2018-08-20 19:37:33.658574	2	1	TEM	20.20
379	2018-08-20 19:37:33.66029	2	1	TEM	20.20
380	2018-08-20 19:37:34.662512	2	2	HUM	29.40
381	2018-08-20 19:37:34.684061	2	2	HUM	29.40
382	2018-08-20 19:37:34.691239	2	2	HUM	29.40
383	2018-08-20 19:37:35.689154	2	3	HUM_SOIL	0.00
384	2018-08-20 19:37:35.69996	2	3	HUM_SOIL	0.00
385	2018-08-20 19:37:35.710166	2	3	HUM_SOIL	0.00
386	2018-08-20 19:37:39.623814	1	4	LUZ	0.00
387	2018-08-20 19:37:42.999047	2	4	LUZ	0.00
388	2018-08-20 19:37:42.99906	2	4	LUZ	0.00
389	2018-08-20 19:37:43.012804	2	4	LUZ	0.00
390	2018-08-20 19:37:44.006552	2	1	TEM	20.20
391	2018-08-20 19:37:44.012129	2	1	TEM	20.20
392	2018-08-20 19:37:44.028521	2	1	TEM	20.20
393	2018-08-20 19:37:45.035236	2	2	HUM	29.40
394	2018-08-20 19:37:45.036682	2	2	HUM	29.40
395	2018-08-20 19:37:45.060717	2	2	HUM	29.40
396	2018-08-20 19:37:46.043255	2	3	HUM_SOIL	0.00
397	2018-08-20 19:37:46.063068	2	3	HUM_SOIL	0.00
398	2018-08-20 19:37:46.091485	2	3	HUM_SOIL	0.00
399	2018-08-20 19:37:49.673511	1	1	TEM	20.30
400	2018-08-20 19:37:53.383469	2	4	LUZ	0.00
401	2018-08-20 19:37:53.383692	2	4	LUZ	0.00
402	2018-08-20 19:37:53.394431	2	4	LUZ	0.00
403	2018-08-20 19:37:54.390454	2	1	TEM	20.20
404	2018-08-20 19:37:54.391622	2	1	TEM	20.20
405	2018-08-20 19:37:54.422888	2	1	TEM	20.20
406	2018-08-20 19:37:55.398013	2	2	HUM	29.40
407	2018-08-20 19:37:55.398012	2	2	HUM	29.40
408	2018-08-20 19:37:55.445463	2	2	HUM	29.40
409	2018-08-20 19:37:56.420908	2	3	HUM_SOIL	0.00
410	2018-08-20 19:37:56.42205	2	3	HUM_SOIL	0.00
411	2018-08-20 19:37:56.473395	2	3	HUM_SOIL	0.00
412	2018-08-20 19:37:59.717895	1	2	HUM	29.00
413	2018-08-20 19:38:03.740022	2	4	LUZ	0.00
414	2018-08-20 19:38:03.737526	2	4	LUZ	0.00
415	2018-08-20 19:38:03.751728	2	4	LUZ	0.00
416	2018-08-20 19:38:04.745026	2	1	TEM	20.20
417	2018-08-20 19:38:04.769691	2	1	TEM	20.20
418	2018-08-20 19:38:04.785066	2	1	TEM	20.20
419	2018-08-20 19:38:05.752208	2	2	HUM	29.30
420	2018-08-20 19:38:05.792951	2	2	HUM	29.30
421	2018-08-20 19:38:05.81816	2	2	HUM	29.30
422	2018-08-20 19:38:06.759798	2	3	HUM_SOIL	0.00
423	2018-08-20 19:38:06.822019	2	3	HUM_SOIL	0.00
424	2018-08-20 19:38:06.837503	2	3	HUM_SOIL	0.00
425	2018-08-20 19:38:09.759161	1	3	HUM_SOIL	2.00
426	2018-08-20 19:38:14.106151	2	4	LUZ	0.00
427	2018-08-20 19:38:14.107976	2	4	LUZ	0.00
428	2018-08-20 19:38:14.119468	2	4	LUZ	0.00
429	2018-08-20 19:38:15.129546	2	1	TEM	20.20
430	2018-08-20 19:38:15.136737	2	1	TEM	20.20
431	2018-08-20 19:38:15.136737	2	1	TEM	20.20
432	2018-08-20 19:38:16.154275	2	2	HUM	29.30
433	2018-08-20 19:38:16.154515	2	2	HUM	29.30
434	2018-08-20 19:38:16.160248	2	2	HUM	29.30
436	2018-08-20 19:38:17.175148	2	3	HUM_SOIL	0.00
440	2018-08-20 19:38:24.48629	2	4	LUZ	0.00
442	2018-08-20 19:38:25.495177	2	1	TEM	20.20
445	2018-08-20 19:38:26.523492	2	2	HUM	29.30
448	2018-08-20 19:38:27.529622	2	3	HUM_SOIL	0.00
453	2018-08-20 19:38:34.847515	2	4	LUZ	0.00
455	2018-08-20 19:38:35.854842	2	1	TEM	20.20
459	2018-08-20 19:38:36.86251	2	2	HUM	29.20
462	2018-08-20 19:38:37.870103	2	3	HUM_SOIL	0.00
435	2018-08-20 19:38:17.179179	2	3	HUM_SOIL	0.00
441	2018-08-20 19:38:24.485254	2	4	LUZ	0.00
444	2018-08-20 19:38:25.517599	2	1	TEM	20.20
446	2018-08-20 19:38:26.531755	2	2	HUM	29.30
449	2018-08-20 19:38:27.541075	2	3	HUM_SOIL	0.00
452	2018-08-20 19:38:34.847515	2	4	LUZ	0.00
456	2018-08-20 19:38:35.854842	2	1	TEM	20.20
458	2018-08-20 19:38:36.862509	2	2	HUM	29.20
461	2018-08-20 19:38:37.870103	2	3	HUM_SOIL	0.00
437	2018-08-20 19:38:17.187511	2	3	HUM_SOIL	0.00
439	2018-08-20 19:38:24.485254	2	4	LUZ	0.00
443	2018-08-20 19:38:25.518294	2	1	TEM	20.20
447	2018-08-20 19:38:26.538037	2	2	HUM	29.30
450	2018-08-20 19:38:27.563521	2	3	HUM_SOIL	0.00
454	2018-08-20 19:38:34.870611	2	4	LUZ	0.00
457	2018-08-20 19:38:35.894916	2	1	TEM	20.20
460	2018-08-20 19:38:36.918837	2	2	HUM	29.20
463	2018-08-20 19:38:37.946304	2	3	HUM_SOIL	0.00
438	2018-08-20 19:38:19.808215	1	4	LUZ	0.00
451	2018-08-20 19:38:29.850543	1	1	TEM	20.30
464	2018-08-20 19:38:39.90165	1	2	HUM	29.10
465	2018-08-20 19:38:45.214951	2	4	LUZ	0.00
466	2018-08-20 19:38:45.222572	2	4	LUZ	0.00
467	2018-08-20 19:38:45.222572	2	4	LUZ	0.00
468	2018-08-20 19:38:46.238457	2	1	TEM	20.20
469	2018-08-20 19:38:46.247567	2	1	TEM	20.20
470	2018-08-20 19:38:46.254029	2	1	TEM	20.20
471	2018-08-20 19:38:47.24545	2	2	HUM	29.80
472	2018-08-20 19:38:47.269362	2	2	HUM	29.80
473	2018-08-20 19:38:47.298323	2	2	HUM	29.80
474	2018-08-20 19:38:48.25289	2	3	HUM_SOIL	0.00
475	2018-08-20 19:38:48.291793	2	3	HUM_SOIL	0.00
476	2018-08-20 19:38:48.327119	2	3	HUM_SOIL	0.00
477	2018-08-20 19:38:49.948439	1	3	HUM_SOIL	1.00
478	2018-08-20 19:38:55.57724	2	4	LUZ	0.00
479	2018-08-20 19:38:55.586122	2	4	LUZ	0.00
480	2018-08-20 19:38:55.602961	2	4	LUZ	0.00
481	2018-08-20 19:38:56.584417	2	1	TEM	20.20
482	2018-08-20 19:38:56.595462	2	1	TEM	20.20
483	2018-08-20 19:38:56.612304	2	1	TEM	20.20
484	2018-08-20 19:38:57.59233	2	2	HUM	29.30
485	2018-08-20 19:38:57.620768	2	2	HUM	29.30
486	2018-08-20 19:38:57.621078	2	2	HUM	29.30
487	2018-08-20 19:38:58.620324	2	3	HUM_SOIL	0.00
488	2018-08-20 19:38:58.629544	2	3	HUM_SOIL	0.00
489	2018-08-20 19:38:58.650196	2	3	HUM_SOIL	0.00
490	2018-08-20 19:38:59.99138	1	4	LUZ	0.00
491	2018-08-20 19:39:05.945498	2	4	LUZ	0.00
492	2018-08-20 19:39:05.947633	2	4	LUZ	0.00
493	2018-08-20 19:39:05.964084	2	4	LUZ	0.00
494	2018-08-20 19:39:06.954322	2	1	TEM	20.10
495	2018-08-20 19:39:06.956128	2	1	TEM	20.10
496	2018-08-20 19:39:06.972664	2	1	TEM	20.10
497	2018-08-20 19:39:07.961902	2	2	HUM	29.30
498	2018-08-20 19:39:07.979431	2	2	HUM	29.30
499	2018-08-20 19:39:07.98886	2	2	HUM	29.30
500	2018-08-20 19:39:08.985883	2	3	HUM_SOIL	0.00
501	2018-08-20 19:39:09.007419	2	3	HUM_SOIL	0.00
502	2018-08-20 19:39:09.010746	2	3	HUM_SOIL	0.00
503	2018-08-20 19:39:10.035013	1	1	TEM	20.30
504	2018-08-20 19:39:16.314333	2	4	LUZ	0.00
505	2018-08-20 19:39:16.314333	2	4	LUZ	0.00
506	2018-08-20 19:39:16.314346	2	4	LUZ	0.00
507	2018-08-20 19:39:17.322117	2	1	TEM	20.20
508	2018-08-20 19:39:17.322118	2	1	TEM	20.20
509	2018-08-20 19:39:17.322118	2	1	TEM	20.20
510	2018-08-20 19:39:18.330626	2	2	HUM	29.50
511	2018-08-20 19:39:18.329398	2	2	HUM	29.50
512	2018-08-20 19:39:18.35166	2	2	HUM	29.50
513	2018-08-20 19:39:19.336225	2	3	HUM_SOIL	0.00
514	2018-08-20 19:39:19.350264	2	3	HUM_SOIL	0.00
515	2018-08-20 19:39:19.356277	2	3	HUM_SOIL	0.00
516	2018-08-20 19:39:20.073812	1	2	HUM	29.10
517	2018-08-20 19:39:26.684048	2	4	LUZ	0.00
518	2018-08-20 19:39:26.686285	2	4	LUZ	0.00
519	2018-08-20 19:39:26.692811	2	4	LUZ	0.00
520	2018-08-20 19:39:27.692612	2	1	TEM	20.10
521	2018-08-20 19:39:27.717768	2	1	TEM	20.10
522	2018-08-20 19:39:27.715264	2	1	TEM	20.10
523	2018-08-20 19:39:28.715024	2	2	HUM	29.70
524	2018-08-20 19:39:28.724	2	2	HUM	29.70
525	2018-08-20 19:39:28.739708	2	2	HUM	29.70
526	2018-08-20 19:39:29.723025	2	3	HUM_SOIL	0.00
527	2018-08-20 19:39:29.732028	2	3	HUM_SOIL	0.00
528	2018-08-20 19:39:29.761987	2	3	HUM_SOIL	0.00
529	2018-08-20 19:39:30.113994	1	3	HUM_SOIL	84.00
531	2018-08-20 19:39:37.051934	2	4	LUZ	0.00
530	2018-08-20 19:39:37.051934	2	4	LUZ	0.00
532	2018-08-20 19:39:37.055608	2	4	LUZ	0.00
533	2018-08-20 19:39:38.059368	2	1	TEM	20.10
534	2018-08-20 19:39:38.061159	2	1	TEM	20.10
535	2018-08-20 19:39:38.065017	2	1	TEM	20.10
536	2018-08-20 19:39:39.06655	2	2	HUM	29.60
537	2018-08-20 19:39:39.068486	2	2	HUM	29.60
538	2018-08-20 19:39:39.07991	2	2	HUM	29.60
539	2018-08-20 19:39:40.07392	2	3	HUM_SOIL	0.00
540	2018-08-20 19:39:40.07392	2	3	HUM_SOIL	0.00
541	2018-08-20 19:39:40.113968	2	3	HUM_SOIL	0.00
542	2018-08-20 19:39:40.154798	1	4	LUZ	0.00
543	2018-08-20 19:39:47.421148	2	4	LUZ	0.00
544	2018-08-20 19:39:47.421148	2	4	LUZ	0.00
545	2018-08-20 19:39:47.437056	2	4	LUZ	0.00
546	2018-08-20 19:39:48.43895	2	1	TEM	20.10
547	2018-08-20 19:39:48.445774	2	1	TEM	20.10
548	2018-08-20 19:39:48.463891	2	1	TEM	20.10
549	2018-08-20 19:39:49.454281	2	2	HUM	29.50
550	2018-08-20 19:39:49.468988	2	2	HUM	29.50
551	2018-08-20 19:39:49.488345	2	2	HUM	29.50
552	2018-08-20 19:39:50.194736	1	1	TEM	20.30
553	2018-08-20 19:39:50.485932	2	3	HUM_SOIL	0.00
554	2018-08-20 19:39:50.486288	2	3	HUM_SOIL	0.00
555	2018-08-20 19:39:50.520278	2	3	HUM_SOIL	0.00
556	2018-08-20 19:39:57.790795	2	4	LUZ	0.00
557	2018-08-20 19:39:57.795773	2	4	LUZ	0.00
558	2018-08-20 19:39:57.810059	2	4	LUZ	0.00
559	2018-08-20 19:39:58.796275	2	1	TEM	20.10
560	2018-08-20 19:39:58.815295	2	1	TEM	20.10
561	2018-08-20 19:39:58.82209	2	1	TEM	20.10
562	2018-08-20 19:39:59.813819	2	2	HUM	29.60
563	2018-08-20 19:39:59.832135	2	2	HUM	29.60
564	2018-08-20 19:39:59.85666	2	2	HUM	29.60
565	2018-08-20 19:40:00.246583	1	2	HUM	29.20
566	2018-08-20 19:40:00.821668	2	3	HUM_SOIL	0.00
567	2018-08-20 19:40:00.861163	2	3	HUM_SOIL	0.00
568	2018-08-20 19:40:00.890502	2	3	HUM_SOIL	0.00
570	2018-08-20 19:40:08.169143	2	4	LUZ	0.00
569	2018-08-20 19:40:08.170188	2	4	LUZ	0.00
571	2018-08-20 19:40:08.169357	2	4	LUZ	0.00
572	2018-08-20 19:40:09.176609	2	1	TEM	20.10
573	2018-08-20 19:40:09.176606	2	1	TEM	20.10
574	2018-08-20 19:40:09.176838	2	1	TEM	20.10
576	2018-08-20 19:40:10.184096	2	2	HUM	30.00
575	2018-08-20 19:40:10.184096	2	2	HUM	30.00
577	2018-08-20 19:40:10.184334	2	2	HUM	30.00
578	2018-08-20 19:40:10.283925	1	3	HUM_SOIL	1.00
579	2018-08-20 19:40:11.191851	2	3	HUM_SOIL	0.00
580	2018-08-20 19:40:11.212455	2	3	HUM_SOIL	0.00
581	2018-08-20 19:40:11.2204	2	3	HUM_SOIL	0.00
582	2018-08-20 19:40:18.52691	2	4	LUZ	0.00
585	2018-08-20 19:40:19.534784	2	1	TEM	20.10
589	2018-08-20 19:40:20.541996	2	2	HUM	29.70
592	2018-08-20 19:40:21.56513	2	3	HUM_SOIL	0.00
597	2018-08-20 19:40:28.896918	2	4	LUZ	0.00
600	2018-08-20 19:40:29.926022	2	1	TEM	20.10
604	2018-08-20 19:40:30.957241	2	2	HUM	29.70
607	2018-08-20 19:40:31.980712	2	3	HUM_SOIL	0.00
610	2018-08-20 19:40:39.282426	2	4	LUZ	0.00
612	2018-08-20 19:40:40.30048	2	1	TEM	20.10
616	2018-08-20 19:40:41.323536	2	2	HUM	29.80
620	2018-08-20 19:40:42.341762	2	3	HUM_SOIL	0.00
583	2018-08-20 19:40:18.532939	2	4	LUZ	0.00
586	2018-08-20 19:40:19.540345	2	1	TEM	20.10
590	2018-08-20 19:40:20.548412	2	2	HUM	29.70
593	2018-08-20 19:40:21.566423	2	3	HUM_SOIL	0.00
596	2018-08-20 19:40:28.892572	2	4	LUZ	0.00
598	2018-08-20 19:40:29.901934	2	1	TEM	20.10
602	2018-08-20 19:40:30.908652	2	2	HUM	29.70
605	2018-08-20 19:40:31.917166	2	3	HUM_SOIL	0.00
608	2018-08-20 19:40:39.267533	2	4	LUZ	0.00
611	2018-08-20 19:40:40.284157	2	1	TEM	20.10
617	2018-08-20 19:40:41.318273	2	2	HUM	29.80
618	2018-08-20 19:40:42.327937	2	3	HUM_SOIL	0.00
584	2018-08-20 19:40:18.533903	2	4	LUZ	0.00
587	2018-08-20 19:40:19.556034	2	1	TEM	20.10
591	2018-08-20 19:40:20.584146	2	2	HUM	29.70
594	2018-08-20 19:40:21.61247	2	3	HUM_SOIL	0.00
595	2018-08-20 19:40:28.894858	2	4	LUZ	0.00
599	2018-08-20 19:40:29.920372	2	1	TEM	20.10
603	2018-08-20 19:40:30.934144	2	2	HUM	29.70
606	2018-08-20 19:40:31.962622	2	3	HUM_SOIL	0.00
609	2018-08-20 19:40:39.265542	2	4	LUZ	0.00
613	2018-08-20 19:40:40.300459	2	1	TEM	20.10
615	2018-08-20 19:40:41.316657	2	2	HUM	29.80
619	2018-08-20 19:40:42.335142	2	3	HUM_SOIL	0.00
588	2018-08-20 19:40:20.32397	1	4	LUZ	0.00
601	2018-08-20 19:40:30.363861	1	1	TEM	20.30
614	2018-08-20 19:40:40.40645	1	2	HUM	29.20
621	2018-08-20 19:40:49.639693	2	4	LUZ	0.00
622	2018-08-20 19:40:49.639863	2	4	LUZ	0.00
623	2018-08-20 19:40:49.640061	2	4	LUZ	0.00
624	2018-08-20 19:40:50.453518	1	3	HUM_SOIL	96.00
625	2018-08-20 19:40:50.658285	2	1	TEM	20.10
626	2018-08-20 19:40:50.664933	2	1	TEM	20.10
627	2018-08-20 19:40:50.673834	2	1	TEM	20.10
628	2018-08-20 19:40:51.69222	2	2	HUM	29.70
629	2018-08-20 19:40:51.699006	2	2	HUM	29.70
630	2018-08-20 19:40:51.706148	2	2	HUM	29.70
631	2018-08-20 19:40:52.721976	2	3	HUM_SOIL	0.00
632	2018-08-20 19:40:52.723272	2	3	HUM_SOIL	0.00
633	2018-08-20 19:40:52.727567	2	3	HUM_SOIL	0.00
634	2018-08-20 19:41:00.004067	2	4	LUZ	0.00
635	2018-08-20 19:41:00.010542	2	4	LUZ	0.00
636	2018-08-20 19:41:00.019974	2	4	LUZ	0.00
637	2018-08-20 19:41:00.497448	1	4	LUZ	0.00
638	2018-08-20 19:41:01.011327	2	1	TEM	20.10
639	2018-08-20 19:41:01.019104	2	1	TEM	20.10
640	2018-08-20 19:41:01.040488	2	1	TEM	20.10
641	2018-08-20 19:41:02.018984	2	2	HUM	29.50
642	2018-08-20 19:41:02.042382	2	2	HUM	29.50
643	2018-08-20 19:41:02.069863	2	2	HUM	29.50
644	2018-08-20 19:41:03.046777	2	3	HUM_SOIL	0.00
645	2018-08-20 19:41:03.076918	2	3	HUM_SOIL	0.00
646	2018-08-20 19:41:03.093506	2	3	HUM_SOIL	0.00
647	2018-08-20 19:41:10.382455	2	4	LUZ	0.00
648	2018-08-20 19:41:10.381641	2	4	LUZ	0.00
649	2018-08-20 19:41:10.402029	2	4	LUZ	0.00
650	2018-08-20 19:41:10.533715	1	1	TEM	20.30
651	2018-08-20 19:41:11.389394	2	1	TEM	20.00
652	2018-08-20 19:41:11.389394	2	1	TEM	20.00
653	2018-08-20 19:41:11.412016	2	1	TEM	20.00
654	2018-08-20 19:41:12.407413	2	2	HUM	29.40
655	2018-08-20 19:41:12.41743	2	2	HUM	29.40
656	2018-08-20 19:41:12.42245	2	2	HUM	29.40
657	2018-08-20 19:41:13.425378	2	3	HUM_SOIL	0.00
658	2018-08-20 19:41:13.430947	2	3	HUM_SOIL	0.00
659	2018-08-20 19:41:13.441255	2	3	HUM_SOIL	0.00
660	2018-08-20 19:41:20.573934	1	2	HUM	29.20
661	2018-08-20 19:41:20.736006	2	4	LUZ	0.00
662	2018-08-20 19:41:20.736006	2	4	LUZ	0.00
663	2018-08-20 19:41:20.752269	2	4	LUZ	0.00
664	2018-08-20 19:41:21.743365	2	1	TEM	20.00
665	2018-08-20 19:41:21.750146	2	1	TEM	20.00
666	2018-08-20 19:41:21.77002	2	1	TEM	20.00
667	2018-08-20 19:41:22.750828	2	2	HUM	29.50
668	2018-08-20 19:41:22.777956	2	2	HUM	29.50
669	2018-08-20 19:41:22.78534	2	2	HUM	29.50
670	2018-08-20 19:41:23.773579	2	3	HUM_SOIL	0.00
671	2018-08-20 19:41:23.791871	2	3	HUM_SOIL	0.00
672	2018-08-20 19:41:23.795561	2	3	HUM_SOIL	0.00
673	2018-08-20 19:41:30.614004	1	3	HUM_SOIL	1.00
674	2018-08-20 19:41:31.103172	2	4	LUZ	0.00
675	2018-08-20 19:41:31.104073	2	4	LUZ	0.00
676	2018-08-20 19:41:31.110287	2	4	LUZ	0.00
677	2018-08-20 19:41:32.110635	2	1	TEM	20.00
678	2018-08-20 19:41:32.125823	2	1	TEM	20.00
679	2018-08-20 19:41:32.132887	2	1	TEM	20.00
680	2018-08-20 19:41:33.138399	2	2	HUM	29.50
681	2018-08-20 19:41:33.154491	2	2	HUM	29.50
682	2018-08-20 19:41:33.176319	2	2	HUM	29.50
683	2018-08-20 19:41:34.171774	2	3	HUM_SOIL	0.00
684	2018-08-20 19:41:34.171718	2	3	HUM_SOIL	0.00
685	2018-08-20 19:41:34.202733	2	3	HUM_SOIL	0.00
686	2018-08-20 19:41:40.658728	1	4	LUZ	0.00
687	2018-08-20 19:41:41.472193	2	4	LUZ	0.00
688	2018-08-20 19:41:41.473009	2	4	LUZ	0.00
689	2018-08-20 19:41:41.474803	2	4	LUZ	0.00
690	2018-08-20 19:41:42.482351	2	1	TEM	20.00
691	2018-08-20 19:41:42.482489	2	1	TEM	20.00
692	2018-08-20 19:41:42.489403	2	1	TEM	20.00
693	2018-08-20 19:41:43.498455	2	2	HUM	29.70
694	2018-08-20 19:41:43.506837	2	2	HUM	29.70
695	2018-08-20 19:41:43.519035	2	2	HUM	29.70
696	2018-08-20 19:41:44.50588	2	3	HUM_SOIL	0.00
697	2018-08-20 19:41:44.54018	2	3	HUM_SOIL	0.00
698	2018-08-20 19:41:44.55374	2	3	HUM_SOIL	0.00
699	2018-08-20 19:41:50.695335	1	1	TEM	20.30
700	2018-08-20 19:41:51.838015	2	4	LUZ	0.00
701	2018-08-20 19:41:51.83799	2	4	LUZ	0.00
702	2018-08-20 19:41:51.839693	2	4	LUZ	0.00
703	2018-08-20 19:41:52.845777	2	1	TEM	20.00
704	2018-08-20 19:41:52.847331	2	1	TEM	20.00
705	2018-08-20 19:41:52.863065	2	1	TEM	20.00
706	2018-08-20 19:41:53.853157	2	2	HUM	29.60
707	2018-08-20 19:41:53.869231	2	2	HUM	29.60
708	2018-08-20 19:41:53.876439	2	2	HUM	29.60
709	2018-08-20 19:41:54.860457	2	3	HUM_SOIL	0.00
710	2018-08-20 19:41:54.883872	2	3	HUM_SOIL	0.00
711	2018-08-20 19:41:54.893031	2	3	HUM_SOIL	0.00
712	2018-08-20 19:42:00.733848	1	2	HUM	29.10
713	2018-08-20 19:42:02.21791	2	4	LUZ	0.00
714	2018-08-20 19:42:02.21791	2	4	LUZ	0.00
715	2018-08-20 19:42:02.219253	2	4	LUZ	0.00
716	2018-08-20 19:42:03.225021	2	1	TEM	20.00
717	2018-08-20 19:42:03.232177	2	1	TEM	20.00
718	2018-08-20 19:42:03.248646	2	1	TEM	20.00
719	2018-08-20 19:42:04.232353	2	2	HUM	29.50
720	2018-08-20 19:42:04.256477	2	2	HUM	29.50
721	2018-08-20 19:42:04.271533	2	2	HUM	29.50
722	2018-08-20 19:42:05.255215	2	3	HUM_SOIL	0.00
723	2018-08-20 19:42:05.275395	2	3	HUM_SOIL	0.00
724	2018-08-20 19:42:05.305135	2	3	HUM_SOIL	0.00
725	2018-08-20 19:42:10.779499	1	3	HUM_SOIL	88.00
726	2018-08-20 19:42:12.580104	2	4	LUZ	0.00
727	2018-08-20 19:42:12.580104	2	4	LUZ	0.00
728	2018-08-20 19:42:12.580104	2	4	LUZ	0.00
729	2018-08-20 19:42:13.586184	2	1	TEM	20.00
730	2018-08-20 19:42:13.593141	2	1	TEM	20.00
731	2018-08-20 19:42:13.606645	2	1	TEM	20.00
732	2018-08-20 19:42:14.615113	2	2	HUM	29.60
733	2018-08-20 19:42:14.615113	2	2	HUM	29.60
734	2018-08-20 19:42:14.625712	2	2	HUM	29.60
735	2018-08-20 19:42:15.638469	2	3	HUM_SOIL	0.00
736	2018-08-20 19:42:15.638206	2	3	HUM_SOIL	0.00
737	2018-08-20 19:42:15.661423	2	3	HUM_SOIL	0.00
738	2018-08-20 19:42:20.829927	1	4	LUZ	0.00
751	2018-08-20 19:42:30.874468	1	1	TEM	20.30
764	2018-08-20 19:42:40.91866	1	2	HUM	29.10
739	2018-08-20 19:42:22.94289	2	4	LUZ	0.00
742	2018-08-20 19:42:23.950378	2	1	TEM	20.00
745	2018-08-20 19:42:24.975116	2	2	HUM	29.60
749	2018-08-20 19:42:26.006738	2	3	HUM_SOIL	0.00
753	2018-08-20 19:42:33.312755	2	4	LUZ	0.00
757	2018-08-20 19:42:34.348524	2	1	TEM	20.00
760	2018-08-20 19:42:35.372163	2	2	HUM	29.60
763	2018-08-20 19:42:36.396103	2	3	HUM_SOIL	0.00
766	2018-08-20 19:42:43.679502	2	4	LUZ	0.00
740	2018-08-20 19:42:22.94289	2	4	LUZ	0.00
744	2018-08-20 19:42:23.969781	2	1	TEM	20.00
746	2018-08-20 19:42:24.982059	2	2	HUM	29.60
748	2018-08-20 19:42:26.004825	2	3	HUM_SOIL	0.00
754	2018-08-20 19:42:33.312769	2	4	LUZ	0.00
755	2018-08-20 19:42:34.319052	2	1	TEM	20.00
759	2018-08-20 19:42:35.327909	2	2	HUM	29.60
762	2018-08-20 19:42:36.334272	2	3	HUM_SOIL	0.00
767	2018-08-20 19:42:43.687136	2	4	LUZ	0.00
741	2018-08-20 19:42:22.963006	2	4	LUZ	0.00
743	2018-08-20 19:42:23.967051	2	1	TEM	20.00
747	2018-08-20 19:42:24.99179	2	2	HUM	29.60
750	2018-08-20 19:42:26.017183	2	3	HUM_SOIL	0.00
752	2018-08-20 19:42:33.311399	2	4	LUZ	0.00
756	2018-08-20 19:42:34.31904	2	1	TEM	20.00
758	2018-08-20 19:42:35.326409	2	2	HUM	29.60
761	2018-08-20 19:42:36.333946	2	3	HUM_SOIL	0.00
765	2018-08-20 19:42:43.679251	2	4	LUZ	0.00
768	2018-08-20 19:42:44.694703	2	1	TEM	19.90
769	2018-08-20 19:42:44.701985	2	1	TEM	19.90
770	2018-08-20 19:42:44.702443	2	1	TEM	19.90
771	2018-08-20 19:42:45.710005	2	2	HUM	29.50
772	2018-08-20 19:42:45.717766	2	2	HUM	29.50
773	2018-08-20 19:42:45.73593	2	2	HUM	29.50
774	2018-08-20 19:42:46.741944	2	3	HUM_SOIL	0.00
775	2018-08-20 19:42:46.745234	2	3	HUM_SOIL	0.00
776	2018-08-20 19:42:46.764536	2	3	HUM_SOIL	0.00
777	2018-08-20 19:42:50.964331	1	3	HUM_SOIL	1.00
778	2018-08-20 19:42:54.04537	2	4	LUZ	0.00
779	2018-08-20 19:42:54.045411	2	4	LUZ	0.00
780	2018-08-20 19:42:54.051931	2	4	LUZ	0.00
781	2018-08-20 19:42:55.057465	2	1	TEM	19.90
782	2018-08-20 19:42:55.069594	2	1	TEM	19.90
783	2018-08-20 19:42:55.076064	2	1	TEM	19.90
784	2018-08-20 19:42:56.081448	2	2	HUM	29.50
785	2018-08-20 19:42:56.085626	2	2	HUM	29.50
786	2018-08-20 19:42:56.085629	2	2	HUM	29.50
787	2018-08-20 19:42:57.095126	2	3	HUM_SOIL	0.00
788	2018-08-20 19:42:57.100193	2	3	HUM_SOIL	0.00
789	2018-08-20 19:42:57.11093	2	3	HUM_SOIL	0.00
790	2018-08-20 19:43:01.004305	1	4	LUZ	0.00
791	2018-08-20 19:43:04.415795	2	4	LUZ	0.00
792	2018-08-20 19:43:04.415795	2	4	LUZ	0.00
793	2018-08-20 19:43:04.432104	2	4	LUZ	0.00
794	2018-08-20 19:43:05.433974	2	1	TEM	19.90
795	2018-08-20 19:43:05.452866	2	1	TEM	19.90
796	2018-08-20 19:43:05.466716	2	1	TEM	19.90
797	2018-08-20 19:43:06.458059	2	2	HUM	29.50
798	2018-08-20 19:43:06.461425	2	2	HUM	29.50
799	2018-08-20 19:43:06.500316	2	2	HUM	29.50
800	2018-08-20 19:43:07.477797	2	3	HUM_SOIL	0.00
801	2018-08-20 19:43:07.492282	2	3	HUM_SOIL	0.00
802	2018-08-20 19:43:07.511106	2	3	HUM_SOIL	0.00
803	2018-08-20 19:43:11.054207	1	1	TEM	20.30
804	2018-08-20 19:43:14.785204	2	4	LUZ	0.00
805	2018-08-20 19:43:14.785204	2	4	LUZ	0.00
806	2018-08-20 19:43:14.788668	2	4	LUZ	0.00
807	2018-08-20 19:43:15.79239	2	1	TEM	19.90
808	2018-08-20 19:43:15.794536	2	1	TEM	19.90
809	2018-08-20 19:43:15.81156	2	1	TEM	19.90
810	2018-08-20 19:43:16.799514	2	2	HUM	29.50
811	2018-08-20 19:43:16.806592	2	2	HUM	29.50
812	2018-08-20 19:43:16.820857	2	2	HUM	29.50
813	2018-08-20 19:43:17.805987	2	3	HUM_SOIL	0.00
814	2018-08-20 19:43:17.823706	2	3	HUM_SOIL	0.00
815	2018-08-20 19:43:17.842542	2	3	HUM_SOIL	0.00
816	2018-08-20 19:43:21.094803	1	2	HUM	29.20
819	2018-08-20 19:43:25.156443	2	4	LUZ	0.00
817	2018-08-20 19:43:25.156443	2	4	LUZ	0.00
818	2018-08-20 19:43:25.156443	2	4	LUZ	0.00
820	2018-08-20 19:43:26.167833	2	1	TEM	19.90
821	2018-08-20 19:43:26.173983	2	1	TEM	19.90
822	2018-08-20 19:43:26.185313	2	1	TEM	19.90
823	2018-08-20 19:43:27.187343	2	2	HUM	29.50
824	2018-08-20 19:43:27.203565	2	2	HUM	29.50
825	2018-08-20 19:43:27.205737	2	2	HUM	29.50
826	2018-08-20 19:43:28.194927	2	3	HUM_SOIL	0.00
827	2018-08-20 19:43:28.213766	2	3	HUM_SOIL	0.00
828	2018-08-20 19:43:28.240288	2	3	HUM_SOIL	0.00
829	2018-08-20 19:43:31.134889	1	3	HUM_SOIL	99.00
830	2018-08-20 19:43:35.526897	2	4	LUZ	0.00
831	2018-08-20 19:43:35.526897	2	4	LUZ	0.00
832	2018-08-20 19:43:35.532948	2	4	LUZ	0.00
833	2018-08-20 19:43:36.534229	2	1	TEM	19.90
834	2018-08-20 19:43:36.534229	2	1	TEM	19.90
835	2018-08-20 19:43:36.56115	2	1	TEM	19.90
836	2018-08-20 19:43:37.54253	2	2	HUM	29.50
837	2018-08-20 19:43:37.577049	2	2	HUM	29.50
838	2018-08-20 19:43:37.589984	2	2	HUM	29.50
839	2018-08-20 19:43:38.56917	2	3	HUM_SOIL	0.00
840	2018-08-20 19:43:38.593868	2	3	HUM_SOIL	0.00
841	2018-08-20 19:43:38.61745	2	3	HUM_SOIL	0.00
842	2018-08-20 19:43:41.186092	1	4	LUZ	0.00
843	2018-08-20 19:43:45.890197	2	4	LUZ	0.00
844	2018-08-20 19:43:45.89633	2	4	LUZ	0.00
845	2018-08-20 19:43:45.907929	2	4	LUZ	0.00
846	2018-08-20 19:43:46.897123	2	1	TEM	19.90
847	2018-08-20 19:43:46.903071	2	1	TEM	19.90
848	2018-08-20 19:43:46.938329	2	1	TEM	19.90
849	2018-08-20 19:43:47.9044	2	2	HUM	29.60
850	2018-08-20 19:43:47.920952	2	2	HUM	29.60
851	2018-08-20 19:43:47.962427	2	2	HUM	29.60
852	2018-08-20 19:43:48.911832	2	3	HUM_SOIL	0.00
853	2018-08-20 19:43:48.939025	2	3	HUM_SOIL	0.00
854	2018-08-20 19:43:48.991778	2	3	HUM_SOIL	0.00
855	2018-08-20 19:43:51.237851	1	1	TEM	20.30
856	2018-08-20 19:43:56.258386	2	4	LUZ	0.00
857	2018-08-20 19:43:56.258386	2	4	LUZ	0.00
858	2018-08-20 19:43:56.2584	2	4	LUZ	0.00
859	2018-08-20 19:43:57.267262	2	1	TEM	19.90
860	2018-08-20 19:43:57.281856	2	1	TEM	19.90
861	2018-08-20 19:43:57.297478	2	1	TEM	19.90
862	2018-08-20 19:43:58.275289	2	2	HUM	29.60
863	2018-08-20 19:43:58.309358	2	2	HUM	29.60
864	2018-08-20 19:43:58.325851	2	2	HUM	29.60
865	2018-08-20 19:43:59.283614	2	3	HUM_SOIL	0.00
866	2018-08-20 19:43:59.317472	2	3	HUM_SOIL	0.00
867	2018-08-20 19:43:59.33392	2	3	HUM_SOIL	0.00
868	2018-08-20 19:44:01.283808	1	2	HUM	29.20
869	2018-08-20 19:44:06.629744	2	4	LUZ	0.00
870	2018-08-20 19:44:06.629816	2	4	LUZ	0.00
871	2018-08-20 19:44:06.648489	2	4	LUZ	0.00
872	2018-08-20 19:44:07.640747	2	1	TEM	19.90
873	2018-08-20 19:44:07.644957	2	1	TEM	19.90
874	2018-08-20 19:44:07.663551	2	1	TEM	19.90
875	2018-08-20 19:44:08.649184	2	2	HUM	29.60
876	2018-08-20 19:44:08.655434	2	2	HUM	29.60
877	2018-08-20 19:44:08.695623	2	2	HUM	29.60
878	2018-08-20 19:44:09.661612	2	3	HUM_SOIL	0.00
879	2018-08-20 19:44:09.662884	2	3	HUM_SOIL	0.00
884	2018-08-20 19:44:17.003082	2	4	LUZ	0.00
886	2018-08-20 19:44:18.009516	2	1	TEM	19.90
888	2018-08-20 19:44:19.017096	2	2	HUM	29.60
892	2018-08-20 19:44:20.049848	2	3	HUM_SOIL	0.00
895	2018-08-20 19:44:27.362584	2	4	LUZ	0.00
898	2018-08-20 19:44:28.370604	2	1	TEM	19.90
902	2018-08-20 19:44:29.389493	2	2	HUM	29.60
904	2018-08-20 19:44:30.406472	2	3	HUM_SOIL	0.00
908	2018-08-20 19:44:37.731671	2	4	LUZ	0.00
911	2018-08-20 19:44:38.737512	2	1	TEM	19.90
916	2018-08-20 19:44:39.776503	2	2	HUM	29.60
919	2018-08-20 19:44:40.796109	2	3	HUM_SOIL	0.00
880	2018-08-20 19:44:09.723536	2	3	HUM_SOIL	0.00
883	2018-08-20 19:44:17.000826	2	4	LUZ	0.00
887	2018-08-20 19:44:18.025022	2	1	TEM	19.90
890	2018-08-20 19:44:19.057429	2	2	HUM	29.60
893	2018-08-20 19:44:20.077045	2	3	HUM_SOIL	0.00
897	2018-08-20 19:44:27.362811	2	4	LUZ	0.00
900	2018-08-20 19:44:28.390609	2	1	TEM	19.90
903	2018-08-20 19:44:29.420577	2	2	HUM	29.60
906	2018-08-20 19:44:30.453374	2	3	HUM_SOIL	0.00
909	2018-08-20 19:44:37.738815	2	4	LUZ	0.00
912	2018-08-20 19:44:38.746455	2	1	TEM	19.90
914	2018-08-20 19:44:39.754477	2	2	HUM	29.60
918	2018-08-20 19:44:40.787528	2	3	HUM_SOIL	0.00
881	2018-08-20 19:44:11.324023	1	3	HUM_SOIL	99.00
894	2018-08-20 19:44:21.366894	1	4	LUZ	0.00
907	2018-08-20 19:44:31.409367	1	1	TEM	20.30
920	2018-08-20 19:44:41.453874	1	2	HUM	29.30
882	2018-08-20 19:44:17.000826	2	4	LUZ	0.00
885	2018-08-20 19:44:18.007967	2	1	TEM	19.90
889	2018-08-20 19:44:19.031765	2	2	HUM	29.60
891	2018-08-20 19:44:20.039985	2	3	HUM_SOIL	0.00
896	2018-08-20 19:44:27.362584	2	4	LUZ	0.00
899	2018-08-20 19:44:28.370604	2	1	TEM	19.90
901	2018-08-20 19:44:29.388014	2	2	HUM	29.60
905	2018-08-20 19:44:30.421553	2	3	HUM_SOIL	0.00
910	2018-08-20 19:44:37.746582	2	4	LUZ	0.00
913	2018-08-20 19:44:38.754651	2	1	TEM	19.90
915	2018-08-20 19:44:39.762438	2	2	HUM	29.60
917	2018-08-20 19:44:40.780527	2	3	HUM_SOIL	0.00
921	2018-08-20 19:44:48.102277	2	4	LUZ	0.00
922	2018-08-20 19:44:48.102277	2	4	LUZ	0.00
923	2018-08-20 19:44:48.112766	2	4	LUZ	0.00
924	2018-08-20 19:44:49.108985	2	1	TEM	19.90
925	2018-08-20 19:44:49.108985	2	1	TEM	19.90
926	2018-08-20 19:44:49.146171	2	1	TEM	19.90
927	2018-08-20 19:44:50.116393	2	2	HUM	29.50
928	2018-08-20 19:44:50.116393	2	2	HUM	29.50
929	2018-08-20 19:44:50.169819	2	2	HUM	29.50
930	2018-08-20 19:44:51.135092	2	3	HUM_SOIL	0.00
931	2018-08-20 19:44:51.139549	2	3	HUM_SOIL	0.00
932	2018-08-20 19:44:51.197812	2	3	HUM_SOIL	0.00
933	2018-08-20 19:44:51.497652	1	3	HUM_SOIL	1.00
934	2018-08-20 19:44:58.469353	2	4	LUZ	0.00
935	2018-08-20 19:44:58.476324	2	4	LUZ	0.00
936	2018-08-20 19:44:58.482716	2	4	LUZ	0.00
937	2018-08-20 19:44:59.476256	2	1	TEM	19.90
938	2018-08-20 19:44:59.494759	2	1	TEM	19.90
939	2018-08-20 19:44:59.510179	2	1	TEM	19.90
940	2018-08-20 19:45:00.48401	2	2	HUM	29.50
941	2018-08-20 19:45:00.505122	2	2	HUM	29.50
942	2018-08-20 19:45:00.527256	2	2	HUM	29.50
943	2018-08-20 19:45:01.507765	2	3	HUM_SOIL	0.00
944	2018-08-20 19:45:01.509984	2	3	HUM_SOIL	0.00
945	2018-08-20 19:45:01.537432	1	4	LUZ	0.00
946	2018-08-20 19:45:01.561062	2	3	HUM_SOIL	0.00
947	2018-08-20 19:45:08.842824	2	4	LUZ	0.00
948	2018-08-20 19:45:08.841221	2	4	LUZ	0.00
949	2018-08-20 19:45:08.841221	2	4	LUZ	0.00
950	2018-08-20 19:45:09.850194	2	1	TEM	19.90
951	2018-08-20 19:45:09.849848	2	1	TEM	19.90
952	2018-08-20 19:45:09.874177	2	1	TEM	19.90
953	2018-08-20 19:45:10.866164	2	2	HUM	29.50
954	2018-08-20 19:45:10.87355	2	2	HUM	29.50
955	2018-08-20 19:45:10.882198	2	2	HUM	29.50
956	2018-08-20 19:45:11.573877	1	1	TEM	20.20
957	2018-08-20 19:45:11.889615	2	3	HUM_SOIL	0.00
958	2018-08-20 19:45:11.8971	2	3	HUM_SOIL	0.00
959	2018-08-20 19:45:11.910744	2	3	HUM_SOIL	0.00
960	2018-08-20 19:45:19.205631	2	4	LUZ	0.00
961	2018-08-20 19:45:19.205631	2	4	LUZ	0.00
962	2018-08-20 19:45:19.205366	2	4	LUZ	0.00
963	2018-08-20 19:45:20.21374	2	1	TEM	19.90
964	2018-08-20 19:45:20.213218	2	1	TEM	19.90
965	2018-08-20 19:45:20.220045	2	1	TEM	19.90
966	2018-08-20 19:45:21.219191	2	2	HUM	29.50
967	2018-08-20 19:45:21.235601	2	2	HUM	29.50
968	2018-08-20 19:45:21.247917	2	2	HUM	29.50
969	2018-08-20 19:45:21.615063	1	2	HUM	29.20
970	2018-08-20 19:45:22.242035	2	3	HUM_SOIL	0.00
971	2018-08-20 19:45:22.264248	2	3	HUM_SOIL	0.00
972	2018-08-20 19:45:22.270615	2	3	HUM_SOIL	0.00
973	2018-08-20 19:45:29.578419	2	4	LUZ	0.00
975	2018-08-20 19:45:29.578194	2	4	LUZ	0.00
974	2018-08-20 19:45:29.578194	2	4	LUZ	0.00
976	2018-08-20 19:45:30.59056	2	1	TEM	19.80
977	2018-08-20 19:45:30.59056	2	1	TEM	19.80
978	2018-08-20 19:45:30.597712	2	1	TEM	19.80
979	2018-08-20 19:45:31.608214	2	2	HUM	29.40
980	2018-08-20 19:45:31.615065	2	2	HUM	29.40
981	2018-08-20 19:45:31.631727	2	2	HUM	29.40
982	2018-08-20 19:45:31.654467	1	3	HUM_SOIL	92.00
983	2018-08-20 19:45:32.632101	2	3	HUM_SOIL	0.00
984	2018-08-20 19:45:32.638219	2	3	HUM_SOIL	0.00
985	2018-08-20 19:45:32.646451	2	3	HUM_SOIL	0.00
986	2018-08-20 19:45:39.947391	2	4	LUZ	0.00
987	2018-08-20 19:45:39.94694	2	4	LUZ	0.00
988	2018-08-20 19:45:39.94694	2	4	LUZ	0.00
989	2018-08-20 19:45:40.955756	2	1	TEM	19.80
990	2018-08-20 19:45:40.955649	2	1	TEM	19.80
991	2018-08-20 19:45:40.973442	2	1	TEM	19.80
992	2018-08-20 19:45:41.704773	1	4	LUZ	0.00
993	2018-08-20 19:45:41.974686	2	2	HUM	29.40
994	2018-08-20 19:45:41.998253	2	2	HUM	29.40
995	2018-08-20 19:45:42.006198	2	2	HUM	29.40
996	2018-08-20 19:45:42.995982	2	3	HUM_SOIL	0.00
997	2018-08-20 19:45:43.007643	2	3	HUM_SOIL	0.00
998	2018-08-20 19:45:43.025368	2	3	HUM_SOIL	0.00
999	2018-08-20 19:45:50.314924	2	4	LUZ	0.00
1000	2018-08-20 19:45:50.314925	2	4	LUZ	0.00
1001	2018-08-20 19:45:50.330349	2	4	LUZ	0.00
1002	2018-08-20 19:45:51.335693	2	1	TEM	19.80
1003	2018-08-20 19:45:51.350473	2	1	TEM	19.80
1004	2018-08-20 19:45:51.359542	2	1	TEM	19.80
1005	2018-08-20 19:45:51.749539	1	1	TEM	20.30
1006	2018-08-20 19:45:52.369399	2	2	HUM	29.40
1007	2018-08-20 19:45:52.384831	2	2	HUM	29.40
1008	2018-08-20 19:45:52.39326	2	2	HUM	29.40
1009	2018-08-20 19:45:53.377503	2	3	HUM_SOIL	0.00
1010	2018-08-20 19:45:53.412144	2	3	HUM_SOIL	0.00
1011	2018-08-20 19:45:53.423441	2	3	HUM_SOIL	0.00
1012	2018-08-20 19:46:00.680419	2	4	LUZ	0.00
1013	2018-08-20 19:46:00.687023	2	4	LUZ	0.00
1014	2018-08-20 19:46:00.696276	2	4	LUZ	0.00
1015	2018-08-20 19:46:01.695092	2	1	TEM	19.80
1016	2018-08-20 19:46:01.715681	2	1	TEM	19.80
1017	2018-08-20 19:46:01.734945	2	1	TEM	19.80
1018	2018-08-20 19:46:01.789412	1	2	HUM	29.20
1019	2018-08-20 19:46:02.718182	2	2	HUM	29.50
1020	2018-08-20 19:46:02.732664	2	2	HUM	29.50
1021	2018-08-20 19:46:02.773324	2	2	HUM	29.50
1022	2018-08-20 19:46:03.72576	2	3	HUM_SOIL	0.00
1023	2018-08-20 19:46:03.745615	2	3	HUM_SOIL	0.00
1024	2018-08-20 19:46:03.796233	2	3	HUM_SOIL	0.00
1025	2018-08-20 19:46:11.046348	2	4	LUZ	0.00
1026	2018-08-20 19:46:11.055054	2	4	LUZ	0.00
1027	2018-08-20 19:46:11.066148	2	4	LUZ	0.00
1028	2018-08-20 19:46:11.837401	1	3	HUM_SOIL	81.00
1029	2018-08-20 19:46:12.053295	2	1	TEM	19.80
1032	2018-08-20 19:46:13.075874	2	2	HUM	29.50
1035	2018-08-20 19:46:14.109965	2	3	HUM_SOIL	0.00
1038	2018-08-20 19:46:21.414643	2	4	LUZ	0.00
1043	2018-08-20 19:46:22.440221	2	1	TEM	19.80
1046	2018-08-20 19:46:23.453914	2	2	HUM	29.50
1048	2018-08-20 19:46:24.472188	2	3	HUM_SOIL	0.00
1051	2018-08-20 19:46:31.784703	2	4	LUZ	0.00
1057	2018-08-20 19:46:32.80926	2	1	TEM	19.90
1060	2018-08-20 19:46:33.828671	2	2	HUM	29.50
1063	2018-08-20 19:46:34.864178	2	3	HUM_SOIL	0.00
1066	2018-08-20 19:46:42.158132	2	4	LUZ	0.00
1068	2018-08-20 19:46:43.175652	2	1	TEM	19.90
1030	2018-08-20 19:46:12.075935	2	1	TEM	19.80
1033	2018-08-20 19:46:13.093095	2	2	HUM	29.50
1036	2018-08-20 19:46:14.11316	2	3	HUM_SOIL	0.00
1040	2018-08-20 19:46:21.430577	2	4	LUZ	0.00
1044	2018-08-20 19:46:22.439603	2	1	TEM	19.80
1047	2018-08-20 19:46:23.462064	2	2	HUM	29.50
1050	2018-08-20 19:46:24.480527	2	3	HUM_SOIL	0.00
1052	2018-08-20 19:46:31.788463	2	4	LUZ	0.00
1055	2018-08-20 19:46:32.79636	2	1	TEM	19.90
1059	2018-08-20 19:46:33.819064	2	2	HUM	29.50
1062	2018-08-20 19:46:34.841794	2	3	HUM_SOIL	0.00
1067	2018-08-20 19:46:42.174111	2	4	LUZ	0.00
1070	2018-08-20 19:46:43.197784	2	1	TEM	19.90
1031	2018-08-20 19:46:12.082012	2	1	TEM	19.80
1034	2018-08-20 19:46:13.100903	2	2	HUM	29.50
1037	2018-08-20 19:46:14.109948	2	3	HUM_SOIL	0.00
1039	2018-08-20 19:46:21.41557	2	4	LUZ	0.00
1042	2018-08-20 19:46:22.421814	2	1	TEM	19.80
1045	2018-08-20 19:46:23.445966	2	2	HUM	29.50
1049	2018-08-20 19:46:24.480527	2	3	HUM_SOIL	0.00
1053	2018-08-20 19:46:31.791626	2	4	LUZ	0.00
1056	2018-08-20 19:46:32.803124	2	1	TEM	19.90
1058	2018-08-20 19:46:33.812343	2	2	HUM	29.50
1061	2018-08-20 19:46:34.834948	2	3	HUM_SOIL	0.00
1065	2018-08-20 19:46:42.151333	2	4	LUZ	0.00
1069	2018-08-20 19:46:43.191452	2	1	TEM	19.90
1041	2018-08-20 19:46:21.87496	1	4	LUZ	0.00
1054	2018-08-20 19:46:31.917157	1	1	TEM	20.20
1064	2018-08-20 19:46:41.958339	1	2	HUM	29.10
1071	2018-08-20 19:46:44.184308	2	2	HUM	29.50
1072	2018-08-20 19:46:44.2075	2	2	HUM	29.50
1073	2018-08-20 19:46:44.214762	2	2	HUM	29.50
1074	2018-08-20 19:46:45.208243	2	3	HUM_SOIL	0.00
1075	2018-08-20 19:46:45.223993	2	3	HUM_SOIL	0.00
1076	2018-08-20 19:46:45.239655	2	3	HUM_SOIL	0.00
1077	2018-08-20 19:46:51.993992	1	3	HUM_SOIL	1.00
1078	2018-08-20 19:46:52.528261	2	4	LUZ	0.00
1079	2018-08-20 19:46:52.533782	2	4	LUZ	0.00
1080	2018-08-20 19:46:52.543271	2	4	LUZ	0.00
1081	2018-08-20 19:46:53.54592	2	1	TEM	19.80
1082	2018-08-20 19:46:53.562761	2	1	TEM	19.80
1083	2018-08-20 19:46:53.576696	2	1	TEM	19.80
1084	2018-08-20 19:46:54.579146	2	2	HUM	29.50
1085	2018-08-20 19:46:54.579288	2	2	HUM	29.50
1086	2018-08-20 19:46:54.595194	2	2	HUM	29.50
1087	2018-08-20 19:46:55.603188	2	3	HUM_SOIL	0.00
1088	2018-08-20 19:46:55.613597	2	3	HUM_SOIL	0.00
1089	2018-08-20 19:46:55.624469	2	3	HUM_SOIL	0.00
1090	2018-08-20 19:47:02.036724	1	4	LUZ	0.00
1091	2018-08-20 19:47:02.891399	2	4	LUZ	0.00
1092	2018-08-20 19:47:02.898423	2	4	LUZ	0.00
1093	2018-08-20 19:47:02.909818	2	4	LUZ	0.00
1094	2018-08-20 19:47:03.898891	2	1	TEM	19.80
1095	2018-08-20 19:47:03.930832	2	1	TEM	19.80
1096	2018-08-20 19:47:03.944652	2	1	TEM	19.80
1097	2018-08-20 19:47:04.931644	2	2	HUM	29.50
1098	2018-08-20 19:47:04.965766	2	2	HUM	29.50
1099	2018-08-20 19:47:04.965766	2	2	HUM	29.50
1100	2018-08-20 19:47:05.954439	2	3	HUM_SOIL	0.00
1102	2018-08-20 19:47:05.993335	2	3	HUM_SOIL	0.00
1101	2018-08-20 19:47:05.993404	2	3	HUM_SOIL	0.00
1103	2018-08-20 19:47:12.086673	1	1	TEM	20.20
1104	2018-08-20 19:47:13.2558	2	4	LUZ	0.00
1105	2018-08-20 19:47:13.25876	2	4	LUZ	0.00
1106	2018-08-20 19:47:13.266362	2	4	LUZ	0.00
1107	2018-08-20 19:47:14.264067	2	1	TEM	19.80
1108	2018-08-20 19:47:14.284669	2	1	TEM	19.80
1109	2018-08-20 19:47:14.289217	2	1	TEM	19.80
1110	2018-08-20 19:47:15.286456	2	2	HUM	29.40
1111	2018-08-20 19:47:15.307776	2	2	HUM	29.40
1112	2018-08-20 19:47:15.318953	2	2	HUM	29.40
1113	2018-08-20 19:47:16.309318	2	3	HUM_SOIL	0.00
1114	2018-08-20 19:47:16.3435	2	3	HUM_SOIL	0.00
1115	2018-08-20 19:47:16.3435	2	3	HUM_SOIL	0.00
1116	2018-08-20 19:47:22.12476	1	2	HUM	29.20
1117	2018-08-20 19:47:23.625982	2	4	LUZ	0.00
1118	2018-08-20 19:47:23.627355	2	4	LUZ	0.00
1119	2018-08-20 19:47:23.642831	2	4	LUZ	0.00
1120	2018-08-20 19:47:24.644195	2	1	TEM	19.80
1121	2018-08-20 19:47:24.660155	2	1	TEM	19.80
1122	2018-08-20 19:47:24.661576	2	1	TEM	19.80
1123	2018-08-20 19:47:25.669911	2	2	HUM	30.00
1124	2018-08-20 19:47:25.677436	2	2	HUM	30.00
1125	2018-08-20 19:47:25.677436	2	2	HUM	30.00
1126	2018-08-20 19:47:26.685599	2	3	HUM_SOIL	0.00
1127	2018-08-20 19:47:26.6896	2	3	HUM_SOIL	0.00
1128	2018-08-20 19:47:26.693189	2	3	HUM_SOIL	0.00
1129	2018-08-20 19:47:32.164645	1	3	HUM_SOIL	34.00
1130	2018-08-20 19:47:33.990087	2	4	LUZ	0.00
1131	2018-08-20 19:47:33.996949	2	4	LUZ	0.00
1132	2018-08-20 19:47:34.007899	2	4	LUZ	0.00
1133	2018-08-20 19:47:35.019907	2	1	TEM	19.80
1134	2018-08-20 19:47:35.025021	2	1	TEM	19.80
1135	2018-08-20 19:47:35.037284	2	1	TEM	19.80
1136	2018-08-20 19:47:36.0436	2	2	HUM	29.50
1137	2018-08-20 19:47:36.053805	2	2	HUM	29.50
1138	2018-08-20 19:47:36.053805	2	2	HUM	29.50
1139	2018-08-20 19:47:37.073851	2	3	HUM_SOIL	0.00
1140	2018-08-20 19:47:37.079529	2	3	HUM_SOIL	0.00
1141	2018-08-20 19:47:37.091361	2	3	HUM_SOIL	0.00
1142	2018-08-20 19:47:42.20937	1	4	LUZ	0.00
1143	2018-08-20 19:47:44.382137	2	4	LUZ	0.00
1144	2018-08-20 19:47:44.388943	2	4	LUZ	0.00
1145	2018-08-20 19:47:44.398348	2	4	LUZ	0.00
1146	2018-08-20 19:47:45.407891	2	1	TEM	19.80
1147	2018-08-20 19:47:45.422472	2	1	TEM	19.80
1148	2018-08-20 19:47:45.422472	2	1	TEM	19.80
1149	2018-08-20 19:47:46.414664	2	2	HUM	29.40
1150	2018-08-20 19:47:46.436265	2	2	HUM	29.40
1151	2018-08-20 19:47:46.455312	2	2	HUM	29.40
1152	2018-08-20 19:47:47.443098	2	3	HUM_SOIL	0.00
1153	2018-08-20 19:47:47.464116	2	3	HUM_SOIL	0.00
1154	2018-08-20 19:47:47.480487	2	3	HUM_SOIL	0.00
1155	2018-08-20 19:47:52.254398	1	1	TEM	20.20
1156	2018-08-20 19:47:54.737016	2	4	LUZ	0.00
1157	2018-08-20 19:47:54.737016	2	4	LUZ	0.00
1158	2018-08-20 19:47:54.738271	2	4	LUZ	0.00
1159	2018-08-20 19:47:55.743475	2	1	TEM	19.80
1160	2018-08-20 19:47:55.759555	2	1	TEM	19.80
1161	2018-08-20 19:47:55.7661	2	1	TEM	19.80
1162	2018-08-20 19:47:56.751216	2	2	HUM	29.50
1163	2018-08-20 19:47:56.781592	2	2	HUM	29.50
1164	2018-08-20 19:47:56.788395	2	2	HUM	29.50
1165	2018-08-20 19:47:57.75914	2	3	HUM_SOIL	0.00
1166	2018-08-20 19:47:57.789507	2	3	HUM_SOIL	0.00
1167	2018-08-20 19:47:57.812237	2	3	HUM_SOIL	0.00
1168	2018-08-20 19:48:02.30378	1	2	HUM	29.20
1170	2018-08-20 19:48:05.102486	2	4	LUZ	0.00
1169	2018-08-20 19:48:05.101672	2	4	LUZ	0.00
1171	2018-08-20 19:48:05.102906	2	4	LUZ	0.00
1172	2018-08-20 19:48:06.110594	2	1	TEM	19.80
1173	2018-08-20 19:48:06.110358	2	1	TEM	19.80
1174	2018-08-20 19:48:06.129068	2	1	TEM	19.80
1175	2018-08-20 19:48:07.127176	2	2	HUM	29.50
1176	2018-08-20 19:48:07.13514	2	2	HUM	29.50
1177	2018-08-20 19:48:07.144946	2	2	HUM	29.50
1178	2018-08-20 19:48:08.154349	2	3	HUM_SOIL	0.00
1179	2018-08-20 19:48:08.162129	2	3	HUM_SOIL	0.00
1180	2018-08-20 19:48:08.169926	2	3	HUM_SOIL	0.00
1181	2018-08-20 19:48:12.344955	1	3	HUM_SOIL	1.00
1182	2018-08-20 19:48:15.467881	2	4	LUZ	0.00
1183	2018-08-20 19:48:15.477415	2	4	LUZ	0.00
1184	2018-08-20 19:48:15.482864	2	4	LUZ	0.00
1185	2018-08-20 19:48:16.474636	2	1	TEM	19.90
1186	2018-08-20 19:48:16.500733	2	1	TEM	19.90
1187	2018-08-20 19:48:16.516215	2	1	TEM	19.90
1188	2018-08-20 19:48:17.48231	2	2	HUM	29.70
1191	2018-08-20 19:48:18.505359	2	3	HUM_SOIL	0.00
1196	2018-08-20 19:48:25.839116	2	4	LUZ	0.00
1200	2018-08-20 19:48:26.878536	2	1	TEM	19.90
1203	2018-08-20 19:48:27.907389	2	2	HUM	30.20
1206	2018-08-20 19:48:28.922721	2	3	HUM_SOIL	0.00
1209	2018-08-20 19:48:36.200589	2	4	LUZ	0.00
1213	2018-08-20 19:48:37.229795	2	1	TEM	19.90
1216	2018-08-20 19:48:38.243113	2	2	HUM	30.00
1217	2018-08-20 19:48:39.248964	2	3	HUM_SOIL	0.00
1221	2018-08-20 19:48:46.575385	2	4	LUZ	0.00
1226	2018-08-20 19:48:47.601543	2	1	TEM	19.80
1229	2018-08-20 19:48:48.631066	2	2	HUM	29.80
1231	2018-08-20 19:48:49.644724	2	3	HUM_SOIL	0.00
1235	2018-08-20 19:48:56.939726	2	4	LUZ	0.00
1238	2018-08-20 19:48:57.973684	2	1	TEM	19.90
1241	2018-08-20 19:48:58.992051	2	2	HUM	29.90
1245	2018-08-20 19:49:00.019699	2	3	HUM_SOIL	0.00
1249	2018-08-20 19:49:07.319574	2	4	LUZ	0.00
1252	2018-08-20 19:49:08.327247	2	1	TEM	19.90
1254	2018-08-20 19:49:09.334831	2	2	HUM	29.90
1256	2018-08-20 19:49:10.342182	2	3	HUM_SOIL	0.00
1260	2018-08-20 19:49:17.682074	2	4	LUZ	0.00
1264	2018-08-20 19:49:18.714287	2	1	TEM	19.90
1266	2018-08-20 19:49:19.721544	2	2	HUM	29.80
1269	2018-08-20 19:49:20.744131	2	3	HUM_SOIL	0.00
1274	2018-08-20 19:49:28.048282	2	4	LUZ	0.00
1277	2018-08-20 19:49:29.056578	2	1	TEM	19.90
1280	2018-08-20 19:49:30.07488	2	2	HUM	29.80
1282	2018-08-20 19:49:31.098612	2	3	HUM_SOIL	0.00
1287	2018-08-20 19:49:38.415561	2	4	LUZ	0.00
1291	2018-08-20 19:49:39.445689	2	1	TEM	19.90
1294	2018-08-20 19:49:40.477544	2	2	HUM	29.90
1297	2018-08-20 19:49:41.505815	2	3	HUM_SOIL	0.00
1189	2018-08-20 19:48:17.508724	2	2	HUM	29.70
1193	2018-08-20 19:48:18.532574	2	3	HUM_SOIL	0.00
1197	2018-08-20 19:48:25.857388	2	4	LUZ	0.00
1199	2018-08-20 19:48:26.862451	2	1	TEM	19.90
1202	2018-08-20 19:48:27.872269	2	2	HUM	30.20
1204	2018-08-20 19:48:28.877638	2	3	HUM_SOIL	0.00
1208	2018-08-20 19:48:36.200589	2	4	LUZ	0.00
1211	2018-08-20 19:48:37.208354	2	1	TEM	19.90
1214	2018-08-20 19:48:38.230953	2	2	HUM	30.00
1218	2018-08-20 19:48:39.249399	2	3	HUM_SOIL	0.00
1223	2018-08-20 19:48:46.575385	2	4	LUZ	0.00
1225	2018-08-20 19:48:47.598098	2	1	TEM	19.80
1228	2018-08-20 19:48:48.626295	2	2	HUM	29.80
1232	2018-08-20 19:48:49.648626	2	3	HUM_SOIL	0.00
1236	2018-08-20 19:48:56.950535	2	4	LUZ	0.00
1239	2018-08-20 19:48:57.973684	2	1	TEM	19.90
1242	2018-08-20 19:48:58.996706	2	2	HUM	29.90
1244	2018-08-20 19:49:00.019198	2	3	HUM_SOIL	0.00
1248	2018-08-20 19:49:07.306323	2	4	LUZ	0.00
1251	2018-08-20 19:49:08.314714	2	1	TEM	19.90
1255	2018-08-20 19:49:09.343808	2	2	HUM	29.90
1258	2018-08-20 19:49:10.367644	2	3	HUM_SOIL	0.00
1262	2018-08-20 19:49:17.688451	2	4	LUZ	0.00
1265	2018-08-20 19:49:18.717708	2	1	TEM	19.90
1268	2018-08-20 19:49:19.751213	2	2	HUM	29.80
1271	2018-08-20 19:49:20.774191	2	3	HUM_SOIL	0.00
1273	2018-08-20 19:49:28.048282	2	4	LUZ	0.00
1276	2018-08-20 19:49:29.056578	2	1	TEM	19.90
1279	2018-08-20 19:49:30.07488	2	2	HUM	29.80
1283	2018-08-20 19:49:31.098037	2	3	HUM_SOIL	0.00
1286	2018-08-20 19:49:38.415561	2	4	LUZ	0.00
1289	2018-08-20 19:49:39.428501	2	1	TEM	19.90
1292	2018-08-20 19:49:40.456518	2	2	HUM	29.90
1295	2018-08-20 19:49:41.464752	2	3	HUM_SOIL	0.00
1190	2018-08-20 19:48:17.525601	2	2	HUM	29.70
1192	2018-08-20 19:48:18.532575	2	3	HUM_SOIL	0.00
1195	2018-08-20 19:48:25.839116	2	4	LUZ	0.00
1198	2018-08-20 19:48:26.849063	2	1	TEM	19.90
1201	2018-08-20 19:48:27.862278	2	2	HUM	30.20
1205	2018-08-20 19:48:28.899182	2	3	HUM_SOIL	0.00
1210	2018-08-20 19:48:36.218116	2	4	LUZ	0.00
1212	2018-08-20 19:48:37.227511	2	1	TEM	19.90
1215	2018-08-20 19:48:38.23986	2	2	HUM	30.00
1219	2018-08-20 19:48:39.272161	2	3	HUM_SOIL	0.00
1222	2018-08-20 19:48:46.575388	2	4	LUZ	0.00
1224	2018-08-20 19:48:47.583173	2	1	TEM	19.80
1227	2018-08-20 19:48:48.611576	2	2	HUM	29.80
1230	2018-08-20 19:48:49.618758	2	3	HUM_SOIL	0.00
1234	2018-08-20 19:48:56.940827	2	4	LUZ	0.00
1237	2018-08-20 19:48:57.9586	2	1	TEM	19.90
1240	2018-08-20 19:48:58.965919	2	2	HUM	29.90
1243	2018-08-20 19:48:59.973384	2	3	HUM_SOIL	0.00
1247	2018-08-20 19:49:07.305854	2	4	LUZ	0.00
1250	2018-08-20 19:49:08.313425	2	1	TEM	19.90
1253	2018-08-20 19:49:09.326248	2	2	HUM	29.90
1257	2018-08-20 19:49:10.349423	2	3	HUM_SOIL	0.00
1261	2018-08-20 19:49:17.681203	2	4	LUZ	0.00
1263	2018-08-20 19:49:18.699484	2	1	TEM	19.90
1267	2018-08-20 19:49:19.724602	2	2	HUM	29.80
1270	2018-08-20 19:49:20.745169	2	3	HUM_SOIL	0.00
1275	2018-08-20 19:49:28.050316	2	4	LUZ	0.00
1278	2018-08-20 19:49:29.083473	2	1	TEM	19.90
1281	2018-08-20 19:49:30.097088	2	2	HUM	29.80
1284	2018-08-20 19:49:31.115327	2	3	HUM_SOIL	0.00
1288	2018-08-20 19:49:38.421691	2	4	LUZ	0.00
1290	2018-08-20 19:49:39.43938	2	1	TEM	19.90
1293	2018-08-20 19:49:40.456519	2	2	HUM	29.90
1296	2018-08-20 19:49:41.48193	2	3	HUM_SOIL	0.00
1194	2018-08-20 19:48:22.425319	1	4	LUZ	0.00
1207	2018-08-20 19:48:32.467402	1	1	TEM	20.20
1220	2018-08-20 19:48:42.503462	1	2	HUM	29.30
1233	2018-08-20 19:48:52.548161	1	3	HUM_SOIL	99.00
1246	2018-08-20 19:49:02.585223	1	4	LUZ	0.00
1259	2018-08-20 19:49:12.624896	1	1	TEM	20.20
1272	2018-08-20 19:49:22.671301	1	2	HUM	29.40
1285	2018-08-20 19:49:32.715188	1	3	HUM_SOIL	99.00
1298	2018-08-20 19:49:42.753428	1	4	LUZ	0.00
1299	2018-08-20 19:49:48.791446	2	4	LUZ	0.00
1300	2018-08-20 19:49:48.791446	2	4	LUZ	0.00
1301	2018-08-20 19:49:48.79892	2	4	LUZ	0.00
1302	2018-08-20 19:49:49.814313	2	1	TEM	19.80
1303	2018-08-20 19:49:49.81355	2	1	TEM	19.80
1304	2018-08-20 19:49:49.831593	2	1	TEM	19.80
1305	2018-08-20 19:49:50.833347	2	2	HUM	30.00
1306	2018-08-20 19:49:50.838505	2	2	HUM	30.00
1307	2018-08-20 19:49:50.849267	2	2	HUM	30.00
1308	2018-08-20 19:49:51.855591	2	3	HUM_SOIL	0.00
1309	2018-08-20 19:49:51.862355	2	3	HUM_SOIL	0.00
1310	2018-08-20 19:49:51.882489	2	3	HUM_SOIL	0.00
1311	2018-08-20 19:49:52.795248	1	1	TEM	20.10
1312	2018-08-20 19:49:59.155735	2	4	LUZ	0.00
1313	2018-08-20 19:49:59.154075	2	4	LUZ	0.00
1314	2018-08-20 19:49:59.170218	2	4	LUZ	0.00
1315	2018-08-20 19:50:00.16302	2	1	TEM	19.90
1316	2018-08-20 19:50:00.187614	2	1	TEM	19.90
1317	2018-08-20 19:50:00.187614	2	1	TEM	19.90
1318	2018-08-20 19:50:01.18528	2	2	HUM	29.90
1319	2018-08-20 19:50:01.20637	2	2	HUM	29.90
1320	2018-08-20 19:50:01.221638	2	2	HUM	29.90
1321	2018-08-20 19:50:02.213505	2	3	HUM_SOIL	0.00
1322	2018-08-20 19:50:02.219714	2	3	HUM_SOIL	0.00
1323	2018-08-20 19:50:02.236344	2	3	HUM_SOIL	0.00
1324	2018-08-20 19:50:02.839319	1	2	HUM	29.30
1325	2018-08-20 19:50:09.519709	2	4	LUZ	0.00
1326	2018-08-20 19:50:09.526719	2	4	LUZ	0.00
1327	2018-08-20 19:50:09.53725	2	4	LUZ	0.00
1328	2018-08-20 19:50:10.54398	2	1	TEM	19.80
1329	2018-08-20 19:50:10.554264	2	1	TEM	19.80
1330	2018-08-20 19:50:10.570375	2	1	TEM	19.80
1331	2018-08-20 19:50:11.566821	2	2	HUM	29.70
1332	2018-08-20 19:50:11.58819	2	2	HUM	29.70
1333	2018-08-20 19:50:11.593263	2	2	HUM	29.70
1334	2018-08-20 19:50:12.576589	2	3	HUM_SOIL	0.00
1335	2018-08-20 19:50:12.606735	2	3	HUM_SOIL	0.00
1336	2018-08-20 19:50:12.621633	2	3	HUM_SOIL	0.00
1337	2018-08-20 19:50:12.877987	1	3	HUM_SOIL	85.00
1338	2018-08-20 19:50:19.887669	2	4	LUZ	0.00
1339	2018-08-20 19:50:19.88794	2	4	LUZ	0.00
1340	2018-08-20 19:50:19.894303	2	4	LUZ	0.00
1341	2018-08-20 19:50:20.90031	2	1	TEM	19.90
1342	2018-08-20 19:50:20.906958	2	1	TEM	19.90
1343	2018-08-20 19:50:20.916282	2	1	TEM	19.90
1344	2018-08-20 19:50:21.919835	2	2	HUM	29.60
1345	2018-08-20 19:50:21.935301	2	2	HUM	29.60
1346	2018-08-20 19:50:21.941635	2	2	HUM	29.60
1347	2018-08-20 19:50:22.915618	1	4	LUZ	0.00
1348	2018-08-20 19:50:22.952482	2	3	HUM_SOIL	0.00
1349	2018-08-20 19:50:22.959216	2	3	HUM_SOIL	0.00
1350	2018-08-20 19:50:22.959216	2	3	HUM_SOIL	0.00
1351	2018-08-20 19:50:30.257675	2	4	LUZ	0.00
1352	2018-08-20 19:50:30.257675	2	4	LUZ	0.00
1353	2018-08-20 19:50:30.26459	2	4	LUZ	0.00
1355	2018-08-20 19:50:31.265199	2	1	TEM	19.90
1354	2018-08-20 19:50:31.265199	2	1	TEM	19.90
1356	2018-08-20 19:50:31.273397	2	1	TEM	19.90
1357	2018-08-20 19:50:32.272474	2	2	HUM	29.60
1358	2018-08-20 19:50:32.279558	2	2	HUM	29.60
1359	2018-08-20 19:50:32.288372	2	2	HUM	29.60
1360	2018-08-20 19:50:32.953514	1	1	TEM	20.10
1361	2018-08-20 19:50:33.305843	2	3	HUM_SOIL	0.00
1362	2018-08-20 19:50:33.310903	2	3	HUM_SOIL	0.00
1363	2018-08-20 19:50:33.314068	2	3	HUM_SOIL	0.00
1364	2018-08-20 19:50:40.624356	2	4	LUZ	0.00
1365	2018-08-20 19:50:40.624356	2	4	LUZ	0.00
1366	2018-08-20 19:50:40.631048	2	4	LUZ	0.00
1367	2018-08-20 19:50:41.638093	2	1	TEM	19.80
1368	2018-08-20 19:50:41.638093	2	1	TEM	19.80
1369	2018-08-20 19:50:41.658382	2	1	TEM	19.80
1370	2018-08-20 19:50:42.656133	2	2	HUM	29.50
1371	2018-08-20 19:50:42.672095	2	2	HUM	29.50
1372	2018-08-20 19:50:42.686801	2	2	HUM	29.50
1373	2018-08-20 19:50:42.994946	1	2	HUM	29.40
1375	2018-08-20 19:50:43.690496	2	3	HUM_SOIL	0.00
1374	2018-08-20 19:50:43.690496	2	3	HUM_SOIL	0.00
1376	2018-08-20 19:50:43.71995	2	3	HUM_SOIL	0.00
1377	2018-08-20 19:50:50.992954	2	4	LUZ	0.00
1378	2018-08-20 19:50:50.992954	2	4	LUZ	0.00
1379	2018-08-20 19:50:50.994385	2	4	LUZ	0.00
1380	2018-08-20 19:50:52.000491	2	1	TEM	19.90
1381	2018-08-20 19:50:52.000491	2	1	TEM	19.90
1382	2018-08-20 19:50:52.000774	2	1	TEM	19.90
1385	2018-08-20 19:50:53.008458	2	2	HUM	29.60
1383	2018-08-20 19:50:53.00835	2	2	HUM	29.60
1384	2018-08-20 19:50:53.00835	2	2	HUM	29.60
1386	2018-08-20 19:50:53.034724	1	3	HUM_SOIL	78.00
1387	2018-08-20 19:50:54.027207	2	3	HUM_SOIL	0.00
1388	2018-08-20 19:50:54.026842	2	3	HUM_SOIL	0.00
1389	2018-08-20 19:50:54.033293	2	3	HUM_SOIL	0.00
1390	2018-08-20 19:51:01.361679	2	4	LUZ	0.00
1391	2018-08-20 19:51:01.361679	2	4	LUZ	0.00
1392	2018-08-20 19:51:01.363337	2	4	LUZ	0.00
1393	2018-08-20 19:51:02.369931	2	1	TEM	19.90
1394	2018-08-20 19:51:02.394875	2	1	TEM	19.90
1395	2018-08-20 19:51:02.397913	2	1	TEM	19.90
1396	2018-08-20 19:51:03.079832	1	4	LUZ	0.00
1397	2018-08-20 19:51:03.376054	2	2	HUM	29.60
1398	2018-08-20 19:51:03.403113	2	2	HUM	29.60
1399	2018-08-20 19:51:03.430794	2	2	HUM	29.60
1400	2018-08-20 19:51:04.404237	2	3	HUM_SOIL	0.00
1401	2018-08-20 19:51:04.425423	2	3	HUM_SOIL	0.00
1402	2018-08-20 19:51:04.454259	2	3	HUM_SOIL	0.00
1403	2018-08-20 19:51:11.730093	2	4	LUZ	0.00
1404	2018-08-20 19:51:11.730195	2	4	LUZ	0.00
1405	2018-08-20 19:51:11.730093	2	4	LUZ	0.00
1406	2018-08-20 19:51:12.737849	2	1	TEM	19.90
1407	2018-08-20 19:51:12.760286	2	1	TEM	19.90
1408	2018-08-20 19:51:12.76633	2	1	TEM	19.90
1409	2018-08-20 19:51:13.11342	1	1	TEM	20.20
1410	2018-08-20 19:51:13.760545	2	2	HUM	30.00
1413	2018-08-20 19:51:14.789081	2	3	HUM_SOIL	0.00
1417	2018-08-20 19:51:22.098814	2	4	LUZ	0.00
1419	2018-08-20 19:51:23.121189	2	1	TEM	19.90
1424	2018-08-20 19:51:24.155415	2	2	HUM	29.60
1426	2018-08-20 19:51:25.172836	2	3	HUM_SOIL	0.00
1429	2018-08-20 19:51:32.466007	2	4	LUZ	0.00
1435	2018-08-20 19:51:33.500459	2	1	TEM	19.90
1438	2018-08-20 19:51:34.517157	2	2	HUM	29.90
1441	2018-08-20 19:51:35.526574	2	3	HUM_SOIL	0.00
1444	2018-08-20 19:51:42.838669	2	4	LUZ	0.00
1448	2018-08-20 19:51:43.857215	2	1	TEM	19.90
1451	2018-08-20 19:51:44.864774	2	2	HUM	29.90
1454	2018-08-20 19:51:45.881113	2	3	HUM_SOIL	0.00
1455	2018-08-20 19:51:53.20406	2	4	LUZ	0.00
1460	2018-08-20 19:51:54.212516	2	1	TEM	19.90
1462	2018-08-20 19:51:55.225482	2	2	HUM	30.00
1465	2018-08-20 19:51:56.253379	2	3	HUM_SOIL	0.00
1469	2018-08-20 19:52:03.571561	2	4	LUZ	0.00
1473	2018-08-20 19:52:04.579507	2	1	TEM	19.90
1475	2018-08-20 19:52:05.586639	2	2	HUM	30.00
1478	2018-08-20 19:52:06.61019	2	3	HUM_SOIL	0.00
1482	2018-08-20 19:52:13.941072	2	4	LUZ	0.00
1485	2018-08-20 19:52:14.947944	2	1	TEM	19.90
1488	2018-08-20 19:52:15.955622	2	2	HUM	31.40
1491	2018-08-20 19:52:16.963122	2	3	HUM_SOIL	0.00
1496	2018-08-20 19:52:24.308631	2	4	LUZ	0.00
1499	2018-08-20 19:52:25.31735	2	1	TEM	19.90
1501	2018-08-20 19:52:26.324015	2	2	HUM	31.00
1504	2018-08-20 19:52:27.331309	2	3	HUM_SOIL	0.00
1510	2018-08-20 19:52:34.67675	2	4	LUZ	0.00
1513	2018-08-20 19:52:35.684125	2	1	TEM	19.90
1515	2018-08-20 19:52:36.691266	2	2	HUM	30.40
1517	2018-08-20 19:52:37.700073	2	3	HUM_SOIL	0.00
1411	2018-08-20 19:51:13.785747	2	2	HUM	30.00
1414	2018-08-20 19:51:14.804335	2	3	HUM_SOIL	0.00
1416	2018-08-20 19:51:22.099629	2	4	LUZ	0.00
1421	2018-08-20 19:51:23.133981	2	1	TEM	19.90
1425	2018-08-20 19:51:24.162384	2	2	HUM	29.60
1428	2018-08-20 19:51:25.195886	2	3	HUM_SOIL	0.00
1430	2018-08-20 19:51:32.467225	2	4	LUZ	0.00
1433	2018-08-20 19:51:33.484009	2	1	TEM	19.90
1436	2018-08-20 19:51:34.492567	2	2	HUM	29.90
1440	2018-08-20 19:51:35.520261	2	3	HUM_SOIL	0.00
1442	2018-08-20 19:51:42.837246	2	4	LUZ	0.00
1446	2018-08-20 19:51:43.844361	2	1	TEM	19.90
1449	2018-08-20 19:51:44.858271	2	2	HUM	29.90
1452	2018-08-20 19:51:45.881113	2	3	HUM_SOIL	0.00
1456	2018-08-20 19:51:53.204307	2	4	LUZ	0.00
1461	2018-08-20 19:51:54.22776	2	1	TEM	19.90
1464	2018-08-20 19:51:55.257362	2	2	HUM	30.00
1467	2018-08-20 19:51:56.27604	2	3	HUM_SOIL	0.00
1471	2018-08-20 19:52:03.571542	2	4	LUZ	0.00
1474	2018-08-20 19:52:04.594997	2	1	TEM	19.90
1476	2018-08-20 19:52:05.602525	2	2	HUM	30.00
1479	2018-08-20 19:52:06.610412	2	3	HUM_SOIL	0.00
1484	2018-08-20 19:52:13.956107	2	4	LUZ	0.00
1487	2018-08-20 19:52:14.984196	2	1	TEM	19.90
1490	2018-08-20 19:52:16.007041	2	2	HUM	31.40
1493	2018-08-20 19:52:17.030224	2	3	HUM_SOIL	0.00
1495	2018-08-20 19:52:24.308631	2	4	LUZ	0.00
1498	2018-08-20 19:52:25.316018	2	1	TEM	19.90
1502	2018-08-20 19:52:26.324023	2	2	HUM	31.00
1506	2018-08-20 19:52:27.359126	2	3	HUM_SOIL	0.00
1509	2018-08-20 19:52:34.676276	2	4	LUZ	0.00
1511	2018-08-20 19:52:35.684133	2	1	TEM	19.90
1516	2018-08-20 19:52:36.69713	2	2	HUM	30.40
1518	2018-08-20 19:52:37.705185	2	3	HUM_SOIL	0.00
1412	2018-08-20 19:51:13.802399	2	2	HUM	30.00
1415	2018-08-20 19:51:14.828062	2	3	HUM_SOIL	0.00
1418	2018-08-20 19:51:22.105921	2	4	LUZ	0.00
1420	2018-08-20 19:51:23.12962	2	1	TEM	19.90
1423	2018-08-20 19:51:24.140613	2	2	HUM	29.60
1427	2018-08-20 19:51:25.174834	2	3	HUM_SOIL	0.00
1431	2018-08-20 19:51:32.468814	2	4	LUZ	0.00
1434	2018-08-20 19:51:33.49046	2	1	TEM	19.90
1437	2018-08-20 19:51:34.50859	2	2	HUM	29.90
1439	2018-08-20 19:51:35.51475	2	3	HUM_SOIL	0.00
1443	2018-08-20 19:51:42.837246	2	4	LUZ	0.00
1447	2018-08-20 19:51:43.850674	2	1	TEM	19.90
1450	2018-08-20 19:51:44.858271	2	2	HUM	29.90
1453	2018-08-20 19:51:45.881125	2	3	HUM_SOIL	0.00
1457	2018-08-20 19:51:53.20406	2	4	LUZ	0.00
1459	2018-08-20 19:51:54.211273	2	1	TEM	19.90
1463	2018-08-20 19:51:55.24392	2	2	HUM	30.00
1466	2018-08-20 19:51:56.268502	2	3	HUM_SOIL	0.00
1470	2018-08-20 19:52:03.572161	2	4	LUZ	0.00
1472	2018-08-20 19:52:04.579431	2	1	TEM	19.90
1477	2018-08-20 19:52:05.606615	2	2	HUM	30.00
1480	2018-08-20 19:52:06.633215	2	3	HUM_SOIL	0.00
1483	2018-08-20 19:52:13.941072	2	4	LUZ	0.00
1486	2018-08-20 19:52:14.949736	2	1	TEM	19.90
1489	2018-08-20 19:52:15.957798	2	2	HUM	31.40
1492	2018-08-20 19:52:16.965413	2	3	HUM_SOIL	0.00
1497	2018-08-20 19:52:24.308631	2	4	LUZ	0.00
1500	2018-08-20 19:52:25.334131	2	1	TEM	19.90
1503	2018-08-20 19:52:26.346594	2	2	HUM	31.00
1505	2018-08-20 19:52:27.353972	2	3	HUM_SOIL	0.00
1508	2018-08-20 19:52:34.676276	2	4	LUZ	0.00
1512	2018-08-20 19:52:35.684125	2	1	TEM	19.90
1514	2018-08-20 19:52:36.691266	2	2	HUM	30.40
1519	2018-08-20 19:52:37.706037	2	3	HUM_SOIL	0.00
1422	2018-08-20 19:51:23.162613	1	2	HUM	29.40
1432	2018-08-20 19:51:33.205595	1	3	HUM_SOIL	82.00
1445	2018-08-20 19:51:43.243654	1	4	LUZ	0.00
1458	2018-08-20 19:51:53.283459	1	1	TEM	20.10
1468	2018-08-20 19:52:03.323605	1	2	HUM	29.30
1481	2018-08-20 19:52:13.363907	1	3	HUM_SOIL	99.00
1494	2018-08-20 19:52:23.405366	1	4	LUZ	0.00
1507	2018-08-20 19:52:33.443402	1	1	TEM	20.20
1520	2018-08-20 19:52:43.483693	1	2	HUM	29.30
1521	2018-08-20 19:52:45.045692	2	4	LUZ	0.00
1522	2018-08-20 19:52:45.047016	2	4	LUZ	0.00
1523	2018-08-20 19:52:45.061625	2	4	LUZ	0.00
1524	2018-08-20 19:52:46.05389	2	1	TEM	19.90
1525	2018-08-20 19:52:46.068859	2	1	TEM	19.90
1526	2018-08-20 19:52:46.094515	2	1	TEM	19.90
1527	2018-08-20 19:52:47.061621	2	2	HUM	30.00
1528	2018-08-20 19:52:47.07854	2	2	HUM	30.00
1529	2018-08-20 19:52:47.117321	2	2	HUM	30.00
1530	2018-08-20 19:52:48.094877	2	3	HUM_SOIL	0.00
1531	2018-08-20 19:52:48.094877	2	3	HUM_SOIL	0.00
1532	2018-08-20 19:52:48.145373	2	3	HUM_SOIL	0.00
1533	2018-08-20 19:52:53.535651	1	3	HUM_SOIL	82.00
1535	2018-08-20 19:52:55.413296	2	4	LUZ	0.00
1534	2018-08-20 19:52:55.413296	2	4	LUZ	0.00
1536	2018-08-20 19:52:55.422147	2	4	LUZ	0.00
1537	2018-08-20 19:52:56.420845	2	1	TEM	19.90
1538	2018-08-20 19:52:56.446269	2	1	TEM	19.90
1539	2018-08-20 19:52:56.452205	2	1	TEM	19.90
1540	2018-08-20 19:52:57.443027	2	2	HUM	30.00
1541	2018-08-20 19:52:57.474641	2	2	HUM	30.00
1542	2018-08-20 19:52:57.480866	2	2	HUM	30.00
1543	2018-08-20 19:52:58.470925	2	3	HUM_SOIL	0.00
1544	2018-08-20 19:52:58.498486	2	3	HUM_SOIL	0.00
1545	2018-08-20 19:52:58.504824	2	3	HUM_SOIL	0.00
1546	2018-08-20 19:53:03.573883	1	4	LUZ	0.00
1547	2018-08-20 19:53:05.781428	2	4	LUZ	0.00
1548	2018-08-20 19:53:05.781299	2	4	LUZ	0.00
1549	2018-08-20 19:53:05.798032	2	4	LUZ	0.00
1550	2018-08-20 19:53:06.789671	2	1	TEM	19.90
1551	2018-08-20 19:53:06.804875	2	1	TEM	19.90
1552	2018-08-20 19:53:06.807555	2	1	TEM	19.90
1553	2018-08-20 19:53:07.813281	2	2	HUM	30.00
1554	2018-08-20 19:53:07.812753	2	2	HUM	30.00
1555	2018-08-20 19:53:07.820878	2	2	HUM	30.00
1556	2018-08-20 19:53:08.835781	2	3	HUM_SOIL	0.00
1557	2018-08-20 19:53:08.835781	2	3	HUM_SOIL	0.00
1558	2018-08-20 19:53:08.850348	2	3	HUM_SOIL	0.00
1559	2018-08-20 19:53:13.613612	1	1	TEM	20.20
1560	2018-08-20 19:53:16.169656	2	4	LUZ	0.00
1561	2018-08-20 19:53:16.168856	2	4	LUZ	0.00
1562	2018-08-20 19:53:16.168856	2	4	LUZ	0.00
1563	2018-08-20 19:53:17.189077	2	1	TEM	nan
1564	2018-08-20 19:53:17.194174	2	1	TEM	nan
1565	2018-08-20 19:53:17.205289	2	1	TEM	nan
1566	2018-08-20 19:53:18.212212	2	2	HUM	nan
1567	2018-08-20 19:53:18.212212	2	2	HUM	nan
1568	2018-08-20 19:53:18.235431	2	2	HUM	nan
1570	2018-08-20 19:53:19.219776	2	3	HUM_SOIL	0.00
1569	2018-08-20 19:53:19.220289	2	3	HUM_SOIL	0.00
1571	2018-08-20 19:53:19.249486	2	3	HUM_SOIL	0.00
1572	2018-08-20 19:53:23.653424	1	2	HUM	29.30
1573	2018-08-20 19:53:26.519515	2	4	LUZ	0.00
1574	2018-08-20 19:53:26.523316	2	4	LUZ	0.00
1575	2018-08-20 19:53:26.537223	2	4	LUZ	0.00
1576	2018-08-20 19:53:27.527596	2	1	TEM	19.90
1577	2018-08-20 19:53:27.551633	2	1	TEM	19.90
1578	2018-08-20 19:53:27.559446	2	1	TEM	19.90
1579	2018-08-20 19:53:28.548779	2	2	HUM	29.80
1580	2018-08-20 19:53:28.577766	2	2	HUM	29.80
1581	2018-08-20 19:53:28.587579	2	2	HUM	29.80
1582	2018-08-20 19:53:29.576372	2	3	HUM_SOIL	0.00
1583	2018-08-20 19:53:29.595656	2	3	HUM_SOIL	0.00
1584	2018-08-20 19:53:29.616365	2	3	HUM_SOIL	0.00
1585	2018-08-20 19:53:33.693827	1	3	HUM_SOIL	99.00
1586	2018-08-20 19:53:36.898892	2	4	LUZ	0.00
1587	2018-08-20 19:53:36.899109	2	4	LUZ	0.00
1588	2018-08-20 19:53:36.922276	2	4	LUZ	0.00
1589	2018-08-20 19:53:37.9179	2	1	TEM	19.90
1590	2018-08-20 19:53:37.935778	2	1	TEM	19.90
1591	2018-08-20 19:53:37.950833	2	1	TEM	19.90
1592	2018-08-20 19:53:38.947639	2	2	HUM	30.40
1593	2018-08-20 19:53:38.954469	2	2	HUM	30.40
1594	2018-08-20 19:53:38.969712	2	2	HUM	30.40
1595	2018-08-20 19:53:39.962235	2	3	HUM_SOIL	0.00
1596	2018-08-20 19:53:39.981975	2	3	HUM_SOIL	0.00
1597	2018-08-20 19:53:40.003143	2	3	HUM_SOIL	0.00
1598	2018-08-20 19:53:43.733614	1	4	LUZ	0.00
1599	2018-08-20 19:53:47.271907	2	4	LUZ	0.00
1600	2018-08-20 19:53:47.271907	2	4	LUZ	0.00
1601	2018-08-20 19:53:47.288116	2	4	LUZ	0.00
1602	2018-08-20 19:53:48.279418	2	1	TEM	19.90
1603	2018-08-20 19:53:48.281057	2	1	TEM	19.90
1604	2018-08-20 19:53:48.31568	2	1	TEM	19.90
1605	2018-08-20 19:53:49.28715	2	2	HUM	30.00
1606	2018-08-20 19:53:49.28715	2	2	HUM	30.00
1607	2018-08-20 19:53:49.33845	2	2	HUM	30.00
1608	2018-08-20 19:53:50.294759	2	3	HUM_SOIL	0.00
1609	2018-08-20 19:53:50.294759	2	3	HUM_SOIL	0.00
1610	2018-08-20 19:53:50.366633	2	3	HUM_SOIL	0.00
1611	2018-08-20 19:53:53.775513	1	1	TEM	20.20
1612	2018-08-20 19:53:57.639018	2	4	LUZ	0.00
1613	2018-08-20 19:53:57.639018	2	4	LUZ	0.00
1614	2018-08-20 19:53:57.654776	2	4	LUZ	0.00
1615	2018-08-20 19:53:58.647048	2	1	TEM	19.90
1616	2018-08-20 19:53:58.669336	2	1	TEM	19.90
1617	2018-08-20 19:53:58.67972	2	1	TEM	19.90
1618	2018-08-20 19:53:59.67643	2	2	HUM	29.80
1619	2018-08-20 19:53:59.691584	2	2	HUM	29.80
1620	2018-08-20 19:53:59.723079	2	2	HUM	29.80
1621	2018-08-20 19:54:00.710705	2	3	HUM_SOIL	0.00
1622	2018-08-20 19:54:00.7095	2	3	HUM_SOIL	0.00
1623	2018-08-20 19:54:00.744426	2	3	HUM_SOIL	0.00
1624	2018-08-20 19:54:03.813526	1	2	HUM	29.20
1625	2018-08-20 19:54:07.996082	2	4	LUZ	0.00
1626	2018-08-20 19:54:07.996082	2	4	LUZ	0.00
1627	2018-08-20 19:54:08.001683	2	4	LUZ	0.00
1628	2018-08-20 19:54:09.015702	2	1	TEM	19.90
1629	2018-08-20 19:54:09.015702	2	1	TEM	19.90
1630	2018-08-20 19:54:09.032473	2	1	TEM	19.90
1631	2018-08-20 19:54:10.024198	2	2	HUM	29.80
1632	2018-08-20 19:54:10.037403	2	2	HUM	29.80
1635	2018-08-20 19:54:11.053528	2	3	HUM_SOIL	0.00
1639	2018-08-20 19:54:18.372151	2	4	LUZ	0.00
1643	2018-08-20 19:54:19.408888	2	1	TEM	19.90
1646	2018-08-20 19:54:20.452332	2	2	HUM	29.70
1649	2018-08-20 19:54:21.480642	2	3	HUM_SOIL	0.00
1653	2018-08-20 19:54:28.758011	2	4	LUZ	0.00
1656	2018-08-20 19:54:29.780681	2	1	TEM	19.90
1659	2018-08-20 19:54:30.795679	2	2	HUM	29.70
1661	2018-08-20 19:54:31.801485	2	3	HUM_SOIL	0.00
1665	2018-08-20 19:54:39.106824	2	4	LUZ	0.00
1669	2018-08-20 19:54:40.139412	2	1	TEM	19.90
1672	2018-08-20 19:54:41.172229	2	2	HUM	29.70
1674	2018-08-20 19:54:42.189931	2	3	HUM_SOIL	0.00
1679	2018-08-20 19:54:49.491877	2	4	LUZ	0.00
1682	2018-08-20 19:54:50.511651	2	1	TEM	19.90
1683	2018-08-20 19:54:51.529076	2	2	HUM	29.70
1686	2018-08-20 19:54:52.552888	2	3	HUM_SOIL	0.00
1691	2018-08-20 19:54:59.847165	2	4	LUZ	0.00
1694	2018-08-20 19:55:00.861285	2	1	TEM	19.90
1696	2018-08-20 19:55:01.874293	2	2	HUM	29.70
1699	2018-08-20 19:55:02.881887	2	3	HUM_SOIL	0.00
1703	2018-08-20 19:55:10.205651	2	4	LUZ	0.00
1708	2018-08-20 19:55:11.22877	2	1	TEM	19.90
1709	2018-08-20 19:55:12.246411	2	2	HUM	29.80
1713	2018-08-20 19:55:13.275013	2	3	HUM_SOIL	0.00
1718	2018-08-20 19:55:20.574523	2	4	LUZ	0.00
1721	2018-08-20 19:55:21.60054	2	1	TEM	19.90
1724	2018-08-20 19:55:22.606165	2	2	HUM	29.90
1725	2018-08-20 19:55:23.612912	2	3	HUM_SOIL	0.00
1730	2018-08-20 19:55:30.943023	2	4	LUZ	0.00
1733	2018-08-20 19:55:31.957288	2	1	TEM	19.90
1736	2018-08-20 19:55:32.98087	2	2	HUM	30.10
1740	2018-08-20 19:55:34.014314	2	3	HUM_SOIL	0.00
1744	2018-08-20 19:55:41.318663	2	4	LUZ	0.00
1746	2018-08-20 19:55:42.3366	2	1	TEM	19.90
1750	2018-08-20 19:55:43.37311	2	2	HUM	30.10
1636	2018-08-20 19:54:11.053748	2	3	HUM_SOIL	0.00
1640	2018-08-20 19:54:18.388558	2	4	LUZ	0.00
1641	2018-08-20 19:54:19.395154	2	1	TEM	19.90
1645	2018-08-20 19:54:20.42948	2	2	HUM	29.70
1648	2018-08-20 19:54:21.451556	2	3	HUM_SOIL	0.00
1652	2018-08-20 19:54:28.738204	2	4	LUZ	0.00
1654	2018-08-20 19:54:29.755421	2	1	TEM	19.90
1657	2018-08-20 19:54:30.778412	2	2	HUM	29.70
1660	2018-08-20 19:54:31.78678	2	3	HUM_SOIL	0.00
1666	2018-08-20 19:54:39.112851	2	4	LUZ	0.00
1667	2018-08-20 19:54:40.120543	2	1	TEM	19.90
1670	2018-08-20 19:54:41.128861	2	2	HUM	29.70
1673	2018-08-20 19:54:42.136089	2	3	HUM_SOIL	0.00
1678	2018-08-20 19:54:49.47645	2	4	LUZ	0.00
1681	2018-08-20 19:54:50.511308	2	1	TEM	19.90
1685	2018-08-20 19:54:51.540977	2	2	HUM	29.70
1688	2018-08-20 19:54:52.56972	2	3	HUM_SOIL	0.00
1690	2018-08-20 19:54:59.847165	2	4	LUZ	0.00
1695	2018-08-20 19:55:00.877264	2	1	TEM	19.90
1698	2018-08-20 19:55:01.90588	2	2	HUM	29.70
1700	2018-08-20 19:55:02.912233	2	3	HUM_SOIL	0.00
1704	2018-08-20 19:55:10.205651	2	4	LUZ	0.00
1706	2018-08-20 19:55:11.223864	2	1	TEM	19.90
1710	2018-08-20 19:55:12.246411	2	2	HUM	29.80
1712	2018-08-20 19:55:13.25409	2	3	HUM_SOIL	0.00
1716	2018-08-20 19:55:20.574523	2	4	LUZ	0.00
1720	2018-08-20 19:55:21.588502	2	1	TEM	19.90
1723	2018-08-20 19:55:22.597038	2	2	HUM	29.90
1727	2018-08-20 19:55:23.620433	2	3	HUM_SOIL	0.00
1731	2018-08-20 19:55:30.944968	2	4	LUZ	0.00
1734	2018-08-20 19:55:31.962979	2	1	TEM	19.90
1737	2018-08-20 19:55:32.986087	2	2	HUM	30.10
1739	2018-08-20 19:55:34.009516	2	3	HUM_SOIL	0.00
1742	2018-08-20 19:55:41.311619	2	4	LUZ	0.00
1745	2018-08-20 19:55:42.330055	2	1	TEM	19.90
1748	2018-08-20 19:55:43.337197	2	2	HUM	30.10
2974	2018-08-21 04:30:29.884027	2	1	TEM	20.60
2975	2018-08-21 04:30:30.63402	2	2	HUM	30.00
2976	2018-08-21 04:30:31.661896	2	3	HUM_SOIL	0.00
2978	2018-08-21 04:30:38.964524	2	4	LUZ	0.00
2979	2018-08-21 04:30:39.987126	2	1	TEM	20.60
2980	2018-08-21 04:30:41.009824	2	2	HUM	30.00
2981	2018-08-21 04:30:42.037948	2	3	HUM_SOIL	0.00
2983	2018-08-21 04:30:49.33083	2	4	LUZ	0.00
2984	2018-08-21 04:30:50.353181	2	1	TEM	20.60
2985	2018-08-21 04:30:51.37604	2	2	HUM	30.00
2986	2018-08-21 04:30:52.403973	2	3	HUM_SOIL	0.00
2988	2018-08-21 04:30:59.694455	2	4	LUZ	0.00
2989	2018-08-21 04:31:00.717775	2	1	TEM	20.60
2990	2018-08-21 04:31:01.740477	2	2	HUM	30.00
2991	2018-08-21 04:31:02.76834	2	3	HUM_SOIL	0.00
2993	2018-08-21 04:31:10.067733	2	4	LUZ	0.00
2994	2018-08-21 04:31:11.090389	2	1	TEM	20.60
2995	2018-08-21 04:31:12.113165	2	2	HUM	30.00
2996	2018-08-21 04:31:13.141083	2	3	HUM_SOIL	0.00
2998	2018-08-21 04:31:20.436197	2	4	LUZ	0.00
2999	2018-08-21 04:31:21.458976	2	1	TEM	20.60
3000	2018-08-21 04:31:22.481629	2	2	HUM	30.00
3002	2018-08-21 04:31:23.509619	2	3	HUM_SOIL	0.00
3003	2018-08-21 04:31:30.826725	2	4	LUZ	0.00
3004	2018-08-21 04:31:31.849474	2	1	TEM	20.60
3005	2018-08-21 04:31:32.872324	2	2	HUM	30.00
3007	2018-08-21 04:31:33.89525	2	3	HUM_SOIL	0.00
3008	2018-08-21 04:31:41.173464	2	4	LUZ	0.00
3009	2018-08-21 04:31:42.196457	2	1	TEM	20.60
3010	2018-08-21 04:31:43.219304	2	2	HUM	30.00
3012	2018-08-21 04:31:44.247063	2	3	HUM_SOIL	0.00
3013	2018-08-21 04:31:51.533604	2	4	LUZ	0.00
3014	2018-08-21 04:31:52.556129	2	1	TEM	20.60
3016	2018-08-21 04:31:53.578727	2	2	HUM	30.00
3017	2018-08-21 04:31:54.606162	2	3	HUM_SOIL	0.00
3018	2018-08-21 04:32:01.909746	2	4	LUZ	0.00
3019	2018-08-21 04:32:02.932516	2	1	TEM	20.60
3021	2018-08-21 04:32:03.955494	2	2	HUM	30.00
3022	2018-08-21 04:32:04.983237	2	3	HUM_SOIL	0.00
3023	2018-08-21 04:32:12.309931	2	4	LUZ	0.00
3024	2018-08-21 04:32:13.33266	2	1	TEM	20.60
3026	2018-08-21 04:32:14.355467	2	2	HUM	30.00
3027	2018-08-21 04:32:15.383308	2	3	HUM_SOIL	0.00
3028	2018-08-21 04:32:22.647081	2	4	LUZ	0.00
3030	2018-08-21 04:32:23.676586	2	1	TEM	20.60
3031	2018-08-21 04:32:24.703252	2	2	HUM	30.00
3032	2018-08-21 04:32:25.731481	2	3	HUM_SOIL	0.00
3033	2018-08-21 04:32:33.011247	2	4	LUZ	0.00
3035	2018-08-21 04:32:34.033643	2	1	TEM	20.60
3036	2018-08-21 04:32:35.056461	2	2	HUM	30.00
3037	2018-08-21 04:32:36.084169	2	3	HUM_SOIL	0.00
3101	2018-08-22 23:47:43.868006	1	1	TEM	21.60
3102	2018-08-22 23:47:53.931777	1	2	HUM	42.90
3103	2018-08-22 23:48:03.975612	1	3	HUM_SOIL	99.00
3104	2018-08-22 23:48:14.019229	1	4	LUZ	0.00
3105	2018-08-22 23:48:24.063571	1	1	TEM	21.50
3106	2018-08-22 23:48:34.106093	1	2	HUM	42.90
3107	2018-08-22 23:48:44.15039	1	3	HUM_SOIL	99.00
3108	2018-08-22 23:48:54.19263	1	4	LUZ	0.00
3109	2018-08-22 23:49:04.234783	1	1	TEM	21.60
3110	2018-08-22 23:49:14.276891	1	2	HUM	43.00
3111	2018-08-22 23:49:24.313493	1	3	HUM_SOIL	99.00
3112	2018-08-22 23:49:34.356147	1	4	LUZ	0.00
3113	2018-08-22 23:49:44.398734	1	1	TEM	21.50
1637	2018-08-20 19:54:13.855293	1	3	HUM_SOIL	1.00
1650	2018-08-20 19:54:23.902435	1	4	LUZ	0.00
1663	2018-08-20 19:54:33.946861	1	1	TEM	20.20
1676	2018-08-20 19:54:43.985138	1	2	HUM	29.20
1689	2018-08-20 19:54:54.023642	1	3	HUM_SOIL	1.00
1702	2018-08-20 19:55:04.066967	1	4	LUZ	0.00
1715	2018-08-20 19:55:14.103534	1	1	TEM	20.20
1728	2018-08-20 19:55:24.143393	1	2	HUM	29.20
1741	2018-08-20 19:55:34.18384	1	3	HUM_SOIL	99.00
1751	2018-08-20 19:55:44.226137	1	4	LUZ	0.00
3038	2018-08-21 04:34:17.948125	2	1	TEM	20.60
3039	2018-08-21 04:34:18.774383	2	2	HUM	30.00
3040	2018-08-21 04:34:19.797219	2	3	HUM_SOIL	0.00
3041	2018-08-21 04:34:27.090016	2	4	LUZ	0.00
3042	2018-08-21 04:34:28.124774	2	1	TEM	20.60
3044	2018-08-21 04:34:29.14886	2	2	HUM	30.10
3045	2018-08-21 04:34:30.176548	2	3	HUM_SOIL	0.00
3046	2018-08-21 04:34:37.432757	2	4	LUZ	0.00
3048	2018-08-21 04:34:38.456058	2	1	TEM	20.60
3049	2018-08-21 04:34:39.479168	2	2	HUM	30.10
3050	2018-08-21 04:34:40.506618	2	3	HUM_SOIL	0.00
3051	2018-08-21 04:34:47.806574	2	4	LUZ	0.00
3053	2018-08-21 04:34:48.82961	2	1	TEM	20.60
3054	2018-08-21 04:34:49.858168	2	2	HUM	30.10
3055	2018-08-21 04:34:50.886368	2	3	HUM_SOIL	0.00
3056	2018-08-21 04:34:58.187632	2	4	LUZ	0.00
3058	2018-08-21 04:34:59.21078	2	1	TEM	20.60
3059	2018-08-21 04:35:00.233558	2	2	HUM	30.10
3060	2018-08-21 04:35:01.258477	2	3	HUM_SOIL	0.00
3062	2018-08-21 04:35:08.53753	2	4	LUZ	0.00
3063	2018-08-21 04:35:09.560209	2	1	TEM	20.60
3064	2018-08-21 04:35:10.582971	2	2	HUM	30.10
3065	2018-08-21 04:35:11.610935	2	3	HUM_SOIL	0.00
3067	2018-08-21 04:35:18.920488	2	4	LUZ	0.00
3068	2018-08-21 04:35:19.94316	2	1	TEM	20.60
3069	2018-08-21 04:35:20.965984	2	2	HUM	30.00
3070	2018-08-21 04:35:21.993672	2	3	HUM_SOIL	0.00
3072	2018-08-21 04:35:29.278401	2	4	LUZ	0.00
3073	2018-08-21 04:35:30.300052	2	1	TEM	20.60
3074	2018-08-21 04:35:31.32285	2	2	HUM	30.00
3075	2018-08-21 04:35:32.345774	2	3	HUM_SOIL	0.00
3077	2018-08-21 04:35:39.651683	2	4	LUZ	0.00
3078	2018-08-21 04:35:40.675011	2	1	TEM	20.60
3079	2018-08-21 04:35:41.698214	2	2	HUM	30.00
3080	2018-08-21 04:35:42.726519	2	3	HUM_SOIL	0.00
3082	2018-08-21 04:35:50.016024	2	4	LUZ	0.00
3083	2018-08-21 04:35:51.039093	2	1	TEM	20.60
3084	2018-08-21 04:35:52.062256	2	2	HUM	30.00
3085	2018-08-21 04:35:53.08572	2	3	HUM_SOIL	0.00
3087	2018-08-21 04:36:00.388078	2	4	LUZ	0.00
3088	2018-08-21 04:36:01.411245	2	1	TEM	20.60
3089	2018-08-21 04:36:02.434415	2	2	HUM	30.00
3090	2018-08-21 04:36:03.462659	2	3	HUM_SOIL	0.00
3092	2018-08-21 04:36:10.748899	2	4	LUZ	0.00
3093	2018-08-21 04:36:11.756226	2	1	TEM	20.60
3094	2018-08-21 04:36:12.76394	2	2	HUM	30.00
3095	2018-08-21 04:36:13.771801	2	3	HUM_SOIL	0.00
3114	2018-08-22 23:50:20.932363	1	1	TEM	21.60
3115	2018-08-22 23:50:31.010948	1	2	HUM	43.10
1752	2018-08-20 19:55:44.362019	2	3	HUM_SOIL	0.00
1756	2018-08-20 19:55:51.683209	2	4	LUZ	0.00
1758	2018-08-20 19:55:52.700769	2	1	TEM	19.90
1763	2018-08-20 19:55:53.724211	2	2	HUM	30.00
1767	2018-08-20 19:55:54.758285	2	3	HUM_SOIL	0.00
1769	2018-08-20 19:56:02.090386	2	4	LUZ	0.00
1771	2018-08-20 19:56:03.098615	2	1	TEM	19.90
1774	2018-08-20 19:56:04.107798	2	2	HUM	30.00
1778	2018-08-20 19:56:05.130477	2	3	HUM_SOIL	0.00
1782	2018-08-20 19:56:12.416211	2	4	LUZ	0.00
1784	2018-08-20 19:56:13.434659	2	1	TEM	19.90
1790	2018-08-20 19:56:14.465999	2	2	HUM	30.10
1793	2018-08-20 19:56:15.499442	2	3	HUM_SOIL	0.00
1794	2018-08-20 19:56:22.785349	2	4	LUZ	0.00
1799	2018-08-20 19:56:23.793042	2	1	TEM	19.90
1802	2018-08-20 19:56:24.802057	2	2	HUM	30.10
1805	2018-08-20 19:56:25.808633	2	3	HUM_SOIL	0.00
1809	2018-08-20 19:56:33.161754	2	4	LUZ	0.00
1812	2018-08-20 19:56:34.195682	2	1	TEM	19.90
1816	2018-08-20 19:56:35.223619	2	2	HUM	30.00
1819	2018-08-20 19:56:36.251863	2	3	HUM_SOIL	0.00
1822	2018-08-20 19:56:43.531059	2	4	LUZ	0.00
1824	2018-08-20 19:56:44.537647	2	1	TEM	19.90
1827	2018-08-20 19:56:45.560554	2	2	HUM	30.00
1832	2018-08-20 19:56:46.591161	2	3	HUM_SOIL	0.00
1833	2018-08-20 19:56:53.890436	2	4	LUZ	0.00
1838	2018-08-20 19:56:54.898285	2	1	TEM	19.90
1842	2018-08-20 19:56:55.932387	2	2	HUM	30.00
1845	2018-08-20 19:56:56.969944	2	3	HUM_SOIL	0.00
1847	2018-08-20 19:57:04.27752	2	4	LUZ	0.00
1851	2018-08-20 19:57:05.289639	2	1	TEM	19.90
1854	2018-08-20 19:57:06.329115	2	2	HUM	30.40
1857	2018-08-20 19:57:07.347465	2	3	HUM_SOIL	0.00
1861	2018-08-20 19:57:14.627595	2	4	LUZ	0.00
1865	2018-08-20 19:57:15.638084	2	1	TEM	20.00
1867	2018-08-20 19:57:16.644207	2	2	HUM	30.10
1870	2018-08-20 19:57:17.677682	2	3	HUM_SOIL	0.00
1875	2018-08-20 19:57:25.011437	2	4	LUZ	0.00
1878	2018-08-20 19:57:26.045507	2	1	TEM	20.00
1881	2018-08-20 19:57:27.073592	2	2	HUM	30.00
1884	2018-08-20 19:57:28.101701	2	3	HUM_SOIL	0.00
1886	2018-08-20 19:57:35.374851	2	4	LUZ	0.00
1889	2018-08-20 19:57:36.393406	2	1	TEM	20.00
1892	2018-08-20 19:57:37.42225	2	2	HUM	30.50
1895	2018-08-20 19:57:38.448135	2	3	HUM_SOIL	0.00
1900	2018-08-20 19:57:45.734652	2	4	LUZ	0.00
1904	2018-08-20 19:57:46.763908	2	1	TEM	20.00
1907	2018-08-20 19:57:47.782013	2	2	HUM	30.00
1909	2018-08-20 19:57:48.789754	2	3	HUM_SOIL	0.00
1914	2018-08-20 19:57:56.117974	2	4	LUZ	0.00
1916	2018-08-20 19:57:57.126343	2	1	TEM	20.00
1919	2018-08-20 19:57:58.13646	2	2	HUM	30.20
1921	2018-08-20 19:57:59.154611	2	3	HUM_SOIL	0.00
1926	2018-08-20 19:58:06.469383	2	4	LUZ	0.00
1929	2018-08-20 19:58:07.497933	2	1	TEM	20.00
1932	2018-08-20 19:58:08.527766	2	2	HUM	30.10
1934	2018-08-20 19:58:09.545583	2	3	HUM_SOIL	0.00
1938	2018-08-20 19:58:16.838183	2	4	LUZ	0.00
1941	2018-08-20 19:58:17.860957	2	1	TEM	20.00
1945	2018-08-20 19:58:18.89031	2	2	HUM	32.10
1947	2018-08-20 19:58:19.912777	2	3	HUM_SOIL	0.00
1952	2018-08-20 19:58:27.206694	2	4	LUZ	0.00
1954	2018-08-20 19:58:28.214069	2	1	TEM	20.00
1957	2018-08-20 19:58:29.221589	2	2	HUM	31.00
1961	2018-08-20 19:58:30.244529	2	3	HUM_SOIL	0.00
1965	2018-08-20 19:58:37.576842	2	4	LUZ	0.00
1967	2018-08-20 19:58:38.591983	2	1	TEM	20.00
1970	2018-08-20 19:58:39.619962	2	2	HUM	31.20
1974	2018-08-20 19:58:40.643891	2	3	HUM_SOIL	0.00
1978	2018-08-20 19:58:47.94631	2	4	LUZ	0.00
1980	2018-08-20 19:58:48.954394	2	1	TEM	20.00
1983	2018-08-20 19:58:49.971904	2	2	HUM	30.40
1987	2018-08-20 19:58:50.997054	2	3	HUM_SOIL	0.00
1991	2018-08-20 19:58:58.310894	2	4	LUZ	0.00
1993	2018-08-20 19:58:59.318284	2	1	TEM	20.00
1996	2018-08-20 19:59:00.325343	2	2	HUM	30.10
1999	2018-08-20 19:59:01.344451	2	3	HUM_SOIL	0.00
2004	2018-08-20 19:59:08.678539	2	4	LUZ	0.00
2007	2018-08-20 19:59:09.711689	2	1	TEM	20.10
2010	2018-08-20 19:59:10.737318	2	2	HUM	30.00
2013	2018-08-20 19:59:11.760708	2	3	HUM_SOIL	0.00
2017	2018-08-20 19:59:19.049051	2	4	LUZ	0.00
2019	2018-08-20 19:59:20.056719	2	1	TEM	20.10
2023	2018-08-20 19:59:21.064304	2	2	HUM	30.00
2027	2018-08-20 19:59:22.087277	2	3	HUM_SOIL	0.00
2031	2018-08-20 19:59:29.421404	2	4	LUZ	0.00
2034	2018-08-20 19:59:30.430951	2	1	TEM	20.10
2036	2018-08-20 19:59:31.442201	2	2	HUM	30.00
2039	2018-08-20 19:59:32.465728	2	3	HUM_SOIL	0.00
2042	2018-08-20 19:59:39.784471	2	4	LUZ	0.00
2045	2018-08-20 19:59:40.791355	2	1	TEM	20.10
2048	2018-08-20 19:59:41.79893	2	2	HUM	30.00
2052	2018-08-20 19:59:42.806259	2	3	HUM_SOIL	0.00
3043	2018-08-21 04:34:28.22934	1	1	TEM	20.70
3047	2018-08-21 04:34:38.299761	1	2	HUM	29.80
3052	2018-08-21 04:34:48.34661	1	3	HUM_SOIL	81.00
3057	2018-08-21 04:34:58.387331	1	4	LUZ	0.00
3061	2018-08-21 04:35:08.423543	1	1	TEM	20.70
3066	2018-08-21 04:35:18.463597	1	2	HUM	29.80
3071	2018-08-21 04:35:28.504759	1	3	HUM_SOIL	93.00
3076	2018-08-21 04:35:38.549696	1	4	LUZ	0.00
3081	2018-08-21 04:35:48.585348	1	1	TEM	20.70
3086	2018-08-21 04:35:58.625377	1	2	HUM	29.80
3091	2018-08-21 04:36:08.671171	1	3	HUM_SOIL	94.00
3096	2018-08-21 04:36:18.717414	1	4	LUZ	0.00
3116	2018-08-22 23:51:07.562001	1	1	TEM	21.60
3117	2018-08-22 23:51:17.630729	1	2	HUM	43.00
1753	2018-08-20 19:55:44.365714	2	3	HUM_SOIL	0.00
1755	2018-08-20 19:55:51.683209	2	4	LUZ	0.00
1759	2018-08-20 19:55:52.707453	2	1	TEM	19.90
1761	2018-08-20 19:55:53.714895	2	2	HUM	30.00
1766	2018-08-20 19:55:54.742289	2	3	HUM_SOIL	0.00
1768	2018-08-20 19:56:02.090623	2	4	LUZ	0.00
1772	2018-08-20 19:56:03.098262	2	1	TEM	19.90
1775	2018-08-20 19:56:04.116225	2	2	HUM	30.00
1779	2018-08-20 19:56:05.150662	2	3	HUM_SOIL	0.00
1781	2018-08-20 19:56:12.416211	2	4	LUZ	0.00
1786	2018-08-20 19:56:13.449632	2	1	TEM	19.90
1789	2018-08-20 19:56:14.467681	2	2	HUM	30.10
1792	2018-08-20 19:56:15.474988	2	3	HUM_SOIL	0.00
1796	2018-08-20 19:56:22.785329	2	4	LUZ	0.00
1797	2018-08-20 19:56:23.793041	2	1	TEM	19.90
1801	2018-08-20 19:56:24.800848	2	2	HUM	30.10
1806	2018-08-20 19:56:25.808633	2	3	HUM_SOIL	0.00
1807	2018-08-20 19:56:33.155574	2	4	LUZ	0.00
1810	2018-08-20 19:56:34.162685	2	1	TEM	19.90
1814	2018-08-20 19:56:35.170348	2	2	HUM	30.00
1817	2018-08-20 19:56:36.177964	2	3	HUM_SOIL	0.00
1820	2018-08-20 19:56:43.524675	2	4	LUZ	0.00
1825	2018-08-20 19:56:44.543717	2	1	TEM	19.90
1828	2018-08-20 19:56:45.560554	2	2	HUM	30.00
1830	2018-08-20 19:56:46.567818	2	3	HUM_SOIL	0.00
1834	2018-08-20 19:56:53.890435	2	4	LUZ	0.00
1837	2018-08-20 19:56:54.898285	2	1	TEM	19.90
1840	2018-08-20 19:56:55.905596	2	2	HUM	30.00
1843	2018-08-20 19:56:56.928681	2	3	HUM_SOIL	0.00
1848	2018-08-20 19:57:04.287521	2	4	LUZ	0.00
1852	2018-08-20 19:57:05.326067	2	1	TEM	19.90
1855	2018-08-20 19:57:06.335925	2	2	HUM	30.40
1858	2018-08-20 19:57:07.369574	2	3	HUM_SOIL	0.00
1860	2018-08-20 19:57:14.626276	2	4	LUZ	0.00
1863	2018-08-20 19:57:15.633837	2	1	TEM	20.00
1866	2018-08-20 19:57:16.641298	2	2	HUM	30.10
1869	2018-08-20 19:57:17.650008	2	3	HUM_SOIL	0.00
1874	2018-08-20 19:57:24.995802	2	4	LUZ	0.00
1877	2018-08-20 19:57:26.029853	2	1	TEM	20.00
1880	2018-08-20 19:57:27.057665	2	2	HUM	30.00
1883	2018-08-20 19:57:28.081083	2	3	HUM_SOIL	0.00
1888	2018-08-20 19:57:35.386763	2	4	LUZ	0.00
1891	2018-08-20 19:57:36.409645	2	1	TEM	20.00
1894	2018-08-20 19:57:37.429124	2	2	HUM	30.50
1897	2018-08-20 19:57:38.456375	2	3	HUM_SOIL	0.00
1899	2018-08-20 19:57:45.734478	2	4	LUZ	0.00
1903	2018-08-20 19:57:46.763908	2	1	TEM	20.00
1905	2018-08-20 19:57:47.781769	2	2	HUM	30.00
1908	2018-08-20 19:57:48.789754	2	3	HUM_SOIL	0.00
1912	2018-08-20 19:57:56.101084	2	4	LUZ	0.00
1915	2018-08-20 19:57:57.114116	2	1	TEM	20.00
1918	2018-08-20 19:57:58.137681	2	2	HUM	30.20
1922	2018-08-20 19:57:59.16153	2	3	HUM_SOIL	0.00
1927	2018-08-20 19:58:06.471215	2	4	LUZ	0.00
1930	2018-08-20 19:58:07.504851	2	1	TEM	20.00
1933	2018-08-20 19:58:08.538688	2	2	HUM	30.10
1936	2018-08-20 19:58:09.572462	2	3	HUM_SOIL	0.00
1940	2018-08-20 19:58:16.853647	2	4	LUZ	0.00
1943	2018-08-20 19:58:17.874506	2	1	TEM	20.00
1946	2018-08-20 19:58:18.902434	2	2	HUM	32.10
1949	2018-08-20 19:58:19.919175	2	3	HUM_SOIL	0.00
1953	2018-08-20 19:58:27.207002	2	4	LUZ	0.00
1955	2018-08-20 19:58:28.216664	2	1	TEM	20.00
1958	2018-08-20 19:58:29.224038	2	2	HUM	31.00
1960	2018-08-20 19:58:30.241772	2	3	HUM_SOIL	0.00
1964	2018-08-20 19:58:37.573787	2	4	LUZ	0.00
1968	2018-08-20 19:58:38.602322	2	1	TEM	20.00
1972	2018-08-20 19:58:39.62601	2	2	HUM	31.20
1975	2018-08-20 19:58:40.649056	2	3	HUM_SOIL	0.00
1979	2018-08-20 19:58:47.951498	2	4	LUZ	0.00
1981	2018-08-20 19:58:48.958931	2	1	TEM	20.00
1984	2018-08-20 19:58:49.977622	2	2	HUM	30.40
1986	2018-08-20 19:58:50.983355	2	3	HUM_SOIL	0.00
1990	2018-08-20 19:58:58.311573	2	4	LUZ	0.00
1995	2018-08-20 19:58:59.336346	2	1	TEM	20.00
1998	2018-08-20 19:59:00.359656	2	2	HUM	30.10
2001	2018-08-20 19:59:01.388866	2	3	HUM_SOIL	0.00
2003	2018-08-20 19:59:08.678539	2	4	LUZ	0.00
2006	2018-08-20 19:59:09.701773	2	1	TEM	20.10
2011	2018-08-20 19:59:10.747117	2	2	HUM	30.00
2014	2018-08-20 19:59:11.75679	2	3	HUM_SOIL	0.00
2018	2018-08-20 19:59:19.049051	2	4	LUZ	0.00
2021	2018-08-20 19:59:20.056719	2	1	TEM	20.10
2024	2018-08-20 19:59:21.064304	2	2	HUM	30.00
2026	2018-08-20 19:59:22.087436	2	3	HUM_SOIL	0.00
2030	2018-08-20 19:59:29.415227	2	4	LUZ	0.00
2032	2018-08-20 19:59:30.422932	2	1	TEM	20.10
2037	2018-08-20 19:59:31.456827	2	2	HUM	30.00
2040	2018-08-20 19:59:32.475149	2	3	HUM_SOIL	0.00
2044	2018-08-20 19:59:39.784471	2	4	LUZ	0.00
2047	2018-08-20 19:59:40.815483	2	1	TEM	20.10
2050	2018-08-20 19:59:41.838658	2	2	HUM	30.00
2053	2018-08-20 19:59:42.866664	2	3	HUM_SOIL	0.00
3118	2018-08-22 23:54:51.368759	1	1	TEM	21.70
1754	2018-08-20 19:55:44.382005	2	3	HUM_SOIL	0.00
1757	2018-08-20 19:55:51.689711	2	4	LUZ	0.00
1760	2018-08-20 19:55:52.707542	2	1	TEM	19.90
1762	2018-08-20 19:55:53.716133	2	2	HUM	30.00
1765	2018-08-20 19:55:54.739243	2	3	HUM_SOIL	0.00
1770	2018-08-20 19:56:02.107415	2	4	LUZ	0.00
1773	2018-08-20 19:56:03.117814	2	1	TEM	19.90
1776	2018-08-20 19:56:04.133751	2	2	HUM	30.00
1780	2018-08-20 19:56:05.157695	2	3	HUM_SOIL	0.00
1783	2018-08-20 19:56:12.416211	2	4	LUZ	0.00
1785	2018-08-20 19:56:13.440766	2	1	TEM	19.90
1788	2018-08-20 19:56:14.447983	2	2	HUM	30.10
1791	2018-08-20 19:56:15.470894	2	3	HUM_SOIL	0.00
1795	2018-08-20 19:56:22.785329	2	4	LUZ	0.00
1798	2018-08-20 19:56:23.793042	2	1	TEM	19.90
1803	2018-08-20 19:56:24.800852	2	2	HUM	30.10
1804	2018-08-20 19:56:25.808633	2	3	HUM_SOIL	0.00
1808	2018-08-20 19:56:33.157058	2	4	LUZ	0.00
1811	2018-08-20 19:56:34.162685	2	1	TEM	19.90
1815	2018-08-20 19:56:35.170348	2	2	HUM	30.00
1818	2018-08-20 19:56:36.179022	2	3	HUM_SOIL	0.00
1821	2018-08-20 19:56:43.524677	2	4	LUZ	0.00
1826	2018-08-20 19:56:44.563107	2	1	TEM	19.90
1829	2018-08-20 19:56:45.569149	2	2	HUM	30.00
1831	2018-08-20 19:56:46.576164	2	3	HUM_SOIL	0.00
1835	2018-08-20 19:56:53.890436	2	4	LUZ	0.00
1839	2018-08-20 19:56:54.898285	2	1	TEM	19.90
1841	2018-08-20 19:56:55.905596	2	2	HUM	30.00
1844	2018-08-20 19:56:56.928681	2	3	HUM_SOIL	0.00
1846	2018-08-20 19:57:04.278226	2	4	LUZ	0.00
1850	2018-08-20 19:57:05.284516	2	1	TEM	19.90
1853	2018-08-20 19:57:06.302585	2	2	HUM	30.40
1856	2018-08-20 19:57:07.329964	2	3	HUM_SOIL	0.00
1862	2018-08-20 19:57:14.626276	2	4	LUZ	0.00
1864	2018-08-20 19:57:15.633837	2	1	TEM	20.00
1868	2018-08-20 19:57:16.659621	2	2	HUM	30.10
1871	2018-08-20 19:57:17.693282	2	3	HUM_SOIL	0.00
1873	2018-08-20 19:57:24.995802	2	4	LUZ	0.00
1876	2018-08-20 19:57:26.005497	2	1	TEM	20.00
1879	2018-08-20 19:57:27.013799	2	2	HUM	30.00
1882	2018-08-20 19:57:28.036183	2	3	HUM_SOIL	0.00
1887	2018-08-20 19:57:35.374988	2	4	LUZ	0.00
1890	2018-08-20 19:57:36.402835	2	1	TEM	20.00
1893	2018-08-20 19:57:37.429124	2	2	HUM	30.50
1896	2018-08-20 19:57:38.452112	2	3	HUM_SOIL	0.00
1901	2018-08-20 19:57:45.741454	2	4	LUZ	0.00
1902	2018-08-20 19:57:46.749283	2	1	TEM	20.00
1906	2018-08-20 19:57:47.781897	2	2	HUM	30.00
1910	2018-08-20 19:57:48.807943	2	3	HUM_SOIL	0.00
1913	2018-08-20 19:57:56.107546	2	4	LUZ	0.00
1917	2018-08-20 19:57:57.136762	2	1	TEM	20.00
1920	2018-08-20 19:57:58.155345	2	2	HUM	30.20
1923	2018-08-20 19:57:59.189574	2	3	HUM_SOIL	0.00
1925	2018-08-20 19:58:06.469383	2	4	LUZ	0.00
1928	2018-08-20 19:58:07.491242	2	1	TEM	20.00
1931	2018-08-20 19:58:08.521179	2	2	HUM	30.10
1935	2018-08-20 19:58:09.555453	2	3	HUM_SOIL	0.00
1939	2018-08-20 19:58:16.843687	2	4	LUZ	0.00
1942	2018-08-20 19:58:17.866857	2	1	TEM	20.00
1944	2018-08-20 19:58:18.883729	2	2	HUM	32.10
1948	2018-08-20 19:58:19.918914	2	3	HUM_SOIL	0.00
1951	2018-08-20 19:58:27.206694	2	4	LUZ	0.00
1956	2018-08-20 19:58:28.245699	2	1	TEM	20.00
1959	2018-08-20 19:58:29.280023	2	2	HUM	31.00
1962	2018-08-20 19:58:30.287611	2	3	HUM_SOIL	0.00
1966	2018-08-20 19:58:37.584655	2	4	LUZ	0.00
1969	2018-08-20 19:58:38.613253	2	1	TEM	20.00
1971	2018-08-20 19:58:39.626011	2	2	HUM	31.20
1973	2018-08-20 19:58:40.644026	2	3	HUM_SOIL	0.00
1977	2018-08-20 19:58:47.941905	2	4	LUZ	0.00
1982	2018-08-20 19:58:48.98176	2	1	TEM	20.00
1985	2018-08-20 19:58:49.988814	2	2	HUM	30.40
1988	2018-08-20 19:58:51.022188	2	3	HUM_SOIL	0.00
1992	2018-08-20 19:58:58.310894	2	4	LUZ	0.00
1994	2018-08-20 19:58:59.330884	2	1	TEM	20.00
1997	2018-08-20 19:59:00.343511	2	2	HUM	30.10
2000	2018-08-20 19:59:01.373263	2	3	HUM_SOIL	0.00
2005	2018-08-20 19:59:08.694897	2	4	LUZ	0.00
2008	2018-08-20 19:59:09.717543	2	1	TEM	20.10
2009	2018-08-20 19:59:10.740707	2	2	HUM	30.00
2012	2018-08-20 19:59:11.753245	2	3	HUM_SOIL	0.00
2016	2018-08-20 19:59:19.049051	2	4	LUZ	0.00
2020	2018-08-20 19:59:20.056719	2	1	TEM	20.10
2022	2018-08-20 19:59:21.064304	2	2	HUM	30.00
2025	2018-08-20 19:59:22.087277	2	3	HUM_SOIL	0.00
2029	2018-08-20 19:59:29.415227	2	4	LUZ	0.00
2033	2018-08-20 19:59:30.429118	2	1	TEM	20.10
2035	2018-08-20 19:59:31.436385	2	2	HUM	30.00
2038	2018-08-20 19:59:32.459919	2	3	HUM_SOIL	0.00
2043	2018-08-20 19:59:39.784471	2	4	LUZ	0.00
2046	2018-08-20 19:59:40.791355	2	1	TEM	20.10
2049	2018-08-20 19:59:41.79893	2	2	HUM	30.00
2051	2018-08-20 19:59:42.806259	2	3	HUM_SOIL	0.00
3119	2018-08-23 00:12:08.569703	1	1	TEM	21.80
3120	2018-08-23 00:12:18.640377	1	2	HUM	42.80
3121	2018-08-23 00:12:28.685469	1	3	HUM_SOIL	56.00
3122	2018-08-23 00:12:38.729556	1	4	LUZ	0.00
3123	2018-08-23 00:12:48.772718	1	1	TEM	21.80
3124	2018-08-23 00:12:58.81649	1	2	HUM	42.80
3125	2018-08-23 00:13:08.852042	1	3	HUM_SOIL	1.00
3126	2018-08-23 00:13:18.894849	1	4	LUZ	0.00
3127	2018-08-23 00:13:28.937468	1	1	TEM	21.80
3128	2018-08-23 00:13:38.980081	1	2	HUM	42.80
3129	2018-08-23 00:13:49.024284	1	3	HUM_SOIL	99.00
3130	2018-08-23 00:13:59.067111	1	4	LUZ	0.00
1764	2018-08-20 19:55:54.263898	1	1	TEM	20.20
1777	2018-08-20 19:56:04.303825	1	2	HUM	29.10
1787	2018-08-20 19:56:14.343712	1	3	HUM_SOIL	99.00
1800	2018-08-20 19:56:24.383472	1	4	LUZ	0.00
1813	2018-08-20 19:56:34.423449	1	1	TEM	20.30
1823	2018-08-20 19:56:44.46701	1	2	HUM	29.10
1836	2018-08-20 19:56:54.503627	1	3	HUM_SOIL	87.00
1849	2018-08-20 19:57:04.543566	1	4	LUZ	0.00
1859	2018-08-20 19:57:14.583353	1	1	TEM	20.20
1872	2018-08-20 19:57:24.62359	1	2	HUM	29.00
1885	2018-08-20 19:57:34.665592	1	3	HUM_SOIL	89.00
1898	2018-08-20 19:57:44.705501	1	4	LUZ	0.00
1911	2018-08-20 19:57:54.743682	1	1	TEM	20.20
1924	2018-08-20 19:58:04.933574	1	2	HUM	29.00
1937	2018-08-20 19:58:14.975519	1	3	HUM_SOIL	98.00
1950	2018-08-20 19:58:25.022979	1	4	LUZ	0.00
1963	2018-08-20 19:58:35.066031	1	1	TEM	20.20
1976	2018-08-20 19:58:45.103454	1	2	HUM	29.10
1989	2018-08-20 19:58:55.145276	1	3	HUM_SOIL	1.00
2002	2018-08-20 19:59:05.188132	1	4	LUZ	0.00
2015	2018-08-20 19:59:15.223557	1	1	TEM	20.20
2028	2018-08-20 19:59:25.263549	1	2	HUM	29.10
2041	2018-08-20 19:59:35.30509	1	3	HUM_SOIL	84.00
3131	2018-08-23 00:14:22.407586	1	1	TEM	21.80
3132	2018-08-23 00:14:32.476431	1	2	HUM	42.80
3133	2018-08-23 00:14:42.522894	1	3	HUM_SOIL	99.00
3134	2018-08-23 00:14:52.57716	1	4	LUZ	0.00
2054	2018-08-20 19:59:45.349003	1	4	LUZ	0.00
2067	2018-08-20 19:59:55.400052	1	1	TEM	20.40
2080	2018-08-20 20:00:05.43338	1	2	HUM	29.60
2093	2018-08-20 20:00:15.473687	1	3	HUM_SOIL	99.00
2106	2018-08-20 20:00:25.515408	1	4	LUZ	0.00
2119	2018-08-20 20:00:35.553601	1	1	TEM	20.40
2132	2018-08-20 20:00:45.595116	1	2	HUM	29.60
2145	2018-08-20 20:00:55.633818	1	3	HUM_SOIL	79.00
2155	2018-08-20 20:01:05.673892	1	4	LUZ	0.00
2168	2018-08-20 20:01:15.713932	1	1	TEM	20.40
2181	2018-08-20 20:01:25.7563	1	2	HUM	29.60
2206	2018-08-20 20:01:46.584602	1	1	TEM	20.40
2218	2018-08-20 20:01:56.629754	1	2	HUM	29.80
2229	2018-08-20 20:02:06.676005	1	3	HUM_SOIL	99.00
2260	2018-08-20 20:02:31.189157	1	1	TEM	20.40
2273	2018-08-20 20:02:41.259271	1	2	HUM	29.60
2286	2018-08-20 20:02:53.492978	1	1	TEM	20.40
2299	2018-08-20 20:03:03.534993	1	2	HUM	29.60
2312	2018-08-20 20:03:13.575952	1	3	HUM_SOIL	1.00
2325	2018-08-20 20:03:23.61357	1	4	LUZ	0.00
2338	2018-08-20 20:03:33.653779	1	1	TEM	20.50
2351	2018-08-20 20:03:43.693366	1	2	HUM	29.70
3135	2018-08-23 00:15:10.803206	1	1	TEM	21.80
3136	2018-08-23 00:15:20.882242	1	2	HUM	42.80
2056	2018-08-20 19:59:50.153516	2	4	LUZ	0.00
2055	2018-08-20 19:59:50.153516	2	4	LUZ	0.00
2059	2018-08-20 19:59:51.160959	2	1	TEM	20.10
2058	2018-08-20 19:59:51.16096	2	1	TEM	20.10
2061	2018-08-20 19:59:52.168749	2	2	HUM	29.90
2063	2018-08-20 19:59:52.191827	2	2	HUM	29.90
2064	2018-08-20 19:59:53.196863	2	3	HUM_SOIL	0.00
2065	2018-08-20 19:59:53.219686	2	3	HUM_SOIL	0.00
2069	2018-08-20 20:00:00.520657	2	4	LUZ	0.00
2070	2018-08-20 20:00:00.527674	2	4	LUZ	0.00
2071	2018-08-20 20:00:01.528449	2	1	TEM	20.10
2072	2018-08-20 20:00:01.550983	2	1	TEM	20.10
2074	2018-08-20 20:00:02.55078	2	2	HUM	29.90
2075	2018-08-20 20:00:02.560401	2	2	HUM	29.90
2077	2018-08-20 20:00:03.579113	2	3	HUM_SOIL	0.00
2078	2018-08-20 20:00:03.583892	2	3	HUM_SOIL	0.00
2081	2018-08-20 20:00:10.888639	2	4	LUZ	0.00
2083	2018-08-20 20:00:10.90535	2	4	LUZ	0.00
2085	2018-08-20 20:00:11.911886	2	1	TEM	20.10
2086	2018-08-20 20:00:11.928876	2	1	TEM	20.10
2088	2018-08-20 20:00:12.945446	2	2	HUM	29.90
2089	2018-08-20 20:00:12.961927	2	2	HUM	29.90
2091	2018-08-20 20:00:13.963517	2	3	HUM_SOIL	0.00
2092	2018-08-20 20:00:13.980408	2	3	HUM_SOIL	0.00
2095	2018-08-20 20:00:21.257516	2	4	LUZ	0.00
2096	2018-08-20 20:00:21.274187	2	4	LUZ	0.00
2097	2018-08-20 20:00:22.276501	2	1	TEM	20.10
2099	2018-08-20 20:00:22.29349	2	1	TEM	20.10
2100	2018-08-20 20:00:23.305159	2	2	HUM	29.90
2102	2018-08-20 20:00:23.321589	2	2	HUM	29.90
2103	2018-08-20 20:00:24.312773	2	3	HUM_SOIL	0.00
2104	2018-08-20 20:00:24.339403	2	3	HUM_SOIL	0.00
2107	2018-08-20 20:00:31.634667	2	4	LUZ	0.00
2109	2018-08-20 20:00:31.650865	2	4	LUZ	0.00
2111	2018-08-20 20:00:32.673697	2	1	TEM	20.10
2112	2018-08-20 20:00:32.684679	2	1	TEM	20.10
2114	2018-08-20 20:00:33.707324	2	2	HUM	29.90
2115	2018-08-20 20:00:33.718983	2	2	HUM	29.90
2117	2018-08-20 20:00:34.73649	2	3	HUM_SOIL	0.00
2118	2018-08-20 20:00:34.741614	2	3	HUM_SOIL	0.00
2121	2018-08-20 20:00:41.99433	2	4	LUZ	0.00
2122	2018-08-20 20:00:41.99599	2	4	LUZ	0.00
2124	2018-08-20 20:00:43.003364	2	1	TEM	20.10
2125	2018-08-20 20:00:43.020008	2	1	TEM	20.10
2126	2018-08-20 20:00:44.019958	2	2	HUM	30.00
2128	2018-08-20 20:00:44.042201	2	2	HUM	30.00
2129	2018-08-20 20:00:45.052929	2	3	HUM_SOIL	0.00
2130	2018-08-20 20:00:45.064615	2	3	HUM_SOIL	0.00
2133	2018-08-20 20:00:52.36318	2	4	LUZ	0.00
2135	2018-08-20 20:00:52.371733	2	4	LUZ	0.00
2137	2018-08-20 20:00:53.379861	2	1	TEM	20.10
2138	2018-08-20 20:00:53.388643	2	1	TEM	20.10
2139	2018-08-20 20:00:54.396849	2	2	HUM	30.10
2140	2018-08-20 20:00:54.404069	2	2	HUM	30.10
2143	2018-08-20 20:00:55.424616	2	3	HUM_SOIL	0.00
2144	2018-08-20 20:00:55.427688	2	3	HUM_SOIL	0.00
2147	2018-08-20 20:01:02.731403	2	4	LUZ	0.00
2148	2018-08-20 20:01:02.731936	2	4	LUZ	0.00
2149	2018-08-20 20:01:03.739459	2	1	TEM	20.10
2151	2018-08-20 20:01:03.746454	2	1	TEM	20.10
2153	2018-08-20 20:01:04.757881	2	2	HUM	30.50
2154	2018-08-20 20:01:04.765047	2	2	HUM	30.50
2157	2018-08-20 20:01:05.779522	2	3	HUM_SOIL	0.00
2158	2018-08-20 20:01:05.785154	2	3	HUM_SOIL	0.00
2159	2018-08-20 20:01:13.156712	2	4	LUZ	0.00
2160	2018-08-20 20:01:13.159634	2	4	LUZ	0.00
2162	2018-08-20 20:01:14.182271	2	1	TEM	20.10
2163	2018-08-20 20:01:14.192537	2	1	TEM	20.10
2165	2018-08-20 20:01:15.200489	2	2	HUM	30.30
2166	2018-08-20 20:01:15.221509	2	2	HUM	30.30
2169	2018-08-20 20:01:16.229158	2	3	HUM_SOIL	0.00
2170	2018-08-20 20:01:16.229158	2	3	HUM_SOIL	0.00
2172	2018-08-20 20:01:23.466753	2	4	LUZ	0.00
2173	2018-08-20 20:01:23.473166	2	4	LUZ	0.00
2175	2018-08-20 20:01:24.490398	2	1	TEM	20.20
2176	2018-08-20 20:01:24.491472	2	1	TEM	20.20
2178	2018-08-20 20:01:25.513974	2	2	HUM	30.90
2179	2018-08-20 20:01:25.513974	2	2	HUM	30.90
2183	2018-08-20 20:01:26.52179	2	3	HUM_SOIL	0.00
2184	2018-08-20 20:01:26.524406	2	3	HUM_SOIL	0.00
2185	2018-08-20 20:01:33.835344	2	4	LUZ	0.00
2186	2018-08-20 20:01:33.835344	2	4	LUZ	0.00
2188	2018-08-20 20:01:34.842518	2	1	TEM	20.20
2189	2018-08-20 20:01:34.84987	2	1	TEM	20.20
2191	2018-08-20 20:01:35.855261	2	2	HUM	32.00
2192	2018-08-20 20:01:35.873718	2	2	HUM	32.00
2194	2018-08-20 20:01:36.863454	2	3	HUM_SOIL	0.00
2195	2018-08-20 20:01:36.894413	2	3	HUM_SOIL	0.00
2197	2018-08-20 20:01:44.203137	2	4	LUZ	0.00
2199	2018-08-20 20:01:44.21426	2	4	LUZ	0.00
2200	2018-08-20 20:01:45.210716	2	1	TEM	20.20
2202	2018-08-20 20:01:45.237557	2	1	TEM	20.20
2203	2018-08-20 20:01:46.2342	2	2	HUM	31.60
2205	2018-08-20 20:01:46.26933	2	2	HUM	31.60
2207	2018-08-20 20:01:47.241389	2	3	HUM_SOIL	0.00
2209	2018-08-20 20:01:47.278207	2	3	HUM_SOIL	0.00
2210	2018-08-20 20:01:54.571829	2	4	LUZ	0.00
2211	2018-08-20 20:01:54.57367	2	4	LUZ	0.00
2213	2018-08-20 20:01:55.589623	2	1	TEM	20.20
2215	2018-08-20 20:01:55.605388	2	1	TEM	20.20
2217	2018-08-20 20:01:56.624228	2	2	HUM	30.70
2219	2018-08-20 20:01:56.624119	2	2	HUM	30.70
2221	2018-08-20 20:01:57.641884	2	3	HUM_SOIL	0.00
2222	2018-08-20 20:01:57.647125	2	3	HUM_SOIL	0.00
2224	2018-08-20 20:02:04.940236	2	4	LUZ	0.00
2225	2018-08-20 20:02:04.95679	2	4	LUZ	0.00
2227	2018-08-20 20:02:05.974188	2	1	TEM	20.20
2228	2018-08-20 20:02:05.974686	2	1	TEM	20.20
2230	2018-08-20 20:02:06.990818	2	2	HUM	30.20
2232	2018-08-20 20:02:06.999959	2	2	HUM	30.20
2234	2018-08-20 20:02:08.009816	2	3	HUM_SOIL	0.00
2235	2018-08-20 20:02:08.008004	2	3	HUM_SOIL	0.00
2236	2018-08-20 20:02:15.308641	2	4	LUZ	0.00
2238	2018-08-20 20:02:15.327137	2	4	LUZ	0.00
2239	2018-08-20 20:02:16.326038	2	1	TEM	20.20
2241	2018-08-20 20:02:16.34519	2	1	TEM	20.20
2243	2018-08-20 20:02:17.362103	2	2	HUM	30.00
2244	2018-08-20 20:02:17.366075	2	2	HUM	30.00
2246	2018-08-20 20:02:18.373079	2	3	HUM_SOIL	0.00
2247	2018-08-20 20:02:18.380062	2	3	HUM_SOIL	0.00
2057	2018-08-20 19:59:50.155402	2	4	LUZ	0.00
2060	2018-08-20 19:59:51.183406	2	1	TEM	20.10
2062	2018-08-20 19:59:52.190607	2	2	HUM	29.90
2066	2018-08-20 19:59:53.222675	2	3	HUM_SOIL	0.00
2068	2018-08-20 20:00:00.520891	2	4	LUZ	0.00
2073	2018-08-20 20:00:01.555142	2	1	TEM	20.10
2076	2018-08-20 20:00:02.580178	2	2	HUM	29.90
2079	2018-08-20 20:00:03.603574	2	3	HUM_SOIL	0.00
2082	2018-08-20 20:00:10.890412	2	4	LUZ	0.00
2084	2018-08-20 20:00:11.896808	2	1	TEM	20.10
2087	2018-08-20 20:00:12.925347	2	2	HUM	29.90
2090	2018-08-20 20:00:13.942547	2	3	HUM_SOIL	0.00
2094	2018-08-20 20:00:21.257516	2	4	LUZ	0.00
2098	2018-08-20 20:00:22.291736	2	1	TEM	20.10
2101	2018-08-20 20:00:23.310864	2	2	HUM	29.90
2105	2018-08-20 20:00:24.349752	2	3	HUM_SOIL	0.00
2108	2018-08-20 20:00:31.634667	2	4	LUZ	0.00
2110	2018-08-20 20:00:32.657745	2	1	TEM	20.10
2113	2018-08-20 20:00:33.680505	2	2	HUM	29.90
2116	2018-08-20 20:00:34.688316	2	3	HUM_SOIL	0.00
2120	2018-08-20 20:00:41.99407	2	4	LUZ	0.00
2123	2018-08-20 20:00:43.001647	2	1	TEM	20.10
2127	2018-08-20 20:00:44.034709	2	2	HUM	30.00
2131	2018-08-20 20:00:45.068267	2	3	HUM_SOIL	0.00
2134	2018-08-20 20:00:52.366352	2	4	LUZ	0.00
2136	2018-08-20 20:00:53.373245	2	1	TEM	20.10
2141	2018-08-20 20:00:54.407003	2	2	HUM	30.10
2142	2018-08-20 20:00:55.424612	2	3	HUM_SOIL	0.00
2146	2018-08-20 20:01:02.731403	2	4	LUZ	0.00
2150	2018-08-20 20:01:03.739151	2	1	TEM	20.10
2152	2018-08-20 20:01:04.747185	2	2	HUM	30.50
2156	2018-08-20 20:01:05.754291	2	3	HUM_SOIL	0.00
2161	2018-08-20 20:01:13.170242	2	4	LUZ	0.00
2164	2018-08-20 20:01:14.208437	2	1	TEM	20.10
2167	2018-08-20 20:01:15.251932	2	2	HUM	30.30
2171	2018-08-20 20:01:16.274843	2	3	HUM_SOIL	0.00
2174	2018-08-20 20:01:23.473166	2	4	LUZ	0.00
2177	2018-08-20 20:01:24.497585	2	1	TEM	20.20
2180	2018-08-20 20:01:25.513974	2	2	HUM	30.90
2182	2018-08-20 20:01:26.521551	2	3	HUM_SOIL	0.00
2187	2018-08-20 20:01:33.842705	2	4	LUZ	0.00
2190	2018-08-20 20:01:34.867168	2	1	TEM	20.20
2193	2018-08-20 20:01:35.898833	2	2	HUM	32.00
2196	2018-08-20 20:01:36.918018	2	3	HUM_SOIL	0.00
2198	2018-08-20 20:01:44.203753	2	4	LUZ	0.00
2201	2018-08-20 20:01:45.21792	2	1	TEM	20.20
2204	2018-08-20 20:01:46.2342	2	2	HUM	31.60
2208	2018-08-20 20:01:47.263607	2	3	HUM_SOIL	0.00
2212	2018-08-20 20:01:54.579205	2	4	LUZ	0.00
2214	2018-08-20 20:01:55.596655	2	1	TEM	20.20
2216	2018-08-20 20:01:56.613936	2	2	HUM	30.70
2220	2018-08-20 20:01:57.641884	2	3	HUM_SOIL	0.00
2223	2018-08-20 20:02:04.939836	2	4	LUZ	0.00
2226	2018-08-20 20:02:05.95904	2	1	TEM	20.20
2231	2018-08-20 20:02:06.99362	2	2	HUM	30.20
2233	2018-08-20 20:02:08.00758	2	3	HUM_SOIL	0.00
2237	2018-08-20 20:02:15.308868	2	4	LUZ	0.00
2240	2018-08-20 20:02:16.320902	2	1	TEM	20.20
2242	2018-08-20 20:02:17.329405	2	2	HUM	30.00
2245	2018-08-20 20:02:18.335903	2	3	HUM_SOIL	0.00
2248	2018-08-20 20:02:25.677199	2	4	LUZ	0.00
2251	2018-08-20 20:02:26.686351	2	1	TEM	20.20
2256	2018-08-20 20:02:27.717084	2	2	HUM	30.00
2258	2018-08-20 20:02:28.734917	2	3	HUM_SOIL	0.00
2263	2018-08-20 20:02:36.046052	2	4	LUZ	0.00
2265	2018-08-20 20:02:37.061027	2	1	TEM	20.20
2268	2018-08-20 20:02:38.079413	2	2	HUM	29.90
2271	2018-08-20 20:02:39.102654	2	3	HUM_SOIL	0.00
2274	2018-08-20 20:02:46.411985	2	4	LUZ	0.00
2277	2018-08-20 20:02:47.419076	2	1	TEM	20.30
2280	2018-08-20 20:02:48.42575	2	2	HUM	30.00
2283	2018-08-20 20:02:49.433307	2	3	HUM_SOIL	0.00
2288	2018-08-20 20:02:56.781791	2	4	LUZ	0.00
2292	2018-08-20 20:02:57.789486	2	1	TEM	20.30
2294	2018-08-20 20:02:58.796671	2	2	HUM	30.00
2297	2018-08-20 20:02:59.820068	2	3	HUM_SOIL	0.00
2300	2018-08-20 20:03:07.150959	2	4	LUZ	0.00
2304	2018-08-20 20:03:08.174511	2	1	TEM	20.30
2307	2018-08-20 20:03:09.193223	2	2	HUM	29.90
2311	2018-08-20 20:03:10.227217	2	3	HUM_SOIL	0.00
2315	2018-08-20 20:03:17.539993	2	4	LUZ	0.00
2318	2018-08-20 20:03:18.578393	2	1	TEM	20.30
2321	2018-08-20 20:03:19.617561	2	2	HUM	30.50
2324	2018-08-20 20:03:20.642649	2	3	HUM_SOIL	0.00
2327	2018-08-20 20:03:27.890943	2	4	LUZ	0.00
2329	2018-08-20 20:03:28.908422	2	1	TEM	20.30
2332	2018-08-20 20:03:29.92117	2	2	HUM	30.30
2335	2018-08-20 20:03:30.944282	2	3	HUM_SOIL	0.00
2340	2018-08-20 20:03:38.256513	2	4	LUZ	0.00
2343	2018-08-20 20:03:39.289324	2	1	TEM	20.30
2347	2018-08-20 20:03:40.328934	2	2	HUM	30.10
2350	2018-08-20 20:03:41.362562	2	3	HUM_SOIL	0.00
3137	2018-08-23 00:16:36.247439	1	1	TEM	21.80
3138	2018-08-23 00:16:46.314709	1	2	HUM	42.80
2249	2018-08-20 20:02:25.67697	2	4	LUZ	0.00
2252	2018-08-20 20:02:26.689616	2	1	TEM	20.20
2254	2018-08-20 20:02:27.691675	2	2	HUM	30.00
2257	2018-08-20 20:02:28.705788	2	3	HUM_SOIL	0.00
2262	2018-08-20 20:02:36.046052	2	4	LUZ	0.00
2264	2018-08-20 20:02:37.057373	2	1	TEM	20.20
2269	2018-08-20 20:02:38.087897	2	2	HUM	29.90
2270	2018-08-20 20:02:39.09475	2	3	HUM_SOIL	0.00
2275	2018-08-20 20:02:46.414335	2	4	LUZ	0.00
2279	2018-08-20 20:02:47.448206	2	1	TEM	20.30
2282	2018-08-20 20:02:48.471454	2	2	HUM	30.00
2285	2018-08-20 20:02:49.499699	2	3	HUM_SOIL	0.00
2289	2018-08-20 20:02:56.781791	2	4	LUZ	0.00
2291	2018-08-20 20:02:57.789486	2	1	TEM	20.30
2293	2018-08-20 20:02:58.796657	2	2	HUM	30.00
2296	2018-08-20 20:02:59.814604	2	3	HUM_SOIL	0.00
2302	2018-08-20 20:03:07.150959	2	4	LUZ	0.00
2305	2018-08-20 20:03:08.177538	2	1	TEM	20.30
2308	2018-08-20 20:03:09.209927	2	2	HUM	29.90
2310	2018-08-20 20:03:10.22553	2	3	HUM_SOIL	0.00
2314	2018-08-20 20:03:17.521303	2	4	LUZ	0.00
2317	2018-08-20 20:03:18.551887	2	1	TEM	20.30
2319	2018-08-20 20:03:19.574262	2	2	HUM	30.50
2323	2018-08-20 20:03:20.603343	2	3	HUM_SOIL	0.00
2326	2018-08-20 20:03:27.890631	2	4	LUZ	0.00
2330	2018-08-20 20:03:28.915753	2	1	TEM	20.30
2333	2018-08-20 20:03:29.937324	2	2	HUM	30.30
2336	2018-08-20 20:03:30.94996	2	3	HUM_SOIL	0.00
2341	2018-08-20 20:03:38.256393	2	4	LUZ	0.00
2344	2018-08-20 20:03:39.295754	2	1	TEM	20.30
2346	2018-08-20 20:03:40.3177	2	2	HUM	30.10
2349	2018-08-20 20:03:41.340695	2	3	HUM_SOIL	0.00
3139	2018-08-23 00:18:52.788091	1	1	TEM	21.90
3140	2018-08-23 00:19:02.859507	1	2	HUM	42.80
3141	2018-08-23 00:19:12.897129	1	3	HUM_SOIL	77.00
3142	2018-08-23 00:19:22.943979	1	4	LUZ	0.00
3143	2018-08-23 00:19:32.983667	1	1	TEM	21.90
3144	2018-08-23 00:19:43.029728	1	2	HUM	42.80
3145	2018-08-23 00:19:53.075696	1	3	HUM_SOIL	1.00
3146	2018-08-23 00:20:03.119355	1	4	LUZ	0.00
3147	2018-08-23 00:20:13.163514	1	1	TEM	21.90
3148	2018-08-23 00:20:23.199086	1	2	HUM	42.80
3149	2018-08-23 00:20:33.245581	1	3	HUM_SOIL	99.00
3150	2018-08-23 00:20:43.283786	1	4	LUZ	0.00
3151	2018-08-23 00:20:53.323582	1	1	TEM	21.90
3152	2018-08-23 00:21:03.363581	1	2	HUM	42.80
3153	2018-08-23 00:21:13.409042	1	3	HUM_SOIL	59.00
3154	2018-08-23 00:21:23.455054	1	4	LUZ	0.00
3155	2018-08-23 00:21:33.493693	1	1	TEM	21.90
3156	2018-08-23 00:21:43.533618	1	2	HUM	42.80
3157	2018-08-23 00:21:53.578964	1	3	HUM_SOIL	1.00
3158	2018-08-23 00:22:03.620405	1	4	LUZ	0.00
3159	2018-08-23 00:22:13.656946	1	1	TEM	21.90
3160	2018-08-23 00:22:23.693625	1	2	HUM	42.80
3161	2018-08-23 00:22:33.734077	1	3	HUM_SOIL	99.00
3162	2018-08-23 00:22:43.772757	1	4	LUZ	0.00
3163	2018-08-23 00:22:53.817764	1	1	TEM	22.00
3164	2018-08-23 00:23:03.861026	1	2	HUM	42.90
3165	2018-08-23 00:23:13.903727	1	3	HUM_SOIL	1.00
3166	2018-08-23 00:23:23.946707	1	4	LUZ	0.00
3167	2018-08-23 00:23:33.994023	1	1	TEM	22.00
3168	2018-08-23 00:23:44.033597	1	2	HUM	42.90
3169	2018-08-23 00:23:54.073715	1	3	HUM_SOIL	1.00
3170	2018-08-23 00:24:04.11388	1	4	LUZ	0.00
3171	2018-08-23 00:24:14.158858	1	1	TEM	21.90
3172	2018-08-23 00:24:24.206839	1	2	HUM	42.80
3173	2018-08-23 00:24:34.253938	1	3	HUM_SOIL	93.00
3174	2018-08-23 00:24:44.293978	1	4	LUZ	0.00
3175	2018-08-23 00:24:54.333986	1	1	TEM	22.00
3176	2018-08-23 00:25:04.373668	1	2	HUM	42.90
3177	2018-08-23 00:25:14.410874	1	3	HUM_SOIL	99.00
3178	2018-08-23 00:25:24.454121	1	4	LUZ	0.00
3179	2018-08-23 00:25:34.493679	1	1	TEM	22.00
3180	2018-08-23 00:25:44.540504	1	2	HUM	42.80
3181	2018-08-23 00:25:54.588093	1	3	HUM_SOIL	99.00
3182	2018-08-23 00:26:04.634924	1	4	LUZ	0.00
3183	2018-08-23 00:26:14.67483	1	1	TEM	22.00
3184	2018-08-23 00:26:24.714614	1	2	HUM	42.80
3185	2018-08-23 00:26:34.755225	1	3	HUM_SOIL	9.00
3186	2018-08-23 00:26:44.794872	1	4	LUZ	0.00
3187	2018-08-23 00:26:54.835073	1	1	TEM	22.00
3188	2018-08-23 00:27:04.882724	1	2	HUM	42.80
3189	2018-08-23 00:27:14.932248	1	3	HUM_SOIL	99.00
3190	2018-08-23 00:27:24.973691	1	4	LUZ	0.00
3191	2018-08-23 00:27:35.013954	1	1	TEM	22.00
3192	2018-08-23 00:27:45.053713	1	2	HUM	42.80
3193	2018-08-23 00:27:55.09067	1	3	HUM_SOIL	21.00
3194	2018-08-23 00:28:05.136271	1	4	LUZ	0.00
3195	2018-08-23 00:28:15.174027	1	1	TEM	22.00
3196	2018-08-23 00:28:25.21388	1	2	HUM	42.80
3197	2018-08-23 00:28:35.258045	1	3	HUM_SOIL	99.00
3198	2018-08-23 00:28:45.303104	1	4	LUZ	0.00
3199	2018-08-23 00:28:55.34186	1	1	TEM	22.00
3200	2018-08-23 00:29:05.383515	1	2	HUM	42.80
3201	2018-08-23 00:29:15.424119	1	3	HUM_SOIL	1.00
3202	2018-08-23 00:29:25.469581	1	4	LUZ	0.00
3203	2018-08-23 00:29:35.516285	1	1	TEM	22.00
3204	2018-08-23 00:29:45.553523	1	2	HUM	42.80
3205	2018-08-23 00:29:55.593653	1	3	HUM_SOIL	99.00
3206	2018-08-23 00:30:05.633924	1	4	LUZ	0.00
3207	2018-08-23 00:30:15.678549	1	1	TEM	22.00
3208	2018-08-23 00:30:25.723611	1	2	HUM	42.80
3209	2018-08-23 00:30:35.763712	1	3	HUM_SOIL	1.00
3210	2018-08-23 00:30:45.803743	1	4	LUZ	0.00
3211	2018-08-23 00:30:55.843578	1	1	TEM	22.00
3212	2018-08-23 00:31:05.888118	1	2	HUM	42.80
3213	2018-08-23 00:31:15.929991	1	3	HUM_SOIL	99.00
3214	2018-08-23 00:31:25.973753	1	4	LUZ	0.00
3215	2018-08-23 00:31:36.013537	1	1	TEM	22.00
3216	2018-08-23 00:31:46.058303	1	2	HUM	42.80
3217	2018-08-23 00:31:56.103285	1	3	HUM_SOIL	99.00
3218	2018-08-23 00:32:06.14369	1	4	LUZ	0.00
3219	2018-08-23 00:32:16.183473	1	1	TEM	22.00
3220	2018-08-23 00:32:26.223483	1	2	HUM	42.80
3221	2018-08-23 00:32:36.262835	1	3	HUM_SOIL	1.00
3222	2018-08-23 00:32:46.30786	1	4	LUZ	0.00
3223	2018-08-23 00:32:56.343543	1	1	TEM	22.00
3224	2018-08-23 00:33:06.383535	1	2	HUM	42.90
3225	2018-08-23 00:33:16.428697	1	3	HUM_SOIL	61.00
3226	2018-08-23 00:33:26.4738	1	4	LUZ	0.00
2250	2018-08-20 20:02:25.678707	2	4	LUZ	0.00
2253	2018-08-20 20:02:26.702886	2	1	TEM	20.20
2255	2018-08-20 20:02:27.715673	2	2	HUM	30.00
2259	2018-08-20 20:02:28.749402	2	3	HUM_SOIL	0.00
2261	2018-08-20 20:02:36.046288	2	4	LUZ	0.00
2266	2018-08-20 20:02:37.056631	2	1	TEM	20.20
2267	2018-08-20 20:02:38.071947	2	2	HUM	29.90
2272	2018-08-20 20:02:39.107169	2	3	HUM_SOIL	0.00
2276	2018-08-20 20:02:46.421353	2	4	LUZ	0.00
2278	2018-08-20 20:02:47.428614	2	1	TEM	20.30
2281	2018-08-20 20:02:48.447244	2	2	HUM	30.00
2284	2018-08-20 20:02:49.469595	2	3	HUM_SOIL	0.00
2287	2018-08-20 20:02:56.781791	2	4	LUZ	0.00
2290	2018-08-20 20:02:57.789486	2	1	TEM	20.30
2295	2018-08-20 20:02:58.802647	2	2	HUM	30.00
2298	2018-08-20 20:02:59.836854	2	3	HUM_SOIL	0.00
2301	2018-08-20 20:03:07.151183	2	4	LUZ	0.00
2303	2018-08-20 20:03:08.159434	2	1	TEM	20.30
2306	2018-08-20 20:03:09.16624	2	2	HUM	29.90
2309	2018-08-20 20:03:10.179021	2	3	HUM_SOIL	0.00
2313	2018-08-20 20:03:17.521303	2	4	LUZ	0.00
2316	2018-08-20 20:03:18.546192	2	1	TEM	20.30
2320	2018-08-20 20:03:19.581588	2	2	HUM	30.50
2322	2018-08-20 20:03:20.599109	2	3	HUM_SOIL	0.00
2328	2018-08-20 20:03:27.910949	2	4	LUZ	0.00
2331	2018-08-20 20:03:28.94035	2	1	TEM	20.30
2334	2018-08-20 20:03:29.973096	2	2	HUM	30.30
2337	2018-08-20 20:03:31.001091	2	3	HUM_SOIL	0.00
2339	2018-08-20 20:03:38.25758	2	4	LUZ	0.00
2342	2018-08-20 20:03:39.274719	2	1	TEM	20.30
2345	2018-08-20 20:03:40.312551	2	2	HUM	30.10
2348	2018-08-20 20:03:41.340695	2	3	HUM_SOIL	0.00
3227	2018-08-23 00:33:36.513543	1	1	TEM	22.00
3228	2018-08-23 00:33:46.553526	1	2	HUM	42.80
3229	2018-08-23 00:33:56.593698	1	3	HUM_SOIL	1.00
3230	2018-08-23 00:34:06.639303	1	4	LUZ	0.00
3231	2018-08-23 00:34:16.686109	1	1	TEM	22.00
3232	2018-08-23 00:34:26.723886	1	2	HUM	42.80
3233	2018-08-23 00:34:36.763801	1	3	HUM_SOIL	99.00
3234	2018-08-23 00:34:46.809142	1	4	LUZ	0.00
3235	2018-08-23 00:34:56.854756	1	1	TEM	22.00
3236	2018-08-23 00:35:06.895047	1	2	HUM	42.80
3237	2018-08-23 00:35:16.933817	1	3	HUM_SOIL	99.00
3238	2018-08-23 00:35:26.973842	1	4	LUZ	0.00
3239	2018-08-23 00:35:37.021677	1	1	TEM	22.00
3240	2018-08-23 00:35:47.06358	1	2	HUM	42.80
3241	2018-08-23 00:35:57.101002	1	3	HUM_SOIL	99.00
3242	2018-08-23 00:36:07.143792	1	4	LUZ	0.00
3243	2018-08-23 00:36:17.183594	1	1	TEM	22.00
3244	2018-08-23 00:36:27.228375	1	2	HUM	42.90
3245	2018-08-23 00:36:37.275772	1	3	HUM_SOIL	89.00
3246	2018-08-23 00:36:47.31384	1	4	LUZ	0.00
3247	2018-08-23 00:36:57.355416	1	1	TEM	22.00
3248	2018-08-23 00:37:07.393612	1	2	HUM	42.90
3249	2018-08-23 00:37:17.4394	1	3	HUM_SOIL	1.00
3250	2018-08-23 00:37:27.479993	1	4	LUZ	0.00
3251	2018-08-23 00:37:37.523578	1	1	TEM	21.90
3252	2018-08-23 00:37:47.567502	1	2	HUM	42.90
3253	2018-08-23 00:37:57.604015	1	3	HUM_SOIL	99.00
3254	2018-08-23 00:38:07.649202	1	4	LUZ	0.00
3255	2018-08-23 00:38:17.684951	1	1	TEM	21.90
3256	2018-08-23 00:38:27.723567	1	2	HUM	42.90
3257	2018-08-23 00:38:37.763872	1	3	HUM_SOIL	99.00
3258	2018-08-23 00:38:47.808706	1	4	LUZ	0.00
3259	2018-08-23 00:38:57.85339	1	1	TEM	21.90
3260	2018-08-23 00:39:07.889748	1	2	HUM	43.00
3261	2018-08-23 00:39:17.933801	1	3	HUM_SOIL	99.00
3262	2018-08-23 00:39:27.973789	1	4	LUZ	0.00
3263	2018-08-23 00:39:38.020527	1	1	TEM	21.90
3264	2018-08-23 00:39:48.065033	1	2	HUM	43.00
3265	2018-08-23 00:39:58.103789	1	3	HUM_SOIL	1.00
3266	2018-08-23 00:40:08.144082	1	4	LUZ	0.00
3267	2018-08-23 00:40:18.188965	1	1	TEM	21.90
3268	2018-08-23 00:40:28.23494	1	2	HUM	43.00
3269	2018-08-23 00:40:38.273799	1	3	HUM_SOIL	74.00
3270	2018-08-23 00:40:48.313779	1	4	LUZ	0.00
3271	2018-08-23 00:40:58.353593	1	1	TEM	21.80
3272	2018-08-23 00:41:08.393814	1	2	HUM	43.00
3273	2018-08-23 00:41:18.439004	1	3	HUM_SOIL	1.00
3274	2018-08-23 00:41:28.483831	1	4	LUZ	0.00
3275	2018-08-23 00:41:38.523307	1	1	TEM	21.80
3276	2018-08-23 00:41:48.562635	1	2	HUM	43.00
3277	2018-08-23 00:41:58.607009	1	3	HUM_SOIL	1.00
3278	2018-08-23 00:42:08.643913	1	4	LUZ	0.00
3279	2018-08-23 00:42:18.68325	1	1	TEM	21.80
3280	2018-08-23 00:42:28.723197	1	2	HUM	43.00
3281	2018-08-23 00:42:38.769488	1	3	HUM_SOIL	1.00
3282	2018-08-23 00:42:48.814816	1	4	LUZ	0.00
3283	2018-08-23 00:42:58.859417	1	1	TEM	21.80
3284	2018-08-23 00:43:08.903343	1	2	HUM	43.10
3285	2018-08-23 00:43:18.943501	1	3	HUM_SOIL	99.00
3286	2018-08-23 00:43:28.983426	1	4	LUZ	0.00
3287	2018-08-23 00:43:39.025157	1	1	TEM	21.80
3288	2018-08-23 00:43:49.063226	1	2	HUM	43.10
3289	2018-08-23 00:43:59.103428	1	3	HUM_SOIL	99.00
3290	2018-08-23 00:44:09.147543	1	4	LUZ	0.00
3291	2018-08-23 00:44:19.193456	1	1	TEM	21.80
3292	2018-08-23 00:44:29.233207	1	2	HUM	43.10
3293	2018-08-23 00:44:39.273452	1	3	HUM_SOIL	61.00
3294	2018-08-23 00:44:49.313491	1	4	LUZ	0.00
3295	2018-08-23 00:44:59.353307	1	1	TEM	21.80
3296	2018-08-23 00:45:09.393526	1	2	HUM	43.20
3297	2018-08-23 00:45:19.433488	1	3	HUM_SOIL	99.00
3298	2018-08-23 00:45:29.473483	1	4	LUZ	0.00
3299	2018-08-23 00:45:39.513253	1	1	TEM	21.80
3300	2018-08-23 00:45:49.554752	1	2	HUM	43.20
3301	2018-08-23 00:45:59.600235	1	3	HUM_SOIL	99.00
3302	2018-08-23 00:46:09.643463	1	4	LUZ	0.00
3303	2018-08-23 00:46:19.683268	1	1	TEM	21.70
3304	2018-08-23 00:46:29.723288	1	2	HUM	43.10
3305	2018-08-23 00:46:39.769123	1	3	HUM_SOIL	1.00
3306	2018-08-23 00:46:49.814067	1	4	LUZ	0.00
3307	2018-08-23 00:46:59.853311	1	1	TEM	21.70
3308	2018-08-23 00:47:09.89828	1	2	HUM	43.10
3309	2018-08-23 00:47:19.933353	1	3	HUM_SOIL	1.00
3310	2018-08-23 00:47:29.977458	1	4	LUZ	0.00
3311	2018-08-23 00:47:40.016827	1	1	TEM	21.70
3312	2018-08-23 00:47:50.05341	1	2	HUM	43.20
3313	2018-08-23 00:48:00.093515	1	3	HUM_SOIL	99.00
3314	2018-08-23 00:48:10.137783	1	4	LUZ	0.00
2352	2018-08-20 20:03:48.627291	2	4	LUZ	0.00
2356	2018-08-20 20:03:49.64152	2	1	TEM	20.30
2358	2018-08-20 20:03:50.647545	2	2	HUM	30.00
2362	2018-08-20 20:03:51.66061	2	3	HUM_SOIL	0.00
2366	2018-08-20 20:03:58.993092	2	4	LUZ	0.00
2368	2018-08-20 20:04:00.011521	2	1	TEM	20.30
2371	2018-08-20 20:04:01.039436	2	2	HUM	29.80
2374	2018-08-20 20:04:02.063341	2	3	HUM_SOIL	0.00
2379	2018-08-20 20:04:09.374693	2	4	LUZ	0.00
2383	2018-08-20 20:04:10.408917	2	1	TEM	20.30
2386	2018-08-20 20:04:11.426314	2	2	HUM	29.70
2389	2018-08-20 20:04:12.441352	2	3	HUM_SOIL	0.00
2392	2018-08-20 20:04:19.729121	2	4	LUZ	0.00
2394	2018-08-20 20:04:20.741327	2	1	TEM	20.30
2397	2018-08-20 20:04:21.766114	2	2	HUM	29.60
2402	2018-08-20 20:04:22.799075	2	3	HUM_SOIL	0.00
2406	2018-08-20 20:04:30.100378	2	4	LUZ	0.00
2409	2018-08-20 20:04:31.128019	2	1	TEM	20.30
2411	2018-08-20 20:04:32.135501	2	2	HUM	29.60
2415	2018-08-20 20:04:33.170757	2	3	HUM_SOIL	0.00
2419	2018-08-20 20:04:40.465047	2	4	LUZ	0.00
2420	2018-08-20 20:04:41.470984	2	1	TEM	20.30
2425	2018-08-20 20:04:42.479718	2	2	HUM	29.60
2427	2018-08-20 20:04:43.493092	2	3	HUM_SOIL	0.00
2432	2018-08-20 20:04:50.834815	2	4	LUZ	0.00
2434	2018-08-20 20:04:51.857702	2	1	TEM	20.30
2437	2018-08-20 20:04:52.890491	2	2	HUM	29.70
2440	2018-08-20 20:04:53.918859	2	3	HUM_SOIL	0.00
2444	2018-08-20 20:05:01.204285	2	4	LUZ	0.00
2447	2018-08-20 20:05:02.237879	2	1	TEM	20.30
2450	2018-08-20 20:05:03.252046	2	2	HUM	29.70
2454	2018-08-20 20:05:04.285544	2	3	HUM_SOIL	0.00
2457	2018-08-20 20:05:11.571712	2	4	LUZ	0.00
2460	2018-08-20 20:05:12.579997	2	1	TEM	20.30
2462	2018-08-20 20:05:13.597312	2	2	HUM	29.60
2466	2018-08-20 20:05:14.604576	2	3	HUM_SOIL	0.00
2470	2018-08-20 20:05:21.94159	2	4	LUZ	0.00
2473	2018-08-20 20:05:22.954597	2	1	TEM	20.40
2476	2018-08-20 20:05:23.978107	2	2	HUM	29.60
2480	2018-08-20 20:05:24.984024	2	3	HUM_SOIL	0.00
2482	2018-08-20 20:05:32.333236	2	4	LUZ	0.00
2486	2018-08-20 20:05:33.361443	2	1	TEM	20.40
2490	2018-08-20 20:05:34.380202	2	2	HUM	29.60
2494	2018-08-20 20:05:35.401951	2	3	HUM_SOIL	0.00
2497	2018-08-20 20:05:42.676428	2	4	LUZ	0.00
2499	2018-08-20 20:05:43.685921	2	1	TEM	20.40
2501	2018-08-20 20:05:44.692999	2	2	HUM	29.70
2504	2018-08-20 20:05:45.720542	2	3	HUM_SOIL	0.00
2509	2018-08-20 20:05:53.053642	2	4	LUZ	0.00
2513	2018-08-20 20:05:54.087836	2	1	TEM	20.40
2515	2018-08-20 20:05:55.104236	2	2	HUM	29.70
2519	2018-08-20 20:05:56.138278	2	3	HUM_SOIL	0.00
2522	2018-08-20 20:06:03.415809	2	4	LUZ	0.00
2526	2018-08-20 20:06:04.431381	2	1	TEM	20.40
2529	2018-08-20 20:06:05.465928	2	2	HUM	29.70
2531	2018-08-20 20:06:06.482461	2	3	HUM_SOIL	0.00
2536	2018-08-20 20:06:13.793202	2	4	LUZ	0.00
2539	2018-08-20 20:06:14.826249	2	1	TEM	20.40
2542	2018-08-20 20:06:15.860244	2	2	HUM	29.60
2545	2018-08-20 20:06:16.894274	2	3	HUM_SOIL	0.00
2549	2018-08-20 20:06:24.165612	2	4	LUZ	0.00
2551	2018-08-20 20:06:25.188214	2	1	TEM	20.40
2554	2018-08-20 20:06:26.211278	2	2	HUM	29.60
2557	2018-08-20 20:06:27.24678	2	3	HUM_SOIL	0.00
2561	2018-08-20 20:06:34.517115	2	4	LUZ	0.00
2563	2018-08-20 20:06:35.526405	2	1	TEM	20.40
2567	2018-08-20 20:06:36.560605	2	2	HUM	29.60
2571	2018-08-20 20:06:37.582577	2	3	HUM_SOIL	0.00
2575	2018-08-20 20:06:44.886226	2	4	LUZ	0.00
2577	2018-08-20 20:06:45.909259	2	1	TEM	20.40
2580	2018-08-20 20:06:46.93752	2	2	HUM	29.60
2584	2018-08-20 20:06:47.950636	2	3	HUM_SOIL	0.00
2586	2018-08-20 20:06:55.25432	2	4	LUZ	0.00
2589	2018-08-20 20:06:56.272986	2	1	TEM	20.40
2592	2018-08-20 20:06:57.300767	2	2	HUM	29.60
2596	2018-08-20 20:06:58.336559	2	3	HUM_SOIL	0.00
2600	2018-08-20 20:07:05.622069	2	4	LUZ	0.00
2602	2018-08-20 20:07:06.629013	2	1	TEM	20.40
2605	2018-08-20 20:07:07.635928	2	2	HUM	30.20
2609	2018-08-20 20:07:08.664474	2	3	HUM_SOIL	0.00
2613	2018-08-20 20:07:16.001042	2	4	LUZ	0.00
2616	2018-08-20 20:07:17.023992	2	1	TEM	20.40
2619	2018-08-20 20:07:18.032015	2	2	HUM	30.00
2622	2018-08-20 20:07:19.065557	2	3	HUM_SOIL	0.00
2626	2018-08-20 20:07:26.363849	2	4	LUZ	0.00
2628	2018-08-20 20:07:27.370959	2	1	TEM	20.40
2631	2018-08-20 20:07:28.378878	2	2	HUM	29.80
2634	2018-08-20 20:07:29.386086	2	3	HUM_SOIL	0.00
2638	2018-08-20 20:07:36.7271	2	4	LUZ	0.00
2641	2018-08-20 20:07:37.745295	2	1	TEM	20.40
2645	2018-08-20 20:07:38.778856	2	2	HUM	29.80
2648	2018-08-20 20:07:39.806802	2	3	HUM_SOIL	0.00
3315	2018-08-23 00:48:20.193583	1	1	TEM	21.70
3316	2018-08-23 00:48:30.2332	1	2	HUM	43.20
3317	2018-08-23 00:48:40.273379	1	3	HUM_SOIL	1.00
3318	2018-08-23 00:48:50.313478	1	4	LUZ	0.00
3319	2018-08-23 00:49:00.358704	1	1	TEM	21.70
3320	2018-08-23 00:49:10.404415	1	2	HUM	43.20
3321	2018-08-23 00:49:20.449733	1	3	HUM_SOIL	99.00
2353	2018-08-20 20:03:48.627291	2	4	LUZ	0.00
2355	2018-08-20 20:03:49.634617	2	1	TEM	20.30
2359	2018-08-20 20:03:50.652961	2	2	HUM	30.00
2361	2018-08-20 20:03:51.66061	2	3	HUM_SOIL	0.00
2367	2018-08-20 20:03:59.01027	2	4	LUZ	0.00
2370	2018-08-20 20:04:00.043067	2	1	TEM	20.30
2373	2018-08-20 20:04:01.071677	2	2	HUM	29.80
2376	2018-08-20 20:04:02.10418	2	3	HUM_SOIL	0.00
2378	2018-08-20 20:04:09.374719	2	4	LUZ	0.00
2381	2018-08-20 20:04:10.383251	2	1	TEM	20.30
2384	2018-08-20 20:04:11.391189	2	2	HUM	29.70
2387	2018-08-20 20:04:12.399173	2	3	HUM_SOIL	0.00
2391	2018-08-20 20:04:19.729122	2	4	LUZ	0.00
2395	2018-08-20 20:04:20.748761	2	1	TEM	20.30
2399	2018-08-20 20:04:21.785504	2	2	HUM	29.60
2401	2018-08-20 20:04:22.789315	2	3	HUM_SOIL	0.00
2404	2018-08-20 20:04:30.097858	2	4	LUZ	0.00
2408	2018-08-20 20:04:31.111985	2	1	TEM	20.30
2412	2018-08-20 20:04:32.142119	2	2	HUM	29.60
2413	2018-08-20 20:04:33.142818	2	3	HUM_SOIL	0.00
2417	2018-08-20 20:04:40.462916	2	4	LUZ	0.00
2421	2018-08-20 20:04:41.470984	2	1	TEM	20.30
2423	2018-08-20 20:04:42.479162	2	2	HUM	29.60
2428	2018-08-20 20:04:43.502531	2	3	HUM_SOIL	0.00
2430	2018-08-20 20:04:50.840193	2	4	LUZ	0.00
2435	2018-08-20 20:04:51.874887	2	1	TEM	20.30
2438	2018-08-20 20:04:52.909654	2	2	HUM	29.70
2441	2018-08-20 20:04:53.941517	2	3	HUM_SOIL	0.00
2445	2018-08-20 20:05:01.220342	2	4	LUZ	0.00
2448	2018-08-20 20:05:02.243692	2	1	TEM	20.30
2451	2018-08-20 20:05:03.276615	2	2	HUM	29.70
2455	2018-08-20 20:05:04.305276	2	3	HUM_SOIL	0.00
2458	2018-08-20 20:05:11.578591	2	4	LUZ	0.00
2461	2018-08-20 20:05:12.612031	2	1	TEM	20.30
2464	2018-08-20 20:05:13.641518	2	2	HUM	29.60
2468	2018-08-20 20:05:14.675287	2	3	HUM_SOIL	0.00
2469	2018-08-20 20:05:21.939122	2	4	LUZ	0.00
2472	2018-08-20 20:05:22.946324	2	1	TEM	20.40
2475	2018-08-20 20:05:23.95486	2	2	HUM	29.60
2479	2018-08-20 20:05:24.961555	2	3	HUM_SOIL	0.00
2484	2018-08-20 20:05:32.355228	2	4	LUZ	0.00
2485	2018-08-20 20:05:33.361443	2	1	TEM	20.40
2489	2018-08-20 20:05:34.379618	2	2	HUM	29.60
2492	2018-08-20 20:05:35.401951	2	3	HUM_SOIL	0.00
2495	2018-08-20 20:05:42.676431	2	4	LUZ	0.00
2500	2018-08-20 20:05:43.709797	2	1	TEM	20.40
2503	2018-08-20 20:05:44.733096	2	2	HUM	29.70
2506	2018-08-20 20:05:45.766842	2	3	HUM_SOIL	0.00
2508	2018-08-20 20:05:53.046577	2	4	LUZ	0.00
2511	2018-08-20 20:05:54.063345	2	1	TEM	20.40
2514	2018-08-20 20:05:55.081663	2	2	HUM	29.70
2517	2018-08-20 20:05:56.08944	2	3	HUM_SOIL	0.00
2521	2018-08-20 20:06:03.413787	2	4	LUZ	0.00
2524	2018-08-20 20:06:04.421092	2	1	TEM	20.40
2527	2018-08-20 20:06:05.428396	2	2	HUM	29.70
2530	2018-08-20 20:06:06.435892	2	3	HUM_SOIL	0.00
2535	2018-08-20 20:06:13.785875	2	4	LUZ	0.00
2537	2018-08-20 20:06:14.803214	2	1	TEM	20.40
2540	2018-08-20 20:06:15.821135	2	2	HUM	29.60
2544	2018-08-20 20:06:16.855591	2	3	HUM_SOIL	0.00
2548	2018-08-20 20:06:24.15617	2	4	LUZ	0.00
2552	2018-08-20 20:06:25.189176	2	1	TEM	20.40
2555	2018-08-20 20:06:26.233539	2	2	HUM	29.60
2558	2018-08-20 20:06:27.262889	2	3	HUM_SOIL	0.00
2562	2018-08-20 20:06:34.533172	2	4	LUZ	0.00
2564	2018-08-20 20:06:35.540542	2	1	TEM	20.40
2566	2018-08-20 20:06:36.547391	2	2	HUM	29.60
2569	2018-08-20 20:06:37.55511	2	3	HUM_SOIL	0.00
2574	2018-08-20 20:06:44.885992	2	4	LUZ	0.00
2576	2018-08-20 20:06:45.893988	2	1	TEM	20.40
2579	2018-08-20 20:06:46.922108	2	2	HUM	29.60
2583	2018-08-20 20:06:47.950636	2	3	HUM_SOIL	0.00
2588	2018-08-20 20:06:55.260719	2	4	LUZ	0.00
2590	2018-08-20 20:06:56.28072	2	1	TEM	20.40
2593	2018-08-20 20:06:57.314705	2	2	HUM	29.60
2597	2018-08-20 20:06:58.33656	2	3	HUM_SOIL	0.00
2599	2018-08-20 20:07:05.622068	2	4	LUZ	0.00
2604	2018-08-20 20:07:06.641044	2	1	TEM	20.40
2607	2018-08-20 20:07:07.673788	2	2	HUM	30.20
2610	2018-08-20 20:07:08.690941	2	3	HUM_SOIL	0.00
2614	2018-08-20 20:07:16.007565	2	4	LUZ	0.00
2617	2018-08-20 20:07:17.039701	2	1	TEM	20.40
2620	2018-08-20 20:07:18.068407	2	2	HUM	30.00
2623	2018-08-20 20:07:19.101447	2	3	HUM_SOIL	0.00
2627	2018-08-20 20:07:26.382524	2	4	LUZ	0.00
2630	2018-08-20 20:07:27.406068	2	1	TEM	20.40
2633	2018-08-20 20:07:28.439988	2	2	HUM	29.80
2636	2018-08-20 20:07:29.463586	2	3	HUM_SOIL	0.00
2639	2018-08-20 20:07:36.728421	2	4	LUZ	0.00
2642	2018-08-20 20:07:37.745295	2	1	TEM	20.40
2644	2018-08-20 20:07:38.754028	2	2	HUM	29.80
2647	2018-08-20 20:07:39.761306	2	3	HUM_SOIL	0.00
3322	2018-08-23 01:09:29.859972	1	1	TEM	21.40
2354	2018-08-20 20:03:48.634651	2	4	LUZ	0.00
2357	2018-08-20 20:03:49.658226	2	1	TEM	20.30
2360	2018-08-20 20:03:50.681849	2	2	HUM	30.00
2363	2018-08-20 20:03:51.709838	2	3	HUM_SOIL	0.00
2365	2018-08-20 20:03:58.993092	2	4	LUZ	0.00
2369	2018-08-20 20:04:00.026593	2	1	TEM	20.30
2372	2018-08-20 20:04:01.055418	2	2	HUM	29.80
2375	2018-08-20 20:04:02.063341	2	3	HUM_SOIL	0.00
2380	2018-08-20 20:04:09.374719	2	4	LUZ	0.00
2382	2018-08-20 20:04:10.393989	2	1	TEM	20.30
2385	2018-08-20 20:04:11.417237	2	2	HUM	29.70
2388	2018-08-20 20:04:12.434709	2	3	HUM_SOIL	0.00
2393	2018-08-20 20:04:19.735584	2	4	LUZ	0.00
2396	2018-08-20 20:04:20.750083	2	1	TEM	20.30
2398	2018-08-20 20:04:21.766114	2	2	HUM	29.60
2400	2018-08-20 20:04:22.778336	2	3	HUM_SOIL	0.00
2405	2018-08-20 20:04:30.097858	2	4	LUZ	0.00
2407	2018-08-20 20:04:31.104774	2	1	TEM	20.30
2410	2018-08-20 20:04:32.127709	2	2	HUM	29.60
2414	2018-08-20 20:04:33.161374	2	3	HUM_SOIL	0.00
2418	2018-08-20 20:04:40.462916	2	4	LUZ	0.00
2422	2018-08-20 20:04:41.477614	2	1	TEM	20.30
2424	2018-08-20 20:04:42.48421	2	2	HUM	29.60
2426	2018-08-20 20:04:43.485746	2	3	HUM_SOIL	0.00
2431	2018-08-20 20:04:50.834815	2	4	LUZ	0.00
2433	2018-08-20 20:04:51.857702	2	1	TEM	20.30
2436	2018-08-20 20:04:52.875555	2	2	HUM	29.70
2439	2018-08-20 20:04:53.88325	2	3	HUM_SOIL	0.00
2443	2018-08-20 20:05:01.2019	2	4	LUZ	0.00
2446	2018-08-20 20:05:02.210251	2	1	TEM	20.30
2449	2018-08-20 20:05:03.231125	2	2	HUM	29.70
2453	2018-08-20 20:05:04.259955	2	3	HUM_SOIL	0.00
2456	2018-08-20 20:05:11.570507	2	4	LUZ	0.00
2459	2018-08-20 20:05:12.578208	2	1	TEM	20.30
2463	2018-08-20 20:05:13.612142	2	2	HUM	29.60
2467	2018-08-20 20:05:14.64547	2	3	HUM_SOIL	0.00
2471	2018-08-20 20:05:21.946556	2	4	LUZ	0.00
2474	2018-08-20 20:05:22.977133	2	1	TEM	20.40
2477	2018-08-20 20:05:24.015744	2	2	HUM	29.60
2481	2018-08-20 20:05:25.038401	2	3	HUM_SOIL	0.00
2483	2018-08-20 20:05:32.351985	2	4	LUZ	0.00
2487	2018-08-20 20:05:33.378406	2	1	TEM	20.40
2491	2018-08-20 20:05:34.399993	2	2	HUM	29.60
2493	2018-08-20 20:05:35.402847	2	3	HUM_SOIL	0.00
2496	2018-08-20 20:05:42.676026	2	4	LUZ	0.00
2498	2018-08-20 20:05:43.683305	2	1	TEM	20.40
2502	2018-08-20 20:05:44.707715	2	2	HUM	29.70
2505	2018-08-20 20:05:45.743218	2	3	HUM_SOIL	0.00
2510	2018-08-20 20:05:53.053642	2	4	LUZ	0.00
2512	2018-08-20 20:05:54.063345	2	1	TEM	20.40
2516	2018-08-20 20:05:55.097524	2	2	HUM	29.70
2518	2018-08-20 20:05:56.109186	2	3	HUM_SOIL	0.00
2523	2018-08-20 20:06:03.413664	2	4	LUZ	0.00
2525	2018-08-20 20:06:04.424182	2	1	TEM	20.40
2528	2018-08-20 20:06:05.451396	2	2	HUM	29.70
2532	2018-08-20 20:06:06.486605	2	3	HUM_SOIL	0.00
2534	2018-08-20 20:06:13.785875	2	4	LUZ	0.00
2538	2018-08-20 20:06:14.810338	2	1	TEM	20.40
2541	2018-08-20 20:06:15.835026	2	2	HUM	29.60
2543	2018-08-20 20:06:16.851562	2	3	HUM_SOIL	0.00
2547	2018-08-20 20:06:24.14968	2	4	LUZ	0.00
2550	2018-08-20 20:06:25.157429	2	1	TEM	20.40
2553	2018-08-20 20:06:26.193157	2	2	HUM	29.60
2556	2018-08-20 20:06:27.213328	2	3	HUM_SOIL	0.00
2560	2018-08-20 20:06:34.517115	2	4	LUZ	0.00
2565	2018-08-20 20:06:35.551534	2	1	TEM	20.40
2568	2018-08-20 20:06:36.560311	2	2	HUM	29.60
2570	2018-08-20 20:06:37.566423	2	3	HUM_SOIL	0.00
2573	2018-08-20 20:06:44.885992	2	4	LUZ	0.00
2578	2018-08-20 20:06:45.910526	2	1	TEM	20.40
2581	2018-08-20 20:06:46.937722	2	2	HUM	29.60
2582	2018-08-20 20:06:47.944591	2	3	HUM_SOIL	0.00
2587	2018-08-20 20:06:55.254343	2	4	LUZ	0.00
2591	2018-08-20 20:06:56.28802	2	1	TEM	20.40
2594	2018-08-20 20:06:57.321969	2	2	HUM	29.60
2595	2018-08-20 20:06:58.330095	2	3	HUM_SOIL	0.00
2601	2018-08-20 20:07:05.628681	2	4	LUZ	0.00
2603	2018-08-20 20:07:06.635002	2	1	TEM	20.40
2606	2018-08-20 20:07:07.6436	2	2	HUM	30.20
2608	2018-08-20 20:07:08.660768	2	3	HUM_SOIL	0.00
2612	2018-08-20 20:07:15.990412	2	4	LUZ	0.00
2615	2018-08-20 20:07:16.997991	2	1	TEM	20.40
2618	2018-08-20 20:07:18.010565	2	2	HUM	30.00
2621	2018-08-20 20:07:19.03955	2	3	HUM_SOIL	0.00
2625	2018-08-20 20:07:26.363905	2	4	LUZ	0.00
2629	2018-08-20 20:07:27.392116	2	1	TEM	20.40
2632	2018-08-20 20:07:28.405256	2	2	HUM	29.80
2635	2018-08-20 20:07:29.427978	2	3	HUM_SOIL	0.00
2640	2018-08-20 20:07:36.727099	2	4	LUZ	0.00
2643	2018-08-20 20:07:37.762235	2	1	TEM	20.40
2646	2018-08-20 20:07:38.796702	2	2	HUM	29.80
2649	2018-08-20 20:07:39.83007	2	3	HUM_SOIL	0.00
2364	2018-08-20 20:03:53.733793	1	3	HUM_SOIL	99.00
2377	2018-08-20 20:04:03.773638	1	4	LUZ	0.00
2390	2018-08-20 20:04:13.813647	1	1	TEM	20.50
2403	2018-08-20 20:04:23.854354	1	2	HUM	29.60
2416	2018-08-20 20:04:33.905563	1	3	HUM_SOIL	1.00
2429	2018-08-20 20:04:43.943754	1	4	LUZ	0.00
2442	2018-08-20 20:04:53.983886	1	1	TEM	20.50
2452	2018-08-20 20:05:04.033701	1	2	HUM	29.60
2465	2018-08-20 20:05:14.075412	1	3	HUM_SOIL	78.00
2478	2018-08-20 20:05:24.115437	1	4	LUZ	0.00
2488	2018-08-20 20:05:34.154015	1	4	LUZ	0.00
2507	2018-08-20 20:05:51.939603	1	1	TEM	20.50
2520	2018-08-20 20:06:01.987166	1	2	HUM	29.40
2533	2018-08-20 20:06:12.026986	1	3	HUM_SOIL	99.00
2546	2018-08-20 20:06:22.06462	1	4	LUZ	0.00
2559	2018-08-20 20:06:32.106128	1	1	TEM	20.50
2572	2018-08-20 20:06:42.144374	1	2	HUM	29.60
2585	2018-08-20 20:06:52.184473	1	3	HUM_SOIL	98.00
2598	2018-08-20 20:07:02.22552	1	4	LUZ	0.00
2611	2018-08-20 20:07:12.263832	1	1	TEM	20.50
2624	2018-08-20 20:07:22.303516	1	2	HUM	29.50
2637	2018-08-20 20:07:32.343547	1	3	HUM_SOIL	1.00
2650	2018-08-20 20:07:42.383844	1	4	LUZ	0.00
2652	2018-08-20 20:07:47.095522	2	4	LUZ	0.00
2651	2018-08-20 20:07:47.095522	2	4	LUZ	0.00
2654	2018-08-20 20:07:48.103009	2	1	TEM	20.40
2655	2018-08-20 20:07:48.103009	2	1	TEM	20.40
2657	2018-08-20 20:07:49.115098	2	2	HUM	29.80
2658	2018-08-20 20:07:49.147308	2	2	HUM	29.80
2660	2018-08-20 20:07:50.138218	2	3	HUM_SOIL	0.00
2661	2018-08-20 20:07:50.175217	2	3	HUM_SOIL	0.00
2665	2018-08-20 20:07:57.464244	2	4	LUZ	0.00
2666	2018-08-20 20:07:57.463948	2	4	LUZ	0.00
2668	2018-08-20 20:07:58.481467	2	1	TEM	20.40
2669	2018-08-20 20:07:58.488128	2	1	TEM	20.40
2670	2018-08-20 20:07:59.494583	2	2	HUM	29.70
2672	2018-08-20 20:07:59.522558	2	2	HUM	29.70
2673	2018-08-20 20:08:00.501831	2	3	HUM_SOIL	0.00
2674	2018-08-20 20:08:00.539874	2	3	HUM_SOIL	0.00
2677	2018-08-20 20:08:07.833586	2	4	LUZ	0.00
2678	2018-08-20 20:08:07.848354	2	4	LUZ	0.00
2680	2018-08-20 20:08:08.855591	2	1	TEM	20.40
2681	2018-08-20 20:08:08.866321	2	1	TEM	20.40
2683	2018-08-20 20:08:09.883548	2	2	HUM	29.70
2684	2018-08-20 20:08:09.883527	2	2	HUM	29.70
2686	2018-08-20 20:08:10.891223	2	3	HUM_SOIL	0.00
2687	2018-08-20 20:08:10.917336	2	3	HUM_SOIL	0.00
2690	2018-08-20 20:08:18.211629	2	4	LUZ	0.00
2692	2018-08-20 20:08:18.219635	2	4	LUZ	0.00
2694	2018-08-20 20:08:19.219256	2	1	TEM	20.40
2695	2018-08-20 20:08:19.249506	2	1	TEM	20.40
2697	2018-08-20 20:08:20.226724	2	2	HUM	29.70
2698	2018-08-20 20:08:20.273905	2	2	HUM	29.70
2700	2018-08-20 20:08:21.234045	2	3	HUM_SOIL	0.00
2701	2018-08-20 20:08:21.302677	2	3	HUM_SOIL	0.00
2704	2018-08-20 20:08:28.565819	2	4	LUZ	0.00
2705	2018-08-20 20:08:28.589171	2	4	LUZ	0.00
2707	2018-08-20 20:08:29.600737	2	1	TEM	20.40
2708	2018-08-20 20:08:29.627264	2	1	TEM	20.40
2710	2018-08-20 20:08:30.622506	2	2	HUM	29.70
2711	2018-08-20 20:08:30.662315	2	2	HUM	29.70
2713	2018-08-20 20:08:31.632646	2	3	HUM_SOIL	0.00
2714	2018-08-20 20:08:31.686303	2	3	HUM_SOIL	0.00
2717	2018-08-20 20:08:38.940059	2	4	LUZ	0.00
2718	2018-08-20 20:08:38.946763	2	4	LUZ	0.00
2720	2018-08-20 20:08:39.954656	2	1	TEM	20.40
2721	2018-08-20 20:08:39.971227	2	1	TEM	20.40
2722	2018-08-20 20:08:40.961865	2	2	HUM	29.70
2724	2018-08-20 20:08:40.983617	2	2	HUM	29.70
2725	2018-08-20 20:08:41.969196	2	3	HUM_SOIL	0.00
2726	2018-08-20 20:08:42.00195	2	3	HUM_SOIL	0.00
2729	2018-08-20 20:08:49.305826	2	4	LUZ	0.00
2730	2018-08-20 20:08:49.305826	2	4	LUZ	0.00
2732	2018-08-20 20:08:50.324184	2	1	TEM	20.40
2734	2018-08-20 20:08:50.339744	2	1	TEM	20.40
2736	2018-08-20 20:08:51.357612	2	2	HUM	29.70
2737	2018-08-20 20:08:51.373715	2	2	HUM	29.70
2738	2018-08-20 20:08:52.36494	2	3	HUM_SOIL	0.00
2740	2018-08-20 20:08:52.387794	2	3	HUM_SOIL	0.00
2742	2018-08-20 20:08:59.675319	2	4	LUZ	0.00
2744	2018-08-20 20:08:59.675063	2	4	LUZ	0.00
2745	2018-08-20 20:09:00.682575	2	1	TEM	20.40
2747	2018-08-20 20:09:00.708581	2	1	TEM	20.40
2748	2018-08-20 20:09:01.704424	2	2	HUM	29.60
2750	2018-08-20 20:09:01.727574	2	2	HUM	29.60
2752	2018-08-20 20:09:02.739449	2	3	HUM_SOIL	0.00
2753	2018-08-20 20:09:02.750624	2	3	HUM_SOIL	0.00
2755	2018-08-20 20:09:10.041931	2	4	LUZ	0.00
2756	2018-08-20 20:09:10.048369	2	4	LUZ	0.00
2759	2018-08-20 20:09:11.06881	2	1	TEM	20.40
2760	2018-08-20 20:09:11.08114	2	1	TEM	20.40
2762	2018-08-20 20:09:12.098101	2	2	HUM	29.70
2763	2018-08-20 20:09:12.114368	2	2	HUM	29.70
2767	2018-08-20 20:09:13.133417	2	3	HUM_SOIL	0.00
2766	2018-08-20 20:09:13.133417	2	3	HUM_SOIL	0.00
2769	2018-08-20 20:09:20.4172	2	4	LUZ	0.00
2770	2018-08-20 20:09:20.423317	2	4	LUZ	0.00
2772	2018-08-20 20:09:21.442726	2	1	TEM	20.50
2773	2018-08-20 20:09:21.442709	2	1	TEM	20.50
2775	2018-08-20 20:09:22.463349	2	2	HUM	29.60
2776	2018-08-20 20:09:22.478799	2	2	HUM	29.60
2779	2018-08-20 20:09:23.493132	2	3	HUM_SOIL	0.00
2780	2018-08-20 20:09:23.508256	2	3	HUM_SOIL	0.00
2781	2018-08-20 20:09:30.800891	2	4	LUZ	0.00
2782	2018-08-20 20:09:30.800891	2	4	LUZ	0.00
2784	2018-08-20 20:09:31.825387	2	1	TEM	20.50
2785	2018-08-20 20:09:31.83547	2	1	TEM	20.50
2787	2018-08-20 20:09:32.854152	2	2	HUM	29.60
2788	2018-08-20 20:09:32.870739	2	2	HUM	29.60
2791	2018-08-20 20:09:33.861673	2	3	HUM_SOIL	0.00
2792	2018-08-20 20:09:33.888067	2	3	HUM_SOIL	0.00
2794	2018-08-20 20:09:41.149859	2	4	LUZ	0.00
2795	2018-08-20 20:09:41.158091	2	4	LUZ	0.00
2797	2018-08-20 20:09:42.157557	2	1	TEM	20.50
2798	2018-08-20 20:09:42.16632	2	1	TEM	20.50
2801	2018-08-20 20:09:43.181105	2	2	HUM	29.50
2802	2018-08-20 20:09:43.189802	2	2	HUM	29.50
2804	2018-08-20 20:09:44.209211	2	3	HUM_SOIL	0.00
2805	2018-08-20 20:09:44.212159	2	3	HUM_SOIL	0.00
2808	2018-08-20 20:09:51.522491	2	4	LUZ	0.00
2809	2018-08-20 20:09:51.522881	2	4	LUZ	0.00
2811	2018-08-20 20:09:52.553835	2	1	TEM	20.50
2812	2018-08-20 20:09:52.556551	2	1	TEM	20.50
2815	2018-08-20 20:09:53.56688	2	2	HUM	29.50
2816	2018-08-20 20:09:53.56688	2	2	HUM	29.50
2817	2018-08-20 20:09:54.573346	2	3	HUM_SOIL	0.00
2818	2018-08-20 20:09:54.574647	2	3	HUM_SOIL	0.00
2821	2018-08-20 20:10:01.921546	2	4	LUZ	0.00
2822	2018-08-20 20:10:01.932959	2	4	LUZ	0.00
2823	2018-08-20 20:10:02.939023	2	1	TEM	20.50
2825	2018-08-20 20:10:02.962509	2	1	TEM	20.50
2827	2018-08-20 20:10:03.96168	2	2	HUM	29.50
2828	2018-08-20 20:10:03.969431	2	2	HUM	29.50
2832	2018-08-20 20:10:04.993346	2	3	HUM_SOIL	0.00
2831	2018-08-20 20:10:04.996016	2	3	HUM_SOIL	0.00
2833	2018-08-20 20:10:12.284207	2	4	LUZ	0.00
2835	2018-08-20 20:10:12.29287	2	4	LUZ	0.00
2838	2018-08-20 20:10:13.292682	2	1	TEM	20.50
2839	2018-08-20 20:10:13.316638	2	1	TEM	20.50
2841	2018-08-20 20:10:14.319435	2	2	HUM	29.50
2842	2018-08-20 20:10:14.338253	2	2	HUM	29.50
2843	2018-08-20 20:10:15.334754	2	3	HUM_SOIL	0.00
2845	2018-08-20 20:10:15.363814	2	3	HUM_SOIL	0.00
2653	2018-08-20 20:07:47.10257	2	4	LUZ	0.00
2656	2018-08-20 20:07:48.137295	2	1	TEM	20.40
2659	2018-08-20 20:07:49.167132	2	2	HUM	29.80
2662	2018-08-20 20:07:50.196656	2	3	HUM_SOIL	0.00
2664	2018-08-20 20:07:57.464315	2	4	LUZ	0.00
2667	2018-08-20 20:07:58.473445	2	1	TEM	20.40
2671	2018-08-20 20:07:59.506536	2	2	HUM	29.70
2675	2018-08-20 20:08:00.540836	2	3	HUM_SOIL	0.00
2679	2018-08-20 20:08:07.850253	2	4	LUZ	0.00
2682	2018-08-20 20:08:08.873306	2	1	TEM	20.40
2685	2018-08-20 20:08:09.899811	2	2	HUM	29.70
2688	2018-08-20 20:08:10.932747	2	3	HUM_SOIL	0.00
2691	2018-08-20 20:08:18.211629	2	4	LUZ	0.00
2693	2018-08-20 20:08:19.219256	2	1	TEM	20.40
2696	2018-08-20 20:08:20.226724	2	2	HUM	29.70
2699	2018-08-20 20:08:21.234045	2	3	HUM_SOIL	0.00
2703	2018-08-20 20:08:28.565819	2	4	LUZ	0.00
2706	2018-08-20 20:08:29.583505	2	1	TEM	20.40
2709	2018-08-20 20:08:30.591613	2	2	HUM	29.70
2712	2018-08-20 20:08:31.614299	2	3	HUM_SOIL	0.00
2716	2018-08-20 20:08:38.936481	2	4	LUZ	0.00
2719	2018-08-20 20:08:39.946133	2	1	TEM	20.40
2723	2018-08-20 20:08:40.968758	2	2	HUM	29.70
2727	2018-08-20 20:08:42.003715	2	3	HUM_SOIL	0.00
2731	2018-08-20 20:08:49.31264	2	4	LUZ	0.00
2733	2018-08-20 20:08:50.331069	2	1	TEM	20.40
2735	2018-08-20 20:08:51.349281	2	2	HUM	29.70
2739	2018-08-20 20:08:52.378119	2	3	HUM_SOIL	0.00
2743	2018-08-20 20:08:59.675063	2	4	LUZ	0.00
2746	2018-08-20 20:09:00.689117	2	1	TEM	20.40
2749	2018-08-20 20:09:01.712311	2	2	HUM	29.60
2751	2018-08-20 20:09:02.719253	2	3	HUM_SOIL	0.00
2757	2018-08-20 20:09:10.044444	2	4	LUZ	0.00
2758	2018-08-20 20:09:11.061902	2	1	TEM	20.40
2761	2018-08-20 20:09:12.085593	2	2	HUM	29.70
2765	2018-08-20 20:09:13.108637	2	3	HUM_SOIL	0.00
2768	2018-08-20 20:09:20.409886	2	4	LUZ	0.00
2771	2018-08-20 20:09:21.4175	2	1	TEM	20.50
2774	2018-08-20 20:09:22.425014	2	2	HUM	29.60
2778	2018-08-20 20:09:23.452772	2	3	HUM_SOIL	0.00
2783	2018-08-20 20:09:30.818066	2	4	LUZ	0.00
2786	2018-08-20 20:09:31.85194	2	1	TEM	20.50
2789	2018-08-20 20:09:32.896835	2	2	HUM	29.60
2793	2018-08-20 20:09:33.917296	2	3	HUM_SOIL	0.00
2796	2018-08-20 20:09:41.160016	2	4	LUZ	0.00
2799	2018-08-20 20:09:42.179487	2	1	TEM	20.50
2803	2018-08-20 20:09:43.205962	2	2	HUM	29.50
2806	2018-08-20 20:09:44.212159	2	3	HUM_SOIL	0.00
2807	2018-08-20 20:09:51.515187	2	4	LUZ	0.00
2810	2018-08-20 20:09:52.522615	2	1	TEM	20.50
2814	2018-08-20 20:09:53.545465	2	2	HUM	29.50
2819	2018-08-20 20:09:54.574647	2	3	HUM_SOIL	0.00
2820	2018-08-20 20:10:01.915811	2	4	LUZ	0.00
2824	2018-08-20 20:10:02.945528	2	1	TEM	20.50
2829	2018-08-20 20:10:03.969431	2	2	HUM	29.50
2830	2018-08-20 20:10:04.987945	2	3	HUM_SOIL	0.00
2834	2018-08-20 20:10:12.285427	2	4	LUZ	0.00
2837	2018-08-20 20:10:13.292463	2	1	TEM	20.50
2840	2018-08-20 20:10:14.307999	2	2	HUM	29.50
2844	2018-08-20 20:10:15.341272	2	3	HUM_SOIL	0.00
2846	2018-08-20 20:10:22.622526	2	4	LUZ	0.00
2850	2018-08-20 20:10:23.641056	2	1	TEM	20.40
2853	2018-08-20 20:10:24.665237	2	2	HUM	29.40
2857	2018-08-20 20:10:25.692864	2	3	HUM_SOIL	0.00
2861	2018-08-20 20:10:33.011789	2	4	LUZ	0.00
2865	2018-08-20 20:10:34.048394	2	1	TEM	20.50
2867	2018-08-20 20:10:35.061339	2	2	HUM	29.40
2870	2018-08-20 20:10:36.078342	2	3	HUM_SOIL	0.00
2875	2018-08-20 20:10:43.36746	2	4	LUZ	0.00
2878	2018-08-20 20:10:44.401702	2	1	TEM	20.40
2881	2018-08-20 20:10:45.425616	2	2	HUM	29.40
2884	2018-08-20 20:10:46.450319	2	3	HUM_SOIL	0.00
2888	2018-08-20 20:10:53.744663	2	4	LUZ	0.00
2891	2018-08-20 20:10:54.777571	2	1	TEM	20.40
2893	2018-08-20 20:10:55.79693	2	2	HUM	29.40
2897	2018-08-20 20:10:56.835633	2	3	HUM_SOIL	0.00
2901	2018-08-20 20:11:04.11944	2	4	LUZ	0.00
2904	2018-08-20 20:11:05.152012	2	1	TEM	20.40
2907	2018-08-20 20:11:06.176585	2	2	HUM	29.40
2910	2018-08-20 20:11:07.203861	2	3	HUM_SOIL	0.00
2912	2018-08-20 20:11:14.462562	2	4	LUZ	0.00
2915	2018-08-20 20:11:15.486505	2	1	TEM	20.40
2919	2018-08-20 20:11:16.500449	2	2	HUM	29.40
2921	2018-08-20 20:11:17.519043	2	3	HUM_SOIL	0.00
2927	2018-08-20 20:11:24.831585	2	4	LUZ	0.00
2929	2018-08-20 20:11:25.839332	2	1	TEM	20.40
2932	2018-08-20 20:11:26.848336	2	2	HUM	29.40
2934	2018-08-20 20:11:27.855278	2	3	HUM_SOIL	0.00
2940	2018-08-20 20:11:35.200953	2	4	LUZ	0.00
2941	2018-08-20 20:11:36.223705	2	1	TEM	20.40
2945	2018-08-20 20:11:37.258953	2	2	HUM	29.40
2947	2018-08-20 20:11:38.276294	2	3	HUM_SOIL	0.00
2952	2018-08-20 20:11:45.566602	2	4	LUZ	0.00
2954	2018-08-20 20:11:46.573923	2	1	TEM	20.40
2957	2018-08-20 20:11:47.591485	2	2	HUM	29.40
2961	2018-08-20 20:11:48.624824	2	3	HUM_SOIL	0.00
2964	2018-08-20 20:11:55.935595	2	4	LUZ	0.00
2969	2018-08-20 20:11:56.969486	2	1	TEM	20.40
2972	2018-08-20 20:11:57.994436	2	2	HUM	29.40
2663	2018-08-20 20:07:52.424786	1	1	TEM	20.50
2676	2018-08-20 20:08:02.46356	1	2	HUM	29.50
2689	2018-08-20 20:08:12.50379	1	3	HUM_SOIL	1.00
2702	2018-08-20 20:08:22.549211	1	4	LUZ	0.00
2715	2018-08-20 20:08:32.588576	1	1	TEM	20.50
2728	2018-08-20 20:08:42.623818	1	2	HUM	29.50
2741	2018-08-20 20:08:52.664475	1	3	HUM_SOIL	94.00
2754	2018-08-20 20:09:02.892085	1	4	LUZ	0.00
2764	2018-08-20 20:09:12.937709	1	1	TEM	20.50
2777	2018-08-20 20:09:22.981025	1	2	HUM	29.50
2790	2018-08-20 20:09:33.028738	1	3	HUM_SOIL	99.00
2800	2018-08-20 20:09:43.077282	1	4	LUZ	0.00
2813	2018-08-20 20:09:53.119986	1	1	TEM	20.50
2826	2018-08-20 20:10:03.163766	1	2	HUM	29.50
2836	2018-08-20 20:10:13.203959	1	3	HUM_SOIL	1.00
2849	2018-08-20 20:10:23.243956	1	4	LUZ	0.00
2862	2018-08-20 20:10:33.284821	1	1	TEM	20.50
2872	2018-08-20 20:10:43.330281	1	2	HUM	29.60
2885	2018-08-20 20:10:53.379003	1	3	HUM_SOIL	1.00
2898	2018-08-20 20:11:03.434392	1	4	LUZ	0.00
2911	2018-08-20 20:11:13.475166	1	1	TEM	20.50
2924	2018-08-20 20:11:23.517551	1	2	HUM	29.60
2937	2018-08-20 20:11:33.553475	1	3	HUM_SOIL	97.00
2950	2018-08-20 20:11:43.595613	1	4	LUZ	0.00
2963	2018-08-20 20:11:53.63414	1	1	TEM	20.50
2847	2018-08-20 20:10:22.622526	2	4	LUZ	0.00
2851	2018-08-20 20:10:23.656629	2	1	TEM	20.40
2854	2018-08-20 20:10:24.66733	2	2	HUM	29.40
2856	2018-08-20 20:10:25.689114	2	3	HUM_SOIL	0.00
2859	2018-08-20 20:10:32.993253	2	4	LUZ	0.00
2863	2018-08-20 20:10:34.016773	2	1	TEM	20.50
2866	2018-08-20 20:10:35.04547	2	2	HUM	29.40
2869	2018-08-20 20:10:36.078342	2	3	HUM_SOIL	0.00
2874	2018-08-20 20:10:43.356616	2	4	LUZ	0.00
2876	2018-08-20 20:10:44.376013	2	1	TEM	20.40
2879	2018-08-20 20:10:45.399046	2	2	HUM	29.40
2882	2018-08-20 20:10:46.422095	2	3	HUM_SOIL	0.00
2887	2018-08-20 20:10:53.732784	2	4	LUZ	0.00
2890	2018-08-20 20:10:54.766503	2	1	TEM	20.40
2894	2018-08-20 20:10:55.803841	2	2	HUM	29.40
2896	2018-08-20 20:10:56.828197	2	3	HUM_SOIL	0.00
2900	2018-08-20 20:11:04.112354	2	4	LUZ	0.00
2902	2018-08-20 20:11:05.119837	2	1	TEM	20.40
2905	2018-08-20 20:11:06.127358	2	2	HUM	29.40
2908	2018-08-20 20:11:07.155397	2	3	HUM_SOIL	0.00
2913	2018-08-20 20:11:14.469349	2	4	LUZ	0.00
2916	2018-08-20 20:11:15.486505	2	1	TEM	20.40
2918	2018-08-20 20:11:16.493641	2	2	HUM	29.40
2922	2018-08-20 20:11:17.522508	2	3	HUM_SOIL	0.00
2926	2018-08-20 20:11:24.831585	2	4	LUZ	0.00
2930	2018-08-20 20:11:25.840655	2	1	TEM	20.40
2933	2018-08-20 20:11:26.851667	2	2	HUM	29.40
2936	2018-08-20 20:11:27.867433	2	3	HUM_SOIL	0.00
2938	2018-08-20 20:11:35.20118	2	4	LUZ	0.00
2942	2018-08-20 20:11:36.22502	2	1	TEM	20.40
2944	2018-08-20 20:11:37.243185	2	2	HUM	29.40
2949	2018-08-20 20:11:38.279424	2	3	HUM_SOIL	0.00
2951	2018-08-20 20:11:45.567911	2	4	LUZ	0.00
2955	2018-08-20 20:11:46.575448	2	1	TEM	20.40
2958	2018-08-20 20:11:47.60404	2	2	HUM	29.40
2960	2018-08-20 20:11:48.621344	2	3	HUM_SOIL	0.00
2965	2018-08-20 20:11:55.935595	2	4	LUZ	0.00
2967	2018-08-20 20:11:56.954247	2	1	TEM	20.40
2971	2018-08-20 20:11:57.988183	2	2	HUM	29.40
2848	2018-08-20 20:10:22.638386	2	4	LUZ	0.00
2852	2018-08-20 20:10:23.65799	2	1	TEM	20.40
2855	2018-08-20 20:10:24.69038	2	2	HUM	29.40
2858	2018-08-20 20:10:25.710511	2	3	HUM_SOIL	0.00
2860	2018-08-20 20:10:32.993129	2	4	LUZ	0.00
2864	2018-08-20 20:10:34.032084	2	1	TEM	20.50
2868	2018-08-20 20:10:35.077455	2	2	HUM	29.40
2871	2018-08-20 20:10:36.111739	2	3	HUM_SOIL	0.00
2873	2018-08-20 20:10:43.356334	2	4	LUZ	0.00
2877	2018-08-20 20:10:44.385734	2	1	TEM	20.40
2880	2018-08-20 20:10:45.420725	2	2	HUM	29.40
2883	2018-08-20 20:10:46.450319	2	3	HUM_SOIL	0.00
2886	2018-08-20 20:10:53.726824	2	4	LUZ	0.00
2889	2018-08-20 20:10:54.756327	2	1	TEM	20.40
2892	2018-08-20 20:10:55.780402	2	2	HUM	29.40
2895	2018-08-20 20:10:56.817736	2	3	HUM_SOIL	0.00
2899	2018-08-20 20:11:04.112354	2	4	LUZ	0.00
2903	2018-08-20 20:11:05.126625	2	1	TEM	20.40
2906	2018-08-20 20:11:06.156701	2	2	HUM	29.40
2909	2018-08-20 20:11:07.18925	2	3	HUM_SOIL	0.00
2914	2018-08-20 20:11:14.479058	2	4	LUZ	0.00
2917	2018-08-20 20:11:15.51376	2	1	TEM	20.40
2920	2018-08-20 20:11:16.538164	2	2	HUM	29.40
2923	2018-08-20 20:11:17.566998	2	3	HUM_SOIL	0.00
2925	2018-08-20 20:11:24.831585	2	4	LUZ	0.00
2928	2018-08-20 20:11:25.839331	2	1	TEM	20.40
2931	2018-08-20 20:11:26.846865	2	2	HUM	29.40
2935	2018-08-20 20:11:27.859949	2	3	HUM_SOIL	0.00
2939	2018-08-20 20:11:35.200953	2	4	LUZ	0.00
2943	2018-08-20 20:11:36.231592	2	1	TEM	20.40
2946	2018-08-20 20:11:37.260302	2	2	HUM	29.40
2948	2018-08-20 20:11:38.284744	2	3	HUM_SOIL	0.00
2953	2018-08-20 20:11:45.574462	2	4	LUZ	0.00
2956	2018-08-20 20:11:46.60859	2	1	TEM	20.40
2959	2018-08-20 20:11:47.637232	2	2	HUM	29.40
2962	2018-08-20 20:11:48.670529	2	3	HUM_SOIL	0.00
2966	2018-08-20 20:11:55.951407	2	4	LUZ	0.00
2968	2018-08-20 20:11:56.969465	2	1	TEM	20.40
2970	2018-08-20 20:11:57.987084	2	2	HUM	29.40
2973	2018-08-21 04:30:23.108914	1	1	TEM	20.70
2977	2018-08-21 04:30:33.177514	1	2	HUM	29.80
2982	2018-08-21 04:30:43.224514	1	3	HUM_SOIL	98.00
2987	2018-08-21 04:30:53.264102	1	4	LUZ	0.00
2992	2018-08-21 04:31:03.307617	1	1	TEM	20.70
2997	2018-08-21 04:31:13.347881	1	2	HUM	29.70
3001	2018-08-21 04:31:23.383319	1	3	HUM_SOIL	29.00
3006	2018-08-21 04:31:33.429831	1	4	LUZ	0.00
3011	2018-08-21 04:31:43.479118	1	1	TEM	20.80
3015	2018-08-21 04:31:53.521956	1	2	HUM	29.80
3020	2018-08-21 04:32:03.566545	1	3	HUM_SOIL	86.00
3025	2018-08-21 04:32:13.612252	1	4	LUZ	0.00
3029	2018-08-21 04:32:23.660425	1	1	TEM	20.70
3034	2018-08-21 04:32:33.70717	1	2	HUM	29.80
3097	2018-08-22 23:45:43.079984	1	1	TEM	21.60
3098	2018-08-22 23:45:53.165633	1	2	HUM	42.90
3099	2018-08-22 23:46:03.20518	1	3	HUM_SOIL	1.00
3100	2018-08-22 23:46:13.243692	1	4	LUZ	0.00
1633	2018-08-20 19:54:10.041807	2	2	HUM	29.80
1634	2018-08-20 19:54:11.046603	2	3	HUM_SOIL	0.00
1638	2018-08-20 19:54:18.37221	2	4	LUZ	0.00
1642	2018-08-20 19:54:19.40162	2	1	TEM	19.90
1644	2018-08-20 19:54:20.419248	2	2	HUM	29.70
1647	2018-08-20 19:54:21.451556	2	3	HUM_SOIL	0.00
1651	2018-08-20 19:54:28.739755	2	4	LUZ	0.00
1655	2018-08-20 19:54:29.773898	2	1	TEM	19.90
1658	2018-08-20 19:54:30.783502	2	2	HUM	29.70
1662	2018-08-20 19:54:31.809951	2	3	HUM_SOIL	0.00
1664	2018-08-20 19:54:39.100314	2	4	LUZ	0.00
1668	2018-08-20 19:54:40.123674	2	1	TEM	19.90
1671	2018-08-20 19:54:41.157068	2	2	HUM	29.70
1675	2018-08-20 19:54:42.189931	2	3	HUM_SOIL	0.00
1677	2018-08-20 19:54:49.476232	2	4	LUZ	0.00
1680	2018-08-20 19:54:50.495495	2	1	TEM	19.90
1684	2018-08-20 19:54:51.529076	2	2	HUM	29.70
1687	2018-08-20 19:54:52.552888	2	3	HUM_SOIL	0.00
1692	2018-08-20 19:54:59.854003	2	4	LUZ	0.00
1693	2018-08-20 19:55:00.861673	2	1	TEM	19.90
1697	2018-08-20 19:55:01.890873	2	2	HUM	29.70
1701	2018-08-20 19:55:02.924221	2	3	HUM_SOIL	0.00
1705	2018-08-20 19:55:10.214449	2	4	LUZ	0.00
1707	2018-08-20 19:55:11.230259	2	1	TEM	19.90
1711	2018-08-20 19:55:12.253341	2	2	HUM	29.80
1714	2018-08-20 19:55:13.286916	2	3	HUM_SOIL	0.00
1717	2018-08-20 19:55:20.574523	2	4	LUZ	0.00
1719	2018-08-20 19:55:21.581614	2	1	TEM	19.90
1722	2018-08-20 19:55:22.589387	2	2	HUM	29.90
1726	2018-08-20 19:55:23.618423	2	3	HUM_SOIL	0.00
1729	2018-08-20 19:55:30.943023	2	4	LUZ	0.00
1732	2018-08-20 19:55:31.950143	2	1	TEM	19.90
1735	2018-08-20 19:55:32.957645	2	2	HUM	30.10
1738	2018-08-20 19:55:33.985873	2	3	HUM_SOIL	0.00
1743	2018-08-20 19:55:41.311619	2	4	LUZ	0.00
1747	2018-08-20 19:55:42.346498	2	1	TEM	19.90
1749	2018-08-20 19:55:43.355153	2	2	HUM	30.10
\.


--
-- Name: measure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('measure_id_seq', 3322, true);


--
-- Data for Name: plant; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY plant (id, name, date_created, varieti_id) FROM stdin;
\.


--
-- Name: plant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('plant_id_seq', 1, false);


--
-- Data for Name: recipe; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY recipe (id, name, date_created, info_recipe) FROM stdin;
\.


--
-- Name: recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('recipe_id_seq', 1, false);


--
-- Data for Name: specie; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY specie (id, name, date_created, especie_id, especie) FROM stdin;
\.


--
-- Name: specie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('specie_id_seq', 1, false);


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY "user" (id, name, date_created, mail, password) FROM stdin;
\.


--
-- Data for Name: user_cultivo; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY user_cultivo (id, name, date_created, role, last_conexion, cultivo_id, user_id) FROM stdin;
\.


--
-- Name: user_cultivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('user_cultivo_id_seq', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('user_id_seq', 1, false);


--
-- Data for Name: varieti; Type: TABLE DATA; Schema: public; Owner: pi
--

COPY varieti (id, name, date_created, genotipo, cruce, production, specie_id) FROM stdin;
\.


--
-- Name: varieti_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pi
--

SELECT pg_catalog.setval('varieti_id_seq', 1, false);


--
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id);


--
-- Name: book book_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);


--
-- Name: ciclo_vegetativo ciclo_vegetativo_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY ciclo_vegetativo
    ADD CONSTRAINT ciclo_vegetativo_pkey PRIMARY KEY (id);


--
-- Name: component_cultivo component_cultivo_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component_cultivo
    ADD CONSTRAINT component_cultivo_pkey PRIMARY KEY (id);


--
-- Name: component component_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component
    ADD CONSTRAINT component_pkey PRIMARY KEY (id);


--
-- Name: cultivo cultivo_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY cultivo
    ADD CONSTRAINT cultivo_pkey PRIMARY KEY (id);


--
-- Name: detaile_cultivo detaile_cultivo_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_cultivo
    ADD CONSTRAINT detaile_cultivo_pkey PRIMARY KEY (id);


--
-- Name: detaile_device detaile_device_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_device
    ADD CONSTRAINT detaile_device_pkey PRIMARY KEY (id);


--
-- Name: device device_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY device
    ADD CONSTRAINT device_pkey PRIMARY KEY (id);


--
-- Name: huerta huerta_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY huerta
    ADD CONSTRAINT huerta_pkey PRIMARY KEY (id, hight, lenght);


--
-- Name: measure measure_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY measure
    ADD CONSTRAINT measure_pkey PRIMARY KEY (id);


--
-- Name: plant plant_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY plant
    ADD CONSTRAINT plant_pkey PRIMARY KEY (id);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (id);


--
-- Name: specie specie_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY specie
    ADD CONSTRAINT specie_pkey PRIMARY KEY (id);


--
-- Name: user_cultivo user_cultivo_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY user_cultivo
    ADD CONSTRAINT user_cultivo_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: varieti varieti_pkey; Type: CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY varieti
    ADD CONSTRAINT varieti_pkey PRIMARY KEY (id);


--
-- Name: action action_component_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_component_id_fkey FOREIGN KEY (component_id) REFERENCES component(id);


--
-- Name: action action_cultivo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY action
    ADD CONSTRAINT action_cultivo_id_fkey FOREIGN KEY (cultivo_id) REFERENCES cultivo(id);


--
-- Name: book book_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY book
    ADD CONSTRAINT book_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES recipe(id);


--
-- Name: ciclo_vegetativo ciclo_vegetativo_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY ciclo_vegetativo
    ADD CONSTRAINT ciclo_vegetativo_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plant(id);


--
-- Name: component_cultivo component_cultivo_component_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component_cultivo
    ADD CONSTRAINT component_cultivo_component_id_fkey FOREIGN KEY (component_id) REFERENCES component(id);


--
-- Name: component_cultivo component_cultivo_cultivo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY component_cultivo
    ADD CONSTRAINT component_cultivo_cultivo_id_fkey FOREIGN KEY (cultivo_id) REFERENCES cultivo(id);


--
-- Name: detaile_cultivo detaile_cultivo_cultivo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_cultivo
    ADD CONSTRAINT detaile_cultivo_cultivo_id_fkey FOREIGN KEY (cultivo_id) REFERENCES cultivo(id);


--
-- Name: detaile_cultivo detaile_cultivo_plant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_cultivo
    ADD CONSTRAINT detaile_cultivo_plant_id_fkey FOREIGN KEY (plant_id) REFERENCES plant(id);


--
-- Name: detaile_device detaile_device_component_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_device
    ADD CONSTRAINT detaile_device_component_id_fkey FOREIGN KEY (component_id) REFERENCES component(id);


--
-- Name: detaile_device detaile_device_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY detaile_device
    ADD CONSTRAINT detaile_device_device_id_fkey FOREIGN KEY (device_id) REFERENCES device(id);


--
-- Name: measure measure_component_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY measure
    ADD CONSTRAINT measure_component_id_fkey FOREIGN KEY (component_id) REFERENCES component(id);


--
-- Name: measure measure_cultivo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY measure
    ADD CONSTRAINT measure_cultivo_id_fkey FOREIGN KEY (cultivo_id) REFERENCES cultivo(id);


--
-- Name: plant plant_varieti_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY plant
    ADD CONSTRAINT plant_varieti_id_fkey FOREIGN KEY (varieti_id) REFERENCES varieti(id);


--
-- Name: user_cultivo user_cultivo_cultivo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY user_cultivo
    ADD CONSTRAINT user_cultivo_cultivo_id_fkey FOREIGN KEY (cultivo_id) REFERENCES cultivo(id);


--
-- Name: user_cultivo user_cultivo_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY user_cultivo
    ADD CONSTRAINT user_cultivo_user_id_fkey FOREIGN KEY (user_id) REFERENCES "user"(id);


--
-- Name: varieti varieti_specie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pi
--

ALTER TABLE ONLY varieti
    ADD CONSTRAINT varieti_specie_id_fkey FOREIGN KEY (specie_id) REFERENCES specie(id);


--
-- PostgreSQL database dump complete
--

