# Sensores y actuadores

En esta sección se puede encontrar dos versiones. La versión 1.0 que corresponde
a la versión serial (Arduino uno) y la version 2.0 que corresponde a la version
mqtt (NodeMCU). 

## Configurar red y usuario en nodemcu

Creamos en la carpeta v2.0, junto al archivo v2.0.ino; un archivo llamado config.h
Este archivo contiene las configuraciones de red

```
const char* ssid = "-- nombre de la wifi-- "; // Ej: "mi_wifi"
const char* password = "-- password -- "; // pass "mi_wifi123"
const char* mqtt_server = "-- broker -- "; //ip del broker o host
const char* mqtt_user = "cultivo1";
const char* mqtt_password = "cultivo1";

```