#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Arduino.h>
#include "config.h"

// ---- init configuraciones de componentes
#include <DHT.h>

struct dht_med{
  float tem;
  float hum;
};

//  --- fin configuraciones

int TIME_DELAY=1000;

WiFiClient espClient;
PubSubClient client(espClient);

int RELE_PIN[4] = {14, 12, 13, 15};

void setup() {
  Serial.begin(9600);
  setup_wifi();
  dht_setup();
  foto_celda_setup();
  red_setup();
  for (int RELE=0; RELE<4; RELE++){
    pinMode(RELE_PIN[RELE], OUTPUT);    
  }
}


void loop() {
  red_loop();
  dht_loop();
  yl69_loop();
  foto_celda_loop();
  delay(TIME_DELAY);
}



  
