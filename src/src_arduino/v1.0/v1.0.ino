  /* 
 * HuerTechno v1.0
 * Prototipo serial
 * Fecha de actualización: 26-mar-2018
 * 
 * Este script recolecta las mediciones de los sensores instalados en el cultivo
 * Las mediciones son transmitidas mediante cable serial (USB) a la Raspberry
 * También se pueden enviar instrucciones desde la raspberry hasta el arduino
 * tales como manipular luz y agua.
 * 
 * Licencia Software Libre 
 *
 */

#include <Arduino.h>
#include <DHT.h>

#define DHTTYPE DHT22 
int RELE_PIN[8] = {4, 5, 6, 7, 8, 9, 10, 11};


int FOTOPIN = 3;
int DHTPIN = 2; //DHT
int HUM_SOIL_PIN = A0; // yl-69
int LUZ_PIN = A1; // celda de luz
//int RIEGO_PIN = 4; //rele bomba de riego
//int LUCES_PIN = 5; //rele luz
//int FAN_PIN = 6; //ventilador 1
int ACT_PIN;
/*
*Definir variables del sistema
*/
String accion;
String nameAct="0";
String inAccion;
int state;

int TIME_DELAY = 10000;

DHT dht(DHTPIN, DHTTYPE); 

struct dht_med{
  float tem;
  float hum;
};


void setup()
{
  Serial.begin(9600);
  for (int RELE=0; RELE<9; RELE++){
    pinMode(RELE_PIN[RELE], OUTPUT);    
  }
  pinMode(DHTPIN, INPUT); 
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  /*
   * Escucha mediante conexión serial, las ordenes deben ir 
   * sin salto de linea ni retorno de carro "\r\n"
   */
   inAccion = ""; 
   while (Serial.available() > 0) {
    int inChar = Serial.read();
    inAccion += char(inChar); 
   }
   String topic = inAccion.substring(0, 3);
   if (topic == "act"){
    actuador(inAccion);
   }
   dht_sensor();
   yl69_sensor();
   foto_celda_sensor();
   delay(TIME_DELAY);
}



int actuador(String accion){
    nameAct = accion.substring(0, 3);
    ACT_PIN = RELE_PIN[accion.substring(4, 5).toInt()];
    //Serial.println(ACT_PIN);
    state = accion.substring(6, 7).toInt();
    digitalWrite(ACT_PIN, state);
    return 0; 
 }




int dht_sensor(){
  float hum_med = dht.readHumidity();
  float tem_med = dht.readTemperature();
  if (isnan(hum_med) || isnan(tem_med)){
    Serial.println(0);
  }else {
    Serial.print("CULTIVO_1/SENSOR/DHT22/TEM:");
    Serial.println(tem_med);
    delay(500);
    Serial.print("CULTIVO_1/SENSOR/DHT22/HUM:");
    Serial.println(hum_med);
  }
  delay(500);
  return 0;
}




int foto_celda_sensor(){
  
  float foto_celda = digitalRead(FOTOPIN);
  if (isnan(foto_celda)){
    Serial.println(0);
  } else {
    Serial.print("CULTIVO_1/SENSOR/FOTO_CELDA/LUM:");
    Serial.println(foto_celda);
  }
  delay(500);
  return 0;
}




int yl69_sensor(){
  /* se debe calibrar el sensor con un vaso de agua o con
   *  tierra mojada. Primero sin el map (para ver los valores 
   *  analogicos y luego esos minimos y maximos poner en map
   */
  float hum_soil = map(analogRead(HUM_SOIL_PIN), 200, 690, 99, 0);
  if (isnan(hum_soil)){
    Serial.println(0);
  } else {
   Serial.print("CULTIVO_1/SENSOR/YL69/HUM_SOIL:");
   Serial.println(hum_soil);
  }
  delay(500);
  return 0;
}



  


 
