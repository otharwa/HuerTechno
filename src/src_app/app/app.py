from flask import Flask, render_template, jsonify
from flask_sqlalchemy import SQLAlchemy
from app.models import db
from app.views import blueprints


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://pi:pi123@localhost/huertechno'


def create_app():

    for bp in  blueprints:
        app.register_blueprint(bp)


    db.init_app(app)
    db.create_all(app=app)

    return app
