from serial import Serial
import os



class Controller(object):

    ports = []

    def __init__(self, ports):
        self.ports = ports
        self.arduino = Serial()
        self.instanciar()

    def instanciar(self):
        ports = self.ports
        if os.path.exists(ports[0]):
            fname = ports[0]
        else:
            fname = ports[1]

        try :
            self.arduino.port = fname
            self.arduino.open()
            self.on = True
            return True
        except:
            self.on = False
            print("Necesita conectar " + fname + " antes de seguir")
            return False




class Actuador(object):

    nameAct = ""
    pin = 0
    TYPE_ACCION_ON = 0
    TYPE_ACCION_OFF = 1


    def __init__(self, name="",  Controlador="", pin=0):
        self.nameAct = name
        self.controller = Controlador
        self.pin = pin

    def setAccion(self, accion):
        if accion == "on":
            print("accion:" + accion)
            self.setOn()
        else:
            self.setOff()


    def setOn(self):
        order = self.nameAct+ ":" + str(self.pin) + ":" + str(self.TYPE_ACCION_ON)
        print(order)
        self.controller.arduino.write(order.encode())
        return True

    def setOff(self):
        order = self.nameAct + ":" + str(self.pin) + ":" + str(self.TYPE_ACCION_OFF)
        print(order)
        self.controller.arduino.write(order.encode())
        return True




class Sensor(object):

    ports = []
    baud = ""
    on = False

    def __init__(self, name, Controlador=""):
        self.nameAct = name
        self.controller = Controlador


    def read_data(self):
        data_payload = dict()
        med = self.controller.arduino.readline().decode("utf-8")[:-2]
        try:
            topic, payload = med.split(':')
            cultivo = topic.split('/')[0]
            function = topic.split('/')[1]
            component = topic.split('/')[2]
            variable = topic.split('/')[3]
            value = payload
            data_payload = {
                    'cultivo':cultivo,
                    'function':function,
                    'component':component,
                    'variable':variable,
                    'value':value
                    }
        except:
            print("error")

        print("---------------------------------------")
        print(" ")
        print("mediciones {}".format(data_payload))
        print(" ")
        print("---------------------------------------")
        return data_payload

