from celery import Celery
from app.client import ClientMqtt
from app.models import db, Measure
import time
from app.sensor import Sensor, Controller



BACKEND = BROKER = 'redis://localhost:6379'

celery = Celery(__name__, backend=BACKEND, broker=BROKER)


@celery.task(name = 'sensor_mqtt')
def create_sensor_mqtt(broker_config):
    print("-----Create Sensor")
    from app.app import create_app
    app = create_app()
    db.init_app(app)
    with app.app_context():
        client2mqtt = client_mqtt(broker_config)
        client2mqtt.loop_start()
        while True:
            if client2mqtt._state_msg:
                data_mqtt = client2mqtt._data_payload
                print(data_mqtt)
                measure_save(data_mqtt)
                client2mqtt._state_msg = False
    return True




@celery.task(name = 'action_mqtt')
def create_action_mqtt(broker_config):
    print("-----Create Action Mqtt")
    from app.app import create_app
    app = create_app()
    db.init_app(app)
    with app.app_context():
        client2mqtt = client_mqtt(broker_config)
        client2mqtt.loop_start()
        from app.models import Action, Component
        while True:
            r_task = db.session.query(Action, Component).join(Component).filter(Action.state == '0', Component.function == 'action').all()
            print(r_task)
            if r_task:
                for r in r_task:
                    topic = 'action'+ '/' +str(r[0].cultivo_id)
                    payload = str(r[1].num_pin) + '/' + str(r[0].accion)
                    pub_task = {'topic':topic, 'payload':payload}
                    print("query: {}:{}".format(pub_task['topic'], pub_task['payload']))
                    task = action_mqtt_exec(pub_task, broker_config, client2mqtt, r[1].id)
                    print("task:{}".format(task))
                    if task:
                        db.session.query(Action).filter(Action.id==r[0].id).update({'state':'1'})
                        db.session.commit()
            time.sleep(0.1)
    return True




def action_mqtt_exec(pub_task, broker_config, client, component_id):
    from app.models import Component
    client = client_mqtt(broker_config)
    client.publish(topic = pub_task['topic'], payload = pub_task['payload'])
    state = pub_task['payload'].split('/')[1]
    print(component_id)
    db.session.query(Component).filter(Component.id==component_id, Component.function=='action').update({'state':state})
    db.session.commit()
    time.sleep(0.1)
    return True





@celery.task(name='serial_excec')
def action_serial_exec():
    serial = s



def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+ str(rc))
    client.subscribe(client._data_topic)


def on_message(client, userdata, msg):
    client._state_msg = True
    time.sleep(1)
    try:
        client.process_msg(msg)

    except:
        print("No hay dispositivo conectado")
        return


def client_mqtt(broker_config):
    topic_name = broker_config['topic']
    broker_name = broker_config['broker']
    port_name = broker_config['port']
    port_name = 1883
    client = ClientMqtt()
    client.on_connect = on_connect
    client.on_message = on_message
    client._data_topic = topic_name
    client.connect(broker_name, port_name)
    return client


def measure_db(data):
    print(data)
    measure = Measure()
    measure.cultivo_id = data['cultivo_id']
    measure.component_id = data['component_id']
    measure.variable = data['variable']
    measure.value = data['value']
    return measure




def measure_save(data_mqtt):
    data = dict()
    print("--------------------------------")
    print("Save dataaa")
    print("--------------------------------")
    if data_mqtt:
        from app.models import DetaileDevice, Component, Device, CultivoDevice
        q_component_cultivo = db.session.query(CultivoDevice, DetaileDevice, Component).join(Device).filter(
                 CultivoDevice.cultivo_id==data_mqtt['cultivo'],
                 DetaileDevice.component_id==data_mqtt['component']).first()
        print("component:".format(q_component_cultivo))
        data['cultivo_id'] = q_component_cultivo[0].cultivo_id
        data['component_id'] = q_component_cultivo[1].component_id
        data['variable'] = q_component_cultivo[2].variable
        data['value'] = data_mqtt['value']
        print("data" + str(data))
        db.session.add(measure_db(data))
        db.session.commit()
    return True





@celery.task(name = 'client_serial')
def create_sensor_serial():
    print("------------------------------------------")
    print(" ")
    print("Inicio de la tarea cliente")
    print(" ")
    print("------------------------------------------")
   # debería venir la info del sensor
    from app.app import create_app
    app = create_app()
    db.init_app(app)
    with app.app_context():
        arduino_sensor = Controller(ports=['/dev/ttyUSB0', '/deb/ttyUSB1'])
        arduino_sensor = Controller(ports=['/dev/ttyACM0',  '/dev/ttyACM1'])
        S = Sensor("sensores", arduino_sensor)
        print("tarea_client")
        while True:
            data_serial = S.read_data()
            print("mediciones {}".format(data_serial))
            measure_save(data_serial)
            time.sleep(10)
    return True

