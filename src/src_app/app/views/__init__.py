from .home import home
from .huerta import huerta
from .register import register


blueprints = [register, home, huerta ]
