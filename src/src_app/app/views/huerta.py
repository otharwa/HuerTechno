from flask import Blueprint, render_template, request
from app.models import db, Measure, Cultivo, DetaileDevice, Component, Action, Device, CultivoDevice
import json
from datetime import datetime

huerta = Blueprint('huerta', __name__)


@huerta.route('/huerta', methods=['GET', 'POST'])
def index():
    check = {'false':0, 'true':1}
    cultivos = [cultivo for cultivo in db.session.query(Cultivo)]
    r_cultivo = request.form.get('num_cultivo')
    actions = dict()
    if r_cultivo:
         actions  = [ act._asdict() for act in db.session.query(Component).join(DetaileDevice).join(Device).join(CultivoDevice).filter(CultivoDevice.cultivo_id == r_cultivo, Component.function=='action')]
         print(actions)
    state = request.form.get('state')
    r_component = request.form.get('id')
    cultivo_id = request.form.get('cultivo_id')
    if state:
        data = {'cultivo_id':cultivo_id,
                'component_id':r_component,
                'time_action':datetime.now(),
                'state':'0',
                'accion':check[state]
                }
        print(data)
        db.session.query(Component).filter(Component.id==r_component).update({'state':check[state]})
        db.session.add(accion_db(data))
        db.session.commit()
    return render_template('huerta/index.html', cultivos=cultivos, actions=actions, select=r_cultivo)



def accion_db(data):
    accion = Action()
    accion.cultivo_id = data['cultivo_id']
    accion.component_id = data['component_id']
    accion.time_action = data['time_action']
    accion.state = data['state']
    accion.accion = data['accion']
    return accion


