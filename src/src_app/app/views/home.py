from flask import Blueprint, render_template, request
from app.models import db, Measure, Cultivo, DetaileDevice, CultivoDevice, Component, Device
import json
from datetime import datetime
from app.sensor import Sensor


home = Blueprint('home', __name__)

COLORES = {
        'LUZ': "card text-white bg-warning mb-3",
        'TEM': "card text-white bg-danger mb-3",
        'HUM_SOIL': "card text-white bg-success mb-3",
        'HUM':"card text-white bg-primary mb-3"
        }


@home.route('/', methods=['GET', 'POST'])
def index():
    data = dict()
    cultivos = [cultivo for cultivo in db.session.query(Cultivo)]
    r_cultivo = request.form.get('num_cultivo')
    select = 1
    if r_cultivo:
        select = r_cultivo
    components  = [(c.id, c.variable)  for c in db.session.query(Component).join(DetaileDevice).join(Device).join(CultivoDevice).filter(CultivoDevice.cultivo_id == select, Component.function=='sensor')]
    print(components)
    for c_id, c_var in components:
        data[c_var] = [med.value for med in  db.session.query(Measure).order_by(Measure.date_created.desc()).filter(Measure.component_id == c_id, Measure.cultivo_id == select).limit(1)]
    print(data)
    data_template={
            "colores":COLORES,
            "data":data,
            "select":r_cultivo
            }
    return render_template('home/index.html', colores=COLORES, data=data, cultivos=cultivos, select=r_cultivo)




